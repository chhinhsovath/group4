<?php

namespace Database\Factories;

use App\Models\buyers;
use Illuminate\Database\Eloquent\Factories\Factory;

class BuyersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = buyers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
