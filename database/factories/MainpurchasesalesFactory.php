<?php

namespace Database\Factories;

use App\Models\mainpurchasesales;
use Illuminate\Database\Eloquent\Factories\Factory;

class MainpurchasesalesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = mainpurchasesales::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
