<?php

namespace Database\Factories;

use App\Models\investments;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvestmentsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = investments::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
