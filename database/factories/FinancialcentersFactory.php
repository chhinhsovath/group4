<?php

namespace Database\Factories;

use App\Models\financialcenters;
use Illuminate\Database\Eloquent\Factories\Factory;

class FinancialcentersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = financialcenters::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
