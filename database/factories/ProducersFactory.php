<?php

namespace Database\Factories;

use App\Models\producers;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProducersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = producers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
