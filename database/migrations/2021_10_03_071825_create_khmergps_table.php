<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKhmergpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khmergps', function (Blueprint $table) {
            $table->id();
            $table->integer('vilcode');
            $table->string('vilname');
            $table->integer('comcode');
            $table->string('comname');
            $table->integer('discode');
            $table->string('disname');
            $table->integer('procode');
            $table->string('proname');
            $table->integer('xcoor');
            $table->integer('ycoor');
            $table->integer('lattitude');
            $table->integer('longtitute');
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khmergps');
    }
}
