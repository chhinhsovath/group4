<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingtopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_topics', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
			$table->string('name');
			$table->string('name_kh');
            $table->text('note');
			$table->integer('record_status');
			$table->integer('delete_status');
			$table->integer('created_id');
			$table->dateTime('created_at');
			$table->integer('updated_id');
			$table->dateTime('updated_at');
			$table->integer('deleted_id');
			$table->dateTime('deleted_at');
			$table->integer('checked_id');
			$table->dateTime('checked_at');
			$table->integer('approved_id');
			$table->dateTime('approved_at');
			$table->integer('rejected_id');
			$table->dateTime('rejected_at');
			$table->integer('grantee_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_topics');
    }
}
