<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->string('facilitator')->nullable();
            $table->string('subsector')->nullable();
            $table->string('buyersid')->nullable();
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('start_date')->default(0);
            $table->string('end_date')->nullable();
            $table->string('hire_full_time_staff_last_quarter')->default(0);
            $table->string('number_full_time_staff')->nullable();
            $table->string('number_of_day_full_time_staff')->default(0);
            $table->string('number_youth')->nullable();
            $table->string('number_female')->nullable();
            $table->string('date_hire_full_time_staff')->nullable();
            $table->string('hire_part_time_staff_last_quarter')->nullable();
            $table->string('number_part_time_staff')->nullable();
            $table->string('how_many_day_part_time_staff_worked_per_quarter')->nullable();
            $table->string('total_man_days')->nullable();
            $table->string('total_fte_jobs_created')->nullable();
            $table->string('date_hire')->nullable();
            $table->string('date_data_collection')->nullable();
            $table->string('total_fte')->nullable();
            $table->string('fte1')->nullable();
            $table->string('fte2')->nullable();
            $table->string('fte3')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
