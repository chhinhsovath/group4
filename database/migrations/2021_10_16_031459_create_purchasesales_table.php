<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesaleitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasesaleitems', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->string('mainpurchasesalesid')->default(0);
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('discode')->default(0);
            $table->string('disname')->nullable();
            $table->string('comcode')->default(0);
            $table->string('comname')->nullable();
            $table->string('vilcode')->default(0);
            $table->string('vilname')->nullable();
            $table->string('buyername')->nullable();
            $table->string('suppliername')->nullable();
            $table->string('cropnames')->nullable();
            $table->string('subsector')->nullable();
            $table->string('date')->nullable();
            $table->string('purchaseunit')->nullable();
            $table->string('purchaseprice')->nullable();
            $table->string('purchasetotal')->nullable();
            $table->string('purchasenote')->nullable();
            $table->string('saleunit')->nullable();
            $table->string('saleprice')->nullable();
            $table->string('saletotal')->nullable();
            $table->string('salenote')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasesaleitems');
    }
}
