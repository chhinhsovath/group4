<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainpurchasesalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mainpurchasesales', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->string('mainpurchasesalesid')->default(0);
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('discode')->default(0);
            $table->string('disname')->nullable();
            $table->string('comcode')->default(0);
            $table->string('comname')->nullable();
            $table->string('vilcode')->default(0);
            $table->string('vilname')->nullable();
            $table->string('address')->nullable();
            $table->string('cropsid1')->nullable();
            $table->string('cropname1')->nullable();
            $table->string('cropsid2')->nullable();
            $table->string('cropname2')->nullable();
            $table->string('cropsid3')->nullable();
            $table->string('cropname3')->nullable();
            $table->string('cropnames')->nullable();
            $table->string('croptype')->nullable();
            $table->string('subsector')->nullable();
            $table->string('buysid1')->nullable();
            $table->string('buyernames')->nullable();
            $table->string('suppliernames')->nullable();
            $table->string('suppliersid1')->nullable();
            $table->string('suppliername1')->nullable();
            $table->string('suppliersid2')->nullable();
            $table->string('suppliername2')->nullable();
            $table->string('suppliersid3')->nullable();
            $table->string('suppliername3')->nullable();
            $table->string('suppliersid4')->nullable();
            $table->string('suppliername4')->nullable();
            $table->string('suppliersid5')->nullable();
            $table->string('suppliername5')->nullable();
            $table->string('note')->nullable();
            $table->string('totalitems')->default(0);
            $table->string('startdate')->nullable();
            $table->string('enddate')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainpurchasesales');
    }
}
