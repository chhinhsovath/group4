<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->string('name')->nullable();
            $table->string('namekh')->nullable();
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->string('youth')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('discode')->default(0);
            $table->string('disname')->nullable();
            $table->string('comcode')->default(0);
            $table->string('comname')->nullable();
            $table->string('vilcode')->default(0);
            $table->string('vilname')->nullable();
            $table->string('areaplantedha')->nullable();
            $table->string('smallholder')->nullable();
            $table->string('cropnames')->nullable();
            $table->string('cropname1')->nullable();
            $table->string('cropname2')->nullable();
            $table->string('cropname3')->nullable();
            $table->string('subsector')->nullable();
            $table->string('buyernames')->nullable();
            $table->string('buyername1')->nullable();
            $table->string('buyername2')->nullable();
            $table->string('buyername3')->nullable();
            $table->string('lead')->nullable();
            $table->string('baseline')->nullable();
            $table->string('lattitude')->nullable();
            $table->string('longtitude')->nullable();
            $table->string('note')->nullable();
            $table->string('address')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
