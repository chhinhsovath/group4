<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bmps', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->integer('suppliersid')->nullable();
            $table->string('name')->nullable();
            $table->string('namekh')->nullable();
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->string('youth')->nullable();
            $table->string('phone')->nullable();
            $table->string('position')->nullable();
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('discode')->default(0);
            $table->string('disname')->nullable();
            $table->string('comcode')->default(0);
            $table->string('comname')->nullable();
            $table->string('vilcode')->default(0);
            $table->string('vilname')->nullable();
            $table->string('location')->nullable();
            $table->string('topic1')->nullable();
            $table->string('topic2')->nullable();
            $table->string('topic3')->nullable();
            $table->string('date')->nullable();
            $table->string('lead')->nullable();
            $table->string('enddate')->nullable();
            $table->string('trainer')->nullable();
            $table->string('trainer1')->nullable();
            $table->string('trainer2')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bmps');
    }
}
