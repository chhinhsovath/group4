<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->string('name')->nullable();
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('partners')->nullable();
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->string('subsector')->nullable();
            $table->string('cropnames')->nullable();
            $table->string('accessloan')->nullable();
            $table->string('loanvalue')->nullable();
            $table->string('period')->nullable();
            $table->string('rate')->nullable();
            $table->string('dateloanddisbursed')->nullable();
            $table->string('purposeofloan')->nullable();
            $table->string('bankormfi')->nullable();
            $table->string('facilitator')->nullable();
            $table->string('facilitatedbyharveststaff')->nullable();
            $table->string('typeofinvestment')->nullable();
            $table->string('otherinvestment')->nullable();
            $table->string('youth')->nullable();
            $table->string('famale')->nullable();
            $table->string('total')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
