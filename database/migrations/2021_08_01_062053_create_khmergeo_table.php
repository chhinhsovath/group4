<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKhmergeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khmergeo', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->double('vil_code');
            $table->string('vil_name');
            $table->double('com_code');
            $table->string('com_name');
            $table->double('dis_code');
            $table->string('dis_name');
            $table->double('pro_code');
            $table->string('pro_name');
            $table->double('xcoor');
            $table->double('ycoor');
            $table->string('lattitude');
            $table->string('longtitute');
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khmergeo');
    }
}
