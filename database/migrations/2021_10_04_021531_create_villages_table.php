<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVillagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('villages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('comcode',50);
            $table->string('vilcode',50);
            $table->string('vilname',50);
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('villages');
    }
}
