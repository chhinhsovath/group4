<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingtrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingtrainers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('namekh')->nullable();
            $table->char('sex')->nullable();
            $table->integer('age')->nullable();
            $table->char('youth')->nullable();
            $table->string('email')->nullable();
            $table->char('phone')->nullable();
            $table->char('subsector')->nullable();
            $table->string('cropnames')->nullable();
            $table->text('note')->nullable();
            $table->string('procode')->default(0);
            $table->string('discode')->default(0);
            $table->string('comcode')->default(0);
            $table->string('vilcode')->default(0);
            $table->string('latitude')->nullable();
            $table->string('longtitude')->nullable();
            $table->string('address')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingtrainers');
    }
}
