<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('oid')->nullable();
            $table->string('buyersid')->nullable();
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->string('subsector')->nullable();
            $table->string('staffprofilesid')->nullable();
            $table->string('staffprofilename')->nullable();
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('discode')->default(0);
            $table->string('disname')->nullable();
            $table->string('comcode')->default(0);
            $table->string('comname')->nullable();
            $table->string('vilcode')->default(0);
            $table->string('vilname')->nullable();
            $table->string('partners')->nullable();
            $table->timestamps('startdate');
            $table->timestamps('enddate');
            $table->string('workingcapitaldate')->nullable();
            $table->integer('workingcapitalamount')->nullable();
            $table->string('workingcapitaltnote')->nullable();
            $table->timestamps('capitalinvestmentdate');
            $table->integer('capitalinvestmentamount')->nullable();
            $table->string('capitalinvestmentnote')->nullable();
            $table->timestamps('supplycredittdate');
            $table->integer('supplycreditamount')->nullable();
            $table->string('supplycreditnote')->nullable();
            $table->timestamps('operationexpensedate');
            $table->string('operationexpenseamount')->nullable();
            $table->string('operationexpensenote')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investments');
    }
}
