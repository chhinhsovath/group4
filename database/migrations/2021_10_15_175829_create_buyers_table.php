<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyers', function (Blueprint $table) {
            $table->bigIncrements('mid');
            $table->integer('id')->nullable();
            $table->integer('id')->nullable();
            $table->string('name')->nullable();
            $table->string('namekh')->nullable();
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->string('youth')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('procode')->default(0);
            $table->string('proname')->nullable();
            $table->string('discode')->default(0);
            $table->string('disname')->nullable();
            $table->string('comcode')->default(0);
            $table->string('comname')->nullable();
            $table->string('vilcode')->default(0);
            $table->string('vilname')->nullable();
            $table->string('lattitude')->nullable();
            $table->string('longtitude')->nullable();
            $table->string('note')->nullable();
            $table->string('businessduration')->nullable();
            $table->string('sourceofsupply')->nullable();
            $table->string('subsector')->nullable();
            $table->string('cropnames')->nullable();
            $table->string('saletarget')->nullable();
            $table->string('buyertatus')->default('New');
            $table->string('mounumber')->nullable();
            $table->string('statusdate')->nullable();
            $table->string('moustartdate')->nullable();
            $table->string('mouenddate')->nullable();
            $table->string('mouextendeddate')->nullable();
            $table->string('typeofagreement')->nullable();
            $table->string('scaleoperation')->nullable();
            $table->string('scaleoperation1')->nullable();
            $table->string('scaleoperation2')->nullable();
            $table->string('lead')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('quarter')->nullable();
            $table->string('projectyear')->nullable();
            $table->string('recordstatus')->nullable();
            $table->integer('usr_cre')->nullable();
            $table->integer('usr_ngo')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('grantid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyers');
    }
}
