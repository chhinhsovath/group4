<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\TrainingTopicController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CropController;
use App\Http\Controllers\KhmergeoController;
use App\Http\Controllers\ProvincesController;
use App\Http\Controllers\DistrictsController;
use App\Http\Controllers\CommunesController;
use App\Http\Controllers\VillagesController;
use App\Http\Controllers\ExportsController;
use App\Http\Controllers\ImportExportController;
use App\Http\Controllers\ImportsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

// Export Import Controller
Route::get('import-export', [ImportExportController::class,'index'])->name('import-export');
Route::get('export', [ImportExportController::class,'export'])->name('export');
Route::post('import', [ImportExportController::class,'import'])->name('import');

Auth::routes();


Route::group(['middleware' => 'auth'], function () {


Route::get('buyers-export', [ExportsController::class, 'buyerExport'])->name('buyers-export');
Route::get('purchasesales-export', [ExportsController::class, 'purchasesalesExport'])->name('purchasesales-export');
Route::get('mainpurchasesales-export', [ExportsController::class, 'mainpurchasesalesExport'])->name('mainpurchasesales-export');
Route::get('suppliers-export', [ExportsController::class, 'suppliersExport'])->name('suppliers-export');
Route::get('producers-export', [ExportsController::class, 'producersExport'])->name('producers-export');
Route::get('bmps-export', [ExportsController::class, 'bmpsExport'])->name('bmps-export');
Route::get('investments-export', [ExportsController::class, 'investmentsExport'])->name('investments-export');
Route::get('loans-export', [ExportsController::class, 'loansExport'])->name('loans-export');
Route::get('jobs-export', [ExportsController::class, 'jobsExport'])->name('jobs-export');
Route::get('maintrainings-export', [ExportsController::class, 'maintrainingsExport'])->name('maintrainings-export');
Route::get('allinonecontacts-export', [ExportsController::class, 'allinonecontactsExport'])->name('allinonecontacts-export');


Route::get('import-tmp-contacts/upload-file', [ImportsController::class, 'index'])->name('import-tmp-contacts.uploadfile');
Route::post('import-tmp-contacts/final-import', [ImportsController::class, 'finalImport'])->name('import-tmp-contacts.finalimport');
Route::post('import-tmp-contacts/import', [ImportsController::class, 'import'])->name('import-tmp-contacts.import');
Route::post('import-tmp-contacts/clear-all', [App\Http\Controllers\ImportsController::class,'clearall'])->name('import-tmp-contacts.clearall');
Route::delete('import-tmp-contacts/destroy/{id}', [App\Http\Controllers\ImportsController::class,'destroy'])->name('import-tmp-contacts.destroy');
//Route::resource('import-tmp-contacts', App\Http\Controllers\ImportsController::class);

Route::post('investments/approve', [App\Http\Controllers\InvestmentsController::class,'approve'])->name('investments.approve');
Route::resource('investments', App\Http\Controllers\InvestmentsController::class);

Route::post('loans/approve', [App\Http\Controllers\LoansController::class,'approve'])->name('loans.approve');
Route::resource('loans', App\Http\Controllers\LoansController::class);

Route::post('jobs/approve', [App\Http\Controllers\JobsController::class,'approve'])->name('jobs.approve');
Route::resource('jobs', App\Http\Controllers\JobsController::class);

Route::resource('bmps', App\Http\Controllers\BmpsController::class);

Route::resource('producers', App\Http\Controllers\ProducersController::class);

Route::post('suppliers/approve', [App\Http\Controllers\SuppliersController::class,'approve'])->name('suppliers.approve');
Route::resource('suppliers', App\Http\Controllers\SuppliersController::class);

Route::get('buyers/hide/{id}', [App\Http\Controllers\BuyersController::class, 'hide'])->name('buyers.hide');
Route::post('buyers/approve', [App\Http\Controllers\BuyersController::class,'approve'])->name('buyers.approve');
Route::resource('buyers', App\Http\Controllers\BuyersController::class);

Route::post('mainpurchasesales/approve', [App\Http\Controllers\MainpurchasesalesController::class,'approve'])->name('mainpurchasesales.approve');
Route::resource('mainpurchasesales', App\Http\Controllers\MainpurchasesalesController::class);

Route::post('purchasesales/approve', [App\Http\Controllers\PurchasesalesController::class,'approve'])->name('purchasesales.approve');
Route::resource('purchasesales', App\Http\Controllers\PurchasesalesController::class);

Route::post('maintrainings/approve', [App\Http\Controllers\MainTrainingsController::class,'approve'])->name('maintrainings.approve');
Route::get('maintrainings/addparticipants/{id}', [App\Http\Controllers\MainTrainingsController::class,'addparticipants'])->name('maintrainings.addparticipants');
Route::resource('maintrainings', App\Http\Controllers\MainTrainingsController::class);
Route::post('maintrainings/storeparticipant', [App\Http\Controllers\MainTrainingsController::class,'storeparticipant'])->name('maintrainings.storeparticipant');
Route::resource('maintrainingsmeta', App\Http\Controllers\MainTrainingsMetaController::class);

Route::post('allinonecontacts/restore', [App\Http\Controllers\AllinoneContactsController::class,'restore'])->name('allinonecontacts.restore');
Route::resource('allinonecontacts', App\Http\Controllers\AllinoneContactsController::class);

//Route::get('/', function () {    return view('welcome'); });

// Route::group(['middleware' => ['auth']], function() {
Route::resource('permissions', PermissionController::class);
Route::resource('roles', RoleController::class);
Route::resource('users', UserController::class);
Route::resource('products', ProductController::class);
// });

Route::resource('references/crops', CropController::class);
Route::get('references/crops/softdelete/{id}',['as'=>'crops.softdelete','uses'=>'App\Http\Controllers\CropController@softdelete']);
Route::get('crops/get-crops',['as'=>'crops.getCrops','uses'=>'App\Http\Controllers\CropController@getCrops']);

Route::resource('references/training-topics', TrainingTopicController::class);
Route::get('training-topics/get-training-topics',['as'=>'training-topics.getTrainingTopics','uses'=>'App\Http\Controllers\TrainingTopicController@getTrainingTopics']);

// Resource Route for article.
//Route::resource('articles', ArticleController::class);
//Route::get('articles/softdelete/{id}',['as'=>'articles.softdelete','uses'=>'App\Http\Controllers\ArticlesController@softdelete']);
//Route::get('get-permissions', [PermissionController::class, 'getPermissions'])->name('get-permissions');
//Route::get('get-articles', [ArticleController::class, 'getArticles'])->name('get-articles');


Route::get('references/khmergeos/get-khmergeos', [KhmergeoController::class,'getKhmergeos'])->name('get-khmergeos');
Route::get('references/khmergeos/fileImportExport',['as'=>'khmergeos.fileImportExport','uses'=>'App\Http\Controllers\KhmergeoController@fileImportExport']);
Route::get('references/khmergeos/file-export',['as'=>'khmergeos.file-export','uses'=>'App\Http\Controllers\KhmergeoController@file_export']);
Route::get('references/khmergeos/file-import',['as'=>'khmergeos.file-import','uses'=>'App\Http\Controllers\KhmergeoController@file_import']);
Route::resource('references/khmergeos', KhmergeoController::class);

Route::get('getProvince',[ProvincesController::class, 'getProvince'])->name('getProvince');
Route::get('getDistrict/{procode}',[DistrictsController::class, 'getDistrict'])->name('getDistrict');
Route::get('getCommune/{discode}',[CommunesController::class, 'getCommune'])->name('getCommune');
Route::get('getVillage/{comcode}',[VillagesController::class, 'getVillage'])->name('getVillage');

    Route::get('/route-cache', function() {
        $exitCode = Artisan::call('route:cache');
        return 'Routes cache cleared';
    });
    Route::get('/config-cache', function() {
        $exitCode = Artisan::call('config:cache');
        return 'Config cache cleared';
    });
    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        return 'Application cache cleared';
    });
    Route::get('/view-clear', function() {
        $exitCode = Artisan::call('view:clear');
        return 'View cache cleared';
    });


// Resource Route for article.
// Route::resource('references/training-topics', TrainingTopicController::class);
// Route for get articles for yajra post request.
// Route::get('get-training-topics', [TrainingTopicController::class, 'getTrainingTopics'])->name('get-training-topics');


// Route::post('references/training-topics/store',['as'=>'references.training-topics.store','uses'=>'App\Http\Controllers\TrainingTopicController@store']);
// Route::get('references/training-topics',['as'=>'references.training-topics.index','uses'=>'App\Http\Controllers\TrainingTopicController@index']);
// Route::get('references/training-topics/ajax',['as'=>'references.training-topics.ajax','uses'=>'App\Http\Controllers\TrainingTopicController@ajax']);
// Route::get('references/training-topics/create',['as'=>'references.training-topics.create','uses'=>'App\Http\Controllers\TrainingTopicController@create']);
// Route::post('references/training-topics/update/{id}',['as'=>'references.training-topics.update','uses'=>'App\Http\Controllers\TrainingTopicController@update']);
// Route::get('references/training-topics/show/{id}',['as'=>'references.training-topics.show','uses'=>'App\Http\Controllers\TrainingTopicController@show']);
// Route::get('references/training-topics/{id}/edit',['as'=>'references.training-topics.edit','uses'=>'App\Http\Controllers\TrainingTopicController@edit']);
// Route::delete('references/training-topics/{id}',['as'=>'training-topics.delete','uses'=>'App\Http\Controllers\TrainingTopicController@delete']);
// Route::delete('references/training-topics/destroy/{id}',['as'=>'references.training-topics.destroy','uses'=>'App\Http\Controllers\TrainingTopicController@destroy']);
// Route::get('references/training-topics/restoreOne/{id}',['as'=>'references.training-topics.restoreOne','uses'=>'App\Http\Controllers\TrainingTopicController@restoreOne']);
// Route::get('references/training-topics/index',['as'=>'references.training-topics.index.getdeleted','uses'=>'App\Http\Controllers\TrainingTopicController@index']);
// Route::get('references/training-topics/restoreAll',['as'=>'references.training-topics.restoreAll','uses'=>'App\Http\Controllers\TrainingTopicController@restoreAll']);

});


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::get('/dashboard', 'App\Http\Controllers\HomeController@index')->name('dashboard.home');
Route::get('/dashboard/charts', 'App\Http\Controllers\HomeController@charts')->name('dashboard.charts');
Route::get('/dashboard/project-years', 'App\Http\Controllers\HomeController@projectyears')->name('dashboard.projectyears');
Route::get('/dashboard/quarters', 'App\Http\Controllers\HomeController@quarters')->name('dashboard.quarters');
Route::get('/dashboard/subsectors', 'App\Http\Controllers\HomeController@subsectors')->name('dashboard.subsectors');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

//Route::get('chart-js', [ChartJSController::class, 'index']);
Route::get('chart-js', 'App\Http\Controllers\ChartJSController@index')->name('index');

