<?php

namespace App\Exports;

use App\Models\suppliers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SuppliersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return suppliers::all();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Name',
            'Name Khmer',
            'Sex',
            'Age',
            'Youth',
            'Phone',
            'E-mail',
            'Province Code',
            'Province Name',
            'District Code',
            'District Name',
            'Commune Code',
            'Commune Name',
            'Village Code',
            'Village Name',
            'lattitude',
            'longtitude',
            'note',
            'businessduration',
            'sourceofsupply',
            'subsector',
            'cropname',
            'saletarget',
            'buyertatus',
            'mounumber',
            'statusdate',
            'moustartdate',
            'mouenddate',
            'mouextendeddate',
            'typeofagreement',
            'scaleoperation',
            'scaleoperation1',
            'scaleoperation2',
            'lead',
            'monthyear',
            'qurater',
            'projectyear',
            'recordstatus',
            'userid',
            'grantid',
            'created_at',
            'updated_at'
        ];
    }
}
