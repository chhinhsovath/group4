<?php

namespace App\Exports;

use App\Models\Buyers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BuyersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        return Buyers::select('id',
                                'name',
                                'namekh',
                                'sex',
                                'age',
                                'youth',
                                'phone',
                                'email',
//                                'procode',
                                'proname',
//                                'discode',
                                'disname',
//                                'comcode',
                                'comname',
//                                'vilcode',
                                'vilname',
                                'latitude',
                                'longitude',
                                'note',
                                'address',
                                'businessduration',
                                'sourceofsupply',
                                'subsector',
                                'cropnames',
                                'saletarget',
                                'buyertatus',
                                'mounumber',
                                'statusdate',
                                'moustartdate',
                                'mouenddate',
                                'mouextendeddate',
                                'typeofagreement',
                                'scaleoperation',
                                'scaleoperation1',
                                'scaleoperation2',
                                'lead',
                                'monthyear',
                                'qurater',
                                'projectyear',
                                'recordstatus',
                                'tables',
                                'uuid',
                                'usr_cre',
                                'usr_ngo'
//                                'userid',
//                                'grantid'
                                )->get();
    }

    public function headings(): array
        {
        return [
            'RowID',
            'Name EN',
            'Name KH',
            'Sex',
            'Age',
            'Youth',
            'Phone',
            'E-mail',
//            'procode',
            'Province',
//            'discode',
            'District',
//            'comcode',
            'Commune',
//            'vilcode',
            'Village',
            'Latitude',
            'Longitude',
            'Note',
            'Address',
            'Business Duration',
            'Source of Supply',
            'Sub Sector',
            'Crop Names',
            'Sale Target',
            'Buyer Status',
            'MoU Number',
            'Status Date',
            'MoU Start Date',
            'MoU End Date',
            'MoU Extended Date',
            'typeofagreement',
            'Scale of Operation)',
            'Scale of Operation 1',
            'Scale of Operation 2',
            'Lead',
            'Month Year',
            'Qurater',
            'Project Year',
            'Record Status',
            'Tables',
            'uuid',
            'Entry By',
            'Grant'
//            'userid',
//            'grantid'
        ];
    }

}





