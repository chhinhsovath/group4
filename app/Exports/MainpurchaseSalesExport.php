<?php

namespace App\Exports;

use App\Models\mainpurchasesales;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MainpurchaseSalesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return mainpurchasesales::all();
    }


}
