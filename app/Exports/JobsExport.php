<?php

namespace App\Exports;

use App\Models\jobs;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class JobsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        return jobs::all();
    }

//    public function headings(): array
//        {
//        return [
//            'Id',
//            'Name',
//            'Name Khmer',
//            'Sex',
//            'Age',
//            'Youth',
//            'Phone',
//            'E-mail',
//            'Province Code',
//            'Province Name',
//            'District Code',
//            'District Name',
//            'Commune Code',
//            'Commune Name',
//            'Village Code',
//            'Village Name',
//            'lattitude',
//            'longtitude',
//            'note',
//            'businessduration',
//            'sourceofsupply',
//            'subsector',
//            'cropname',
//            'saletarget',
//            'buyertatus',
//            'mounumber',
//            'statusdate',
//            'moustartdate',
//            'mouenddate',
//            'mouextendeddate',
//            'typeofagreement',
//            'scaleoperation',
//            'scaleoperation1',
//            'scaleoperation2',
//            'lead',
//            'monthyear',
//            'qurater',
//            'projectyear',
//            'recordstatus',
//            'userid',
//            'grantid',
//            'created_at',
//            'updated_at'
//        ];
//    }

}





