<?php

namespace App\Exports;

use App\Models\AllinoneContacts;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllinoneContactsExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        return AllinoneContacts::select('id',
                                'name_en',
                                'name_kh',
                                'sex',
                                'age',
                                'phone',
                                'procode',
                                'proname',
                                'discode',
                                'disname',
                                'comcode',
                                'comname',
                                'vilcode',
                                'vilname',
                                'latitude',
                                'longitude',
                                'tables',
                                'uuid'
                                )->groupby('name_en','proname')->get();
    }

    public function headings(): array
        {
        return [
            'RowID',
            'Name EN',
            'Name KH',
            'Sex',
            'Age',
            'Phone',
            '#',
            'Province',
            '#',
            'District',
            '#',
            'Commune',
            '#',
            'Village',
            'Latitude',
            'Longitude',
            'Tables',
            'UUID',
        ];
    }

}





