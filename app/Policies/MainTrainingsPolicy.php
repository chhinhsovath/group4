<?php

namespace App\Policies;

use App\Models\MainTrainings;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MainTrainingsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $User
     * @param  \App\Models\MainTrainings  $MainTrainings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canupdate(User $User, MainTrainings $MainTrainings)
    {
        return $User->roles === 'admin' ||  $MainTrainings->userid === $User->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $User
     * @param  \App\Models\MainTrainings  $MainTrainings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function candelete(User $User, MainTrainings $MainTrainings)
    {
        return $User->roles === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $User
     * @param  \App\Models\MainTrainings  $MainTrainings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canapprove(User $User, MainTrainings $MainTrainings)
    {
        return $User->roles === 'admin' || $User->roles === 'approver';
    }
}
