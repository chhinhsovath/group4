<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Jobs;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canupdate(User $user, Jobs $Jobs)
    {
        return $user->roles === 'admin' ||  $Jobs->userid === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function candelete(User $user, Jobs $Jobs)
    {
        return $user->roles === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canapprove(User $user, Jobs $Jobs)
    {
        return $user->roles === 'admin' || $user->roles === 'approver';
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Jobs $Jobs)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Jobs $Jobs)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Jobs $Jobs)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Jobs $Jobs)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Jobs $Jobs)
    {
        //
    }
}
