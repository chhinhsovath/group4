<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Suppliers;
use Illuminate\Auth\Access\HandlesAuthorization;

class SuppliersPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canupdate(User $user, Suppliers $Suppliers)
    {
        return $user->roles === 'admin' ||  $Suppliers->userid === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function candelete(User $user, Suppliers $Suppliers)
    {
        return $user->roles === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canapprove(User $user, Suppliers $Suppliers)
    {
        return $user->roles === 'admin' || $user->roles === 'approver';
    }
}
