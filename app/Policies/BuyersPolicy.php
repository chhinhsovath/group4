<?php

namespace App\Policies;

use App\Models\Buyers;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyersPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $User
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canupdate(User $User, Buyers $Buyers)
    {
        return $User->roles === 'admin' ||  $Buyers->userid === $User->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $User
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function candelete(User $User, Buyers $Buyers)
    {
        return $User->roles === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $User
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canapprove(User $User, Buyers $Buyers)
    {
        return $User->roles === 'admin' || $User->roles === 'approver';
    }
}
