<?php

namespace App\Policies;

use App\Models\mainpurchasesales;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MainpurchasesalesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\mainpurchasesales  $mainpurchasesales
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canupdate(User $user, mainpurchasesales $mainpurchasesales)
    {
        return $user->roles === 'admin' ||  $mainpurchasesales->userid === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\mainpurchasesales  $mainpurchasesales
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function candelete(User $user, mainpurchasesales $mainpurchasesales)
    {
        return $user->roles === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\mainpurchasesales  $mainpurchasesales
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canapprove(User $user, mainpurchasesales $mainpurchasesales)
    {
        return $user->roles === 'admin' || $user->roles === 'approver';
    }
}
