<?php

namespace App\Policies;

use App\Models\Purchasesales;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PurchasesalesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canupdate(User $user, Purchasesales $Purchasesales)
    {
        return $user->roles === 'admin' ||  $Purchasesales->userid === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function candelete(User $user, Purchasesales $Purchasesales)
    {
        return $user->roles === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function canapprove(User $user, Purchasesales $Purchasesales)
    {
        return $user->roles === 'admin' || $user->roles === 'approver';
    }
}
