<?php

namespace App\Http\Controllers;

//use App\Models\communes;
use App\Models\Districts;
use App\Http\Controllers\Controller;
use App\Models\Khmergeo;
use App\Models\provinces;
//use App\Models\villages;
use Illuminate\Http\Request;

class DistrictsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Districts  $Districts
     * @return \Illuminate\Http\Response
     */
    public function show(Districts $Districts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Districts  $Districts
     * @return \Illuminate\Http\Response
     */
    public function edit(Districts $Districts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Districts  $Districts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Districts $Districts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Districts  $Districts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Districts $Districts)
    {
        //
    }
//    public function getProvince(Request $request)
//    {
//        $provinces = provinces::select('procode','proname')->whereRaw('proname <> ""')->groupBy('procode')
//            ->pluck('proname','procode');
//        return response()
//            ->json($provinces);
//    }

    public function getDistrict(Request $request)
    {
        $Districts = Districts::select('discode','disname')->where("procode","=", $request->procode)->pluck("disname", "discode");
//        dd($Districts);
        return response()
            ->json($Districts);
    }

//    public function getCommune(Request $request)
//    {
//        $communes = communes::select('comcode','comname')->where("discode", $request->discode)->groupBy('comcode')
//            ->pluck("comname", "comcode");
//        return response()
//            ->json($communes);
//    }

//    public function getVillage(Request $request)
//    {
//        $villages = villages::select('vilcode','vilname')->where("comcode", $request->comcode)->groupBy('vilcode')
//            ->pluck("vilname", "vilcode");
//        return response()
//            ->json($villages);
//    }
}
