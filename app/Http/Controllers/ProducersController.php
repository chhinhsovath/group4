<?php

namespace App\Http\Controllers;

use App\Models\Producers;
use App\Models\Provinces;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ProducersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Producers = QueryBuilder::for(Producers::class)
        ->allowedFilters(['id','name','namekh','sex','phone','procode'])
        ->orderBy('id','desc')->paginate(15);
        $Provinces = Provinces::pluck('proname', 'procode');
        $AllProducers = Producers::pluck('name', 'id');
        return view('producers.index')
        ->with('Producers',$Producers)->with('Provinces',$Provinces)->with('AllProducers',$AllProducers);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producers  $Producers
     * @return \Illuminate\Http\Response
     */
    public function show(Producers $Producers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producers  $Producers
     * @return \Illuminate\Http\Response
     */
    public function edit(Producers $Producers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producers  $Producers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producers $Producers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producers  $Producers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producers $Producers)
    {
        //
    }
}
