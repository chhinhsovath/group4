<?php

namespace App\Http\Controllers;

use App\Models\Bmps;
use App\Models\Provinces;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class BmpsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Bmps = QueryBuilder::for(Bmps::class)
        ->allowedFilters(['id','name','namekh','sex','phone','procode'])
        ->where('id','>',0)->orderBy('id','desc')->paginate(15);
        $Provinces = Provinces::pluck('proname', 'procode');
        return view('bmps.index')
        ->with('Bmps',$Bmps)->with('Provinces',$Provinces);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bmps  $Bmps
     * @return \Illuminate\Http\Response
     */
    public function show(Bmps $Bmps)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bmps  $Bmps
     * @return \Illuminate\Http\Response
     */
    public function edit(Bmps $Bmps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bmps  $Bmps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bmps $Bmps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bmps  $Bmps
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bmps $Bmps)
    {
        //
    }
}
