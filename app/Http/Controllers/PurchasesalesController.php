<?php

namespace App\Http\Controllers;

use App\Models\MainPurchasesales;
use App\Models\Purchasesales;
use App\Models\Provinces;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;

class PurchasesalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Purchasesales = QueryBuilder::for(Purchasesales::class)
        ->allowedFilters(['id','buyername','suppliername','subsector','cropname','proname'])
        ->where('id','>',0)->orderBy('id','desc')->paginate(15);
        $provinces = Provinces::pluck('proname', 'procode');
        return view('purchasesales.index')
        ->with('Purchasesales',$Purchasesales)->with('provinces',$provinces);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $Purchasesales = $request->all();
        $Purchasesales['userid'] = Auth::id();
        $Purchasesales['grantid'] = $request->grantid;
        Purchasesales::create($Purchasesales);
//        return redirect(route('backends.rooms.index'));
        smilify('success', 'Record was successfully stored');
        return redirect(url()->previous());
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Http\Response
     */
    public function show(Purchasesales $Purchasesales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchasesales $Purchasesales)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchasesales $Purchasesales)
    {
        $Purchasesales =Purchasesales::findOrFail($request->id);
        $Purchasesales->userid=$request->userid;
        $Purchasesales->grantid=$request->grantid;
        $Purchasesales->cropnames=$request->cropnames;
        $Purchasesales->date=$request->date;
        $Purchasesales->purchaseunit=$request->purchaseunit;
        $Purchasesales->purchaseprice=$request->purchaseprice;
        $Purchasesales->purchasetotal=$request->purchasetotal;
        $Purchasesales->saleunit=$request->saleunit;
        $Purchasesales->saleprice=$request->saleprice;
        $Purchasesales->saletotal=$request->saletotal;
        $Purchasesales->salenote=$request->salenote;
        $Purchasesales->save();

        smilify('success', 'Record was successfully updated');
        return redirect(url()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchasesales  $Purchasesales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchasesales $Purchasesales,$id)
    {
//        dd($id);
        $this->authorize('candelete', $Purchasesales);
        Purchasesales::where('id','=',$id)->delete();
        smilify('danger', 'Record was successfully deleted');
//        return redirect(route('purchasesales.index'));
        return redirect(url()->previous());
    }

    public function approve( Purchasesales $Purchasesales, Request $request)
    {
        // dd($request);
//        $this->authorize('canapprove', $Purchasesales);
        $Purchasesales =Purchasesales::findOrFail($request->id);
        $Purchasesales->recordstatus=$request->recordstatus;
        $Purchasesales->save();
        //notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
}
