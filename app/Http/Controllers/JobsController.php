<?php

namespace App\Http\Controllers;

use App\Models\Jobs;
use App\Models\Buyers;
use App\Models\Provinces;
use Illuminate\Http\Request;
use App\Models\Staffprofiles;
use Spatie\QueryBuilder\QueryBuilder;

class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Jobs = QueryBuilder::for(Jobs::class)
            ->allowedFilters(['id','partners','sex','procode'])
            ->where('id','>',0)->orderBy('id','desc')->paginate(15);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('jobs.index', [
            'Jobs' => $Jobs,
            'Provinces' => $Provinces,
            'Staffprofiles' => $Staffprofiles,
            'Buyers' => $Buyers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
//        $users = User::orderBy('name','asc')->pluck('name', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('jobs.create', [
            'Provinces' => $Provinces,
//            'users' => $users,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $Jobsdata = $request->all();
        Jobs::create($Jobsdata);
        notify()->preset('user-created');
        return redirect(route('jobs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Http\Response
     */
    public function show(Jobs $Jobs)
    {
        //        dd($request->all());

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobs $Jobs,$id)
    {
        //
        $this->authorize('canupdate', $Jobs);
        $Jobs = Jobs::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('jobs.edit', [
            'Jobs' => $Jobs,
            'Provinces' => $Provinces,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobs $Jobs)
    {
//        dd($request->all());
        $this->authorize('canupdate', $Jobs);
        $Jobs = Jobs::find($request->id);
//        dd($request->id);
        if($request->name!=NULL){
            $Jobs->userid =$request->userid;
            $Jobs->grantid =$request->grantid;
            $Jobs->name =$request->name;
            $Jobs->subsector =$request->subsector;
            $Jobs->procode  =$request->procode;
            $Jobs->staffprofilename  =$request->staffprofilename;
            $Jobs->startdate  =$request->startdate;
            $Jobs->enddate  =$request->enddate;
            $Jobs->note  =$request->note;
            $Jobs->totalexistingfulltimestaff =$request->totalexistingfulltimestaff;
            $Jobs->totalnewfulltimestaff =$request->totalnewfulltimestaff;
            $Jobs->totalfulltimeyouth =$request->totalfulltimeyouth;
            $Jobs->totalfulltimefemale =$request->totalfulltimefemale;
            $Jobs->datehirefulltimestaff =$request->datehirefulltimestaff;
            $Jobs->fulltimenote =$request->fulltimenote;
            $Jobs->totalparttimestaff =$request->totalparttimestaff;
            $Jobs->numberofdaysparttimestaff =$request->numberofdaysparttimestaff;
            $Jobs->totalpartimefemale =$request->totalpartimefemale;
            $Jobs->totalparttimeyouth =$request->totalparttimeyouth;
            $Jobs->datehireparttimestaff =$request->datehireparttimestaff;
            $Jobs->parttimenote =$request->parttimenote;

            $Jobs->update();
            notify()->preset('user-updated');
            return redirect(route('jobs.index'));
//            return redirect(url()->previous());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobs $Jobs,$id)
    {
        $this->authorize('candelete', $Jobs);
        Jobs::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(route('jobs.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jobs  $Jobs
     * @return \Illuminate\Http\Response
     */
    public function approve( Jobs $Jobs, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Jobs);
        // $Jobss = Jobs::where("id",$request->id)->first();
        $Jobs =Jobs::findOrFail($request->id);
        $Jobs->recordstatus=$request->recordstatus;
        $Jobs->save();
//        notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
}
