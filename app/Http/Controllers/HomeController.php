<?php

namespace App\Http\Controllers;
use App\Charts\BuyersSupplierProducersBmpsChart;

use App\Charts\PurchasesSalesInvestmentsLoansChart;
use App\Models\Dashboards;
use App\Charts\InvestmentChart;

use App\Models\Reports\BuyersSupplierProducersBmps;
use App\Models\User;

use App\Models\ReportPurchasesalesSubsectorsProjectyear;
use App\Models\ReportPurchasesalesProvincesProjectyear;

use App\Models\Reports\ProvinceProjectYearsPurchase;
use App\Charts\ProvinceProjectYearsPurchaseChart;

use App\Models\Reports\PurchaseSaleProvinces;
use App\Charts\PurchaseSaleProvincesChart;

use App\Models\Reports\PurchaseSaleProjectYears;
use App\Charts\PurchaseSaleProjectYearsChart;

use App\Models\Reports\PurchaseSaleSubsector;
use App\Charts\PurchaseSaleSubsectorChart;

use App\Models\Reports\TopFiveCrop;
use App\Charts\TopFiveCropChart;

use App\Models\Reports\Jobs;
use App\Charts\JobsChart;


use Illuminate\Http\Request;
//use Illuminate\Support\FacadesDB;
//use Illuminate\Support\FacadesAuth;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $borderColors = [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ];
        $fillColors = [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(255, 205, 86)',
        ];

        $label_right = [
            'legend' => [
                'display' => true,
                'labels' => [
                    'fontColor' => '#000',
                    'fontFamily' => 'Montserrat',
                ],
                'position' =>'right',
            ]];

        $options = [
                'legend' => [
                    'display' => true,
                    'labels' => [
                        'fontColor' => '#000',
                        'fontFamily' => 'Montserrat',
                    ],
                    'position' =>'top',
                ],
                'scales' => [
                    'xAxes' => [
                        [
//                            'stacked' => true,
                            'gridLines' => [
                                'display' => true
                            ]
                        ]
                    ],
                    'yAxes' => [
                        [
//                            'stacked' => true,
                            'gridLines' => [
                                'display' => true
                            ]
                        ]
                    ]
                ],
            'tooltips' => [
                'enabled' => true
            ],
            'hover'=> [
                 'animationDuration' => 1
            ],
        ];
        $options_stack = [
                'legend' => [
                    'display' => true,
                    'labels' => [
                        'fontColor' => '#000',
                        'fontFamily' => 'Montserrat',
                    ],
                    'position' =>'top',
                ],
                'scales' => [
                    'xAxes' => [
                        [
                            'stacked' => true,
                            'gridLines' => [
                                'display' => true
                            ]
                        ]
                    ],
                    'yAxes' => [
                        [
                            'stacked' => true,
                            'gridLines' => [
                                'display' => true
                            ]
                        ]
                    ]
                ],
            'tooltips' => [
                'enabled' => true
            ],
            'hover'=> [
                 'animationDuration' => 1
            ],
        ];

        $options_job = [
            'centerText'=> [
                    'display' => true,
                    'text' => "280"
                    ],
        'elements' => [
            'center' => [
                'text' => '11%',
                        'color' => '#66615c', // Default is #000000
                        'fontStyle' => 'Montserrat', // Default is Arial
                        'sidePadding' => 60 // Defualt is 20 (as a percentage)
                    ]
        ],
        'cutoutPercentage' => 90,
                'legend' => [
                    'display' => true,
                    'position' =>'right',
                ],
                    'tooltips' => [
                'enabled' => true
                    ],

                    'scales'=> [
                        'yAxes' => [[

                    'ticks' => [
                        'display' => true
                            ],
                    'gridLines' => [
                        'drawBorder' => true,
                                'zeroLineColor' => "transparent",
                                'color' => 'rgba(255,255,255,0.05)'
                            ]

                ]],

                        'xAxes'=> [[
                'barPercentage' => 100,
                        'gridLines' => [
                    'drawBorder' => true,
                            'color' => 'rgba(255,255,255,0.1)',
                            'zeroLineColor' => "transparent"
                        ],
                        'ticks' => [
                    'display' => true,
                        ]
                    ]]
                ],
            ];

        DB::select('CALL dashboards()');
        $dashboard = Dashboards::where('id',1)->first();
        $db_job = Dashboards::where('id',2)->first();

        $jobs_current = $dashboard->jobs_all;
        $jobs_target = $db_job->jobs_all;
        $jobs_percentage_completed = Round((100*$jobs_current)/$jobs_target,2);
        $jobs_percentage_missed = Round(100-$jobs_percentage_completed,2);
        $jobs_chart = new JobsChart;
        $jobs_chart->minimalist(true);
        $jobs_chart->options(
            $options_job
        );

        $jobs_chart->height(200);
        $jobs_chart->displaylegend(true);
        $jobs_chart->labels(['Jobs Achieved','Jobs To Achieve']);
        $jobs_chart->dataset('Jobs Created with H2', 'pie', [$jobs_percentage_completed, $jobs_percentage_missed])
            ->color($borderColors)
            ->backgroundcolor($fillColors);


        //BuyersSupplierProducersBmps
        $buyers_all = $dashboard->buyers_all;
        $suppliers_all = $dashboard->suppliers_active;
        $producers_all = $dashboard->producers_all;
        $bmps_all = $dashboard->bmps_all;

        $bspb_chart = new BuyersSupplierProducersBmpsChart;
        $bspb_chart->minimalist(true);
        $bspb_chart->options(
            $label_right
        );
        $bspb_chart->height(200);
        $bspb_chart->displaylegend(true);
        $bspb_chart->labels(['Buyers', 'Suppliers','BMPs']);
        $bspb_chart->dataset('People Engaged with H2', 'pie', [$buyers_all, $suppliers_all, $bmps_all])
            ->color($borderColors)
            ->backgroundcolor($fillColors);

        //PurchasesSalesInvestmentsLoans
        $investments_all = $dashboard->investments_all;
        $sale_at_farm = $dashboard->sale_at_farm;
        $sale_at_firm = $dashboard->sale_at_firm;
        $loans_all = $dashboard->loans_all;

        $psil_chart = new PurchasesSalesInvestmentsLoansChart;
        $psil_chart->minimalist(true);
        $psil_chart->options(
            $label_right
        );
        $psil_chart->height(200);
        $psil_chart->displaylegend(true);
        $psil_chart->labels(['Purchase', 'Sale', 'Investments','Loans']);
        $psil_chart->dataset('Money generated with H2', 'polarArea', [$sale_at_farm, $sale_at_firm, $investments_all,$loans_all])
            ->color($borderColors)
            ->backgroundcolor($fillColors);


        //INVESTMENT DOUGHNUT
        $workingcapitalamount = $dashboard->investments_workingcapitalamount;
        $capitalinvestmentamount = $dashboard->investments_capitalinvestmentamount;
        $supplycreditamount = $dashboard->investments_supplycreditamount;
        $operationexpenseamount = $dashboard->investments_operationexpenseamount;

        $investment_chart = new InvestmentChart;
        //$investment_chart->title('Investment by Types', 30, "rgb(255, 99, 132)", true, 'Kantumruy,Montserrat');
        $investment_chart->minimalist(true);
        $investment_chart->options(
            $label_right
        );
        $investment_chart->height(200);
        $investment_chart->displaylegend(true);
        $investment_chart->labels(['Working Capital', 'Capital Investment', 'Supply Credit','Operation Expense']);
        $investment_chart->dataset('Investment by Types', 'doughnut', [$workingcapitalamount, $capitalinvestmentamount, $supplycreditamount,$operationexpenseamount])
            ->color($borderColors)
            ->backgroundcolor($fillColors);
        //END INVESTMENT

        //PURCHASE SALE SUBSECTOR
        $data_pss = PurchaseSaleSubsector::all();
        $label_pss = [];
        $value_pss_p = [];
        $value_pss_s = [];
        foreach ($data_pss as $row){
            $label_pss[] = $row->subsector;
            $value_pss_p[] = $row->purchase;
            $value_pss_s[] = $row->sale;
        }
//        return $label_pss;
        $pss_chart = new PurchaseSaleSubsectorChart;


//        $pss_chart->minimalist(true);
        $pss_chart->height(200);
//        $pss_chart->displaylegend(true);
        $pss_chart->options($options);
        $pss_chart->labels($label_pss);
        $pss_chart->dataset('Purchase','bar',$value_pss_p)->backgroundcolor("rgb(219,242,242)")->color('rgb(107,208,152)');
        $pss_chart->dataset('Sale','line',$value_pss_s)
            ->color($borderColors)
            ->backgroundcolor("rgb(255,245,221)");

//        $data = PurchaseSaleProvinces::all();
//        $label = [];
//        $value_p = [];
//        $value_s = [];
//        foreach ($data as $row){
//            $label[] = $row->province;
//            $value_p[] = $row->purchase;
//            $value_s[] = $row->sale;
//        }
        //Start Top 10
        $topfivecrop_data = TopFiveCrop::all();
        $topfivecrop_label = [];
        $topfivecrop_p = [];
        $topfivecrop_s = [];
        foreach ($topfivecrop_data as $row){
            $topfivecrop_label[] = $row->crops;
            $topfivecrop_p[] = $row->purchase;
            $topfivecrop_s[] = $row->sale;
        }
        $topfivecrop_chart = new TopFiveCropChart;
        $topfivecrop_chart->options($options_stack);
        $topfivecrop_chart->height(200);
        $topfivecrop_chart->displaylegend(true);
        $topfivecrop_chart->labels($topfivecrop_label);
        $topfivecrop_chart->dataset('Purchase','bar',$topfivecrop_p)->backgroundcolor("rgb(219,242,242)")->color('rgb(107,208,152)');
        $topfivecrop_chart->dataset('Sale','bar',$topfivecrop_s)
            ->color($borderColors)
            ->backgroundcolor($fillColors);
        //End top 5


        $psp_pruchase   = PurchaseSaleProvinces::pluck('purchase','province');
        $psp_sale       = PurchaseSaleProvinces::pluck('sale','province');
        $psp_chart = new PurchaseSaleProvincesChart;
        //$psp_chart->minimalist(true);
        $psp_chart->options($options);
        $psp_chart->height(200);
        $psp_chart->displaylegend(true);
        $psp_chart->labels($psp_pruchase->keys(),$psp_sale->keys());
        $psp_chart->dataset('Purchase','line',$psp_pruchase->values())->backgroundcolor("rgb(219,242,242)")->color('rgb(107,208,152)');
        $psp_chart->dataset('Sale','line',$psp_sale->values())
            ->color($borderColors)
            ->backgroundcolor($fillColors);


        $pspy_pruchase  = PurchaseSaleProjectYears::pluck('purchase','projectyear');
        $pspy_sale      = PurchaseSaleProjectYears::pluck('sale','projectyear');
        $pspy_chart = new PurchaseSaleProjectYearsChart;
        //$pspy_chart->minimalist(true);
        $pspy_chart->options($options);
        $pspy_chart->barwidth(0.9);
        $pspy_chart->height(200);
        $pspy_chart->displaylegend(true);
        $pspy_chart->labels($pspy_pruchase->keys(),$pspy_sale->keys());
        $pspy_chart->dataset('Purchase','bar',$pspy_pruchase->values())->backgroundcolor("rgb(219,242,242)")->color('rgb(107,208,152)');
        $pspy_chart->dataset('Sale','bar',$pspy_sale->values())
            ->color($borderColors)
            ->backgroundcolor($fillColors);


        $purchase_sale_subsector_py = ReportPurchasesalesSubsectorsProjectyear::select("*")->paginate(10);
        $purchase_sale_province_py = ReportPurchasesalesProvincesProjectyear::select("*")->paginate(10);

        return view('pages.dashboard', [
            'purchase_sale_subsector_py'=> $purchase_sale_subsector_py,
            'purchase_sale_province_py'=> $purchase_sale_province_py,
            'dashboard'=> $dashboard,
            'psp_chart'=> $psp_chart,
            'pspy_chart'=> $pspy_chart,
            'investment_chart'=> $investment_chart,
            'pss_chart'=> $pss_chart,
            'topfivecrop_chart'=> $topfivecrop_chart,
            'bspb_chart'=> $bspb_chart,
            'psil_chart'=> $psil_chart,
            'jobs_chart'=> $jobs_chart,
        ]);
    }
}
