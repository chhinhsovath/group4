<?php

namespace App\Http\Controllers;

use App\Models\Buyers;
use App\Models\Suppliers;
use App\Models\Provinces;
use App\Models\Crops;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class SuppliersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Suppliers = QueryBuilder::for(Suppliers::class)
        ->allowedFilters(['id','name','namekh','sex','phone','procode'])
        ->where('id','>',0)->orderBy('id','desc')->paginate(15);

        $Provinces = Provinces::pluck('proname', 'procode');
        $AllSuppliers = Suppliers::all();

        return view('suppliers.index', [
            'AllSuppliers' => $AllSuppliers,
            'Suppliers' => $Suppliers,
            'Provinces' => $Provinces,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::pluck('proname', 'procode');
        $Crops = Crops::pluck('cropname', 'id');
        $Users = User::pluck('name', 'id');
        $Buyers = Buyers::pluck('name', 'id');

        return view('suppliers.create', [
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users,
            'Buyers' => $Buyers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        if($request->name!=NULL){
            $Suppliers =new Suppliers();
            $Suppliers->userid =$request->userid;
            $Suppliers->grantid =$request->grantid;
            $Suppliers->name =$request->name;
            $Suppliers->namekh =$request->namekh;
            $Suppliers->sex  =$request->sex;
            $Suppliers->age  =$request->age;
            $Suppliers->phone  =$request->phone;
            $Suppliers->email  =$request->email;
            $Suppliers->lead  =$request->lead;
            $Suppliers->subsector =$request->subsector;
            $Suppliers->areaplantedha =$request->areaplantedha;
            $Suppliers->smallholder =$request->smallholder;
            $Suppliers->address =$request->address;
            $Suppliers->procode =$request->procode;
            $Suppliers->discode =$request->discode;
            $Suppliers->comcode =$request->comcode;
            $Suppliers->vilcode =$request->vilcode;
            $Suppliers->latitude =$request->latitude;
            $Suppliers->longitude =$request->longitude;
            $Suppliers->note =$request->note;

            $buyernames = $request->buyernames;
            if( ($buyernames) > 0 ){
                $Suppliers->buyernames = implode(',', $buyernames);
            }

            $cropnames = $request->cropnames;
            if( ($cropnames) > 0 ){
                $Suppliers->cropnames = implode(',', $cropnames);
            }
            $youth = $request->age;
            if($youth<=29 && $youth>=18 ){
                $Suppliers->youth = 'Youth';
            }else{
                $Suppliers->youth = 'Non-Youth';
            }

            $Suppliers->save();
        }
        notify()->preset('user-created');
        return redirect()->route('suppliers.index')
            ->with('success', 'Suppliers created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Suppliers  $Suppliers,$id)
    {
        $Suppliers = Suppliers::find($id);
        $Provinces = Provinces::pluck('proname', 'procode');
        $Crops = Crops::pluck('cropname', 'id');
        $Users = User::pluck('name', 'id');
        $Buyers = Buyers::pluck('name', 'id');
//        $allSuppliers = Suppliers::all();

        return view('suppliers.show', [
//            'allSuppliers' => $allSuppliers,
            'Suppliers' => $Suppliers,
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users,
            'Buyers' => $Buyers
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Suppliers = Suppliers::find($id);
        $Provinces = Provinces::pluck('proname', 'procode');
        $Crops = Crops::pluck('cropname', 'id');
        $Users = User::pluck('name', 'id');
        $Buyers = Buyers::pluck('name', 'id');

        return view('suppliers.edit', [
            'Suppliers' => $Suppliers,
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users,
            'Buyers' => $Buyers
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suppliers $Suppliers)
    {
        $Suppliers = Suppliers::find($request->id);
        if($request->name!=NULL){
            $Suppliers->userid =$request->userid;
            $Suppliers->grantid =$request->grantid;
            $Suppliers->name =$request->name;
            $Suppliers->namekh =$request->namekh;
            $Suppliers->sex  =$request->sex;
            $Suppliers->age  =$request->age;
            $Suppliers->phone  =$request->phone;
            $Suppliers->email  =$request->email;
            $Suppliers->lead  =$request->lead;
            $Suppliers->subsector =$request->subsector;
            $Suppliers->areaplantedha =$request->areaplantedha;
            $Suppliers->smallholder =$request->smallholder;
            $Suppliers->address =$request->address;
            $Suppliers->procode =$request->procode;
            $Suppliers->discode =$request->discode;
            $Suppliers->comcode =$request->comcode;
            $Suppliers->vilcode =$request->vilcode;
            $Suppliers->latitude =$request->latitude;
            $Suppliers->longitude =$request->longitude;
            $Suppliers->note =$request->note;

            $buyernames = $request->buyernames;
            if( ($buyernames) > 0 ){
                $Suppliers->buyernames = implode(',', $buyernames);
            }

            $cropnames = $request->cropnames;
            if( ($cropnames) > 0 ){
                $Suppliers->cropnames = implode(',', $cropnames);
            }
            $youth = $request->age;
            if($youth<=29 && $youth>=18 ){
                $Suppliers->youth = 'Youth';
            }else{
                $Suppliers->youth = 'Non-Youth';
            }

            $Suppliers->update();
        }
        notify()->preset('user-updated');
        return redirect()->route('suppliers.index')
            ->with('success', 'Suppliers created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suppliers $Suppliers,$id)
    {
        $this->authorize('candelete', $Suppliers);
        Suppliers::where('id', $id)->delete();
        notify()->preset('user-deleted');
        return redirect(route('suppliers.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Suppliers  $Suppliers
     * @return \Illuminate\Http\Response
     */
    public function approve( Suppliers $Suppliers, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Suppliers);
        // $Supplierss = Suppliers::where("id",$request->id)->first();
        $Suppliers =Suppliers::findOrFail($request->id);
        $Suppliers->recordstatus=$request->recordstatus;
        $Suppliers->save();
        notify()->preset('user-approved');
        return redirect(url()->previous());
    }
}
