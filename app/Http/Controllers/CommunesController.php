<?php

namespace App\Http\Controllers;

use App\Models\Communes;
use App\Http\Controllers\Controller;
//use App\Models\districts;
use App\Models\Khmergeo;
//use App\Models\provinces;
//use App\Models\villages;
use Illuminate\Http\Request;

class CommunesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Communes  $Communes
     * @return \Illuminate\Http\Response
     */
    public function show(Communes $Communes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Communes  $Communes
     * @return \Illuminate\Http\Response
     */
    public function edit(Communes $Communes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Communes  $Communes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Communes $Communes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Communes  $Communes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Communes $Communes)
    {
        //
    }

//    public function getProvince(Request $request)
//    {
//        $provinces = provinces::select('procode','proname')->whereRaw('proname <> ""')->groupBy('procode')
//            ->pluck('proname','procode');
//        return response()
//            ->json($provinces);
//    }

//    public function getDistrict(Request $request)
//    {
//        $districts = districts::select('discode','disname')->where("procode", $request->procode)->groupBy('discode')
//            ->pluck("disname", "discode");
//        return response()
//            ->json($districts);
//    }

    public function getCommune(Request $request)
    {
        $Communes = Communes::select('comcode','comname')->where("discode", "=",$request->discode)->pluck("comname", "comcode");
        return response()
            ->json($Communes);
    }

//    public function getVillage(Request $request)
//    {
//        $villages = villages::select('vilcode','vilname')->where("comcode", $request->comcode)->groupBy('vilcode')
//            ->pluck("vilname", "vilcode");
//        return response()
//            ->json($villages);
//    }
}
