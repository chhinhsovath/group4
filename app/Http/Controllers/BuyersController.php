<?php

namespace App\Http\Controllers;

use App\Models\Buyers;
use App\Models\Provinces;
use App\Models\Crops;
use App\Models\Staffprofiles;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use DB;
class BuyersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        DB::enableQueryLog();


        $Buyers = QueryBuilder::for(Buyers::class)
        ->allowedFilters(['name','phone','proname'])
        ->orderBy('id','desc')->paginate(15);
//        $Provinces = Provinces::pluck('proname', 'procode');
        $Provinces = Buyers::groupBy('proname')->pluck('proname', 'proname');
        $allBuyers = Buyers::groupBy('name')->get();

//        dd(DB::getQueryLog());

        return view('buyers.index', [
            'allBuyers' => $allBuyers,
            'Buyers' => $Buyers,
            'Provinces' => $Provinces,
        ]);

    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::pluck('proname', 'procode');
        $Crops = Crops::pluck('cropname', 'id');
        $Users = User::pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('buyers.create', [
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->name!=NULL){
            $Buyers =new Buyers();
            $Buyers->userid =$request->userid;
            $Buyers->grantid =$request->grantid;
            $Buyers->name =$request->name;
            $Buyers->namekh =$request->namekh;
            $Buyers->sex  =$request->sex;
            $Buyers->age  =$request->age;
            $Buyers->phone  =$request->phone;
            $Buyers->email  =$request->email;
            $Buyers->lead  =$request->lead;
            $Buyers->businessduration =$request->businessduration;
            $Buyers->sourceofsupply =$request->sourceofsupply;
            $Buyers->subsector =$request->subsector;
            $Buyers->buyertatus =$request->buyertatus;
            $Buyers->statusdate =$request->statusdate;
            $Buyers->saletarget =$request->saletarget;
            $Buyers->mounumber =$request->mounumber;
            $Buyers->moustartdate =$request->moustartdate;
            $Buyers->mouenddate =$request->mouenddate;
            $Buyers->mouextendeddate =$request->mouextendeddate;
            $Buyers->typeofagreement =$request->typeofagreement;
            $Buyers->address =$request->address;
            $Buyers->procode =$request->procode;
            $Buyers->discode =$request->discode;
            $Buyers->comcode =$request->comcode;
            $Buyers->vilcode =$request->vilcode;
            $Buyers->latitude =$request->latitude;
            $Buyers->longitude =$request->longitude;
            $Buyers->note =$request->note;

            $scaleoperation = $request->scaleoperation;
            if( ($scaleoperation) > 0 ){
                $Buyers->scaleoperation = implode(',', $scaleoperation);
            }

            $cropnames = $request->cropnames;
            if( ($cropnames) > 0 ){
                $Buyers->cropnames = implode(',', $cropnames);
            }
            $youth = $request->age;
            if($youth<=29 && $youth>=18 ){
                $Buyers->youth = 'Youth';
            }else{
                $Buyers->youth = 'Non-Youth';
            }

            $Buyers->save();
        }
        notify()->preset('user-created');
        return redirect()->route('buyers.index')
            ->with('success', 'Buyers created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Buyers
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Buyers $Buyers,$id)
    {
//         $Buyers = Buyers::find($request->id);
        $Buyers = Buyers::find($id);
        // return view('buyers.edit', compact('Buyers'));
        $Provinces = Provinces::pluck('proname', 'procode');
        $Crops = Crops::pluck('cropname', 'id');
        $Users = User::pluck('name', 'id');

        return view('buyers.show', [
            'Buyers' => $Buyers,
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function edit($id, Buyers $Buyers)
    {
        $this->authorize('canupdate', $Buyers);
        $Buyers = Buyers::find($id);
        // return view('buyers.edit', compact('Buyers'));
        $Provinces = Provinces::pluck('proname', 'procode');
        $Crops = Crops::pluck('cropname', 'id');
        $Users = User::pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');

        return view('buyers.edit', [
            'Buyers' => $Buyers,
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buyers $Buyers)
    {
        $this->authorize('canupdate', $Buyers);
        $Buyers = Buyers::find($request->id);
        if($request->name!=NULL){
            $Buyers->userid =$request->userid;
            $Buyers->grantid =$request->grantid;
            $Buyers->name =$request->name;
            $Buyers->namekh =$request->namekh;
            $Buyers->sex  =$request->sex;
            $Buyers->age  =$request->age;
            $Buyers->phone  =$request->phone;
            $Buyers->email  =$request->email;
            $Buyers->lead  =$request->lead;
            $Buyers->businessduration =$request->businessduration;
            $Buyers->sourceofsupply =$request->sourceofsupply;
            $Buyers->subsector =$request->subsector;
            $Buyers->buyertatus =$request->buyertatus;
            $Buyers->statusdate =$request->statusdate;
            $Buyers->saletarget =$request->saletarget;
            $Buyers->mounumber =$request->mounumber;
            $Buyers->moustartdate =$request->moustartdate;
            $Buyers->mouenddate =$request->mouenddate;
            $Buyers->mouextendeddate =$request->mouextendeddate;
            $Buyers->typeofagreement =$request->typeofagreement;
            $Buyers->address =$request->address;
            $Buyers->procode =$request->procode;
            $Buyers->discode =$request->discode;
            $Buyers->comcode =$request->comcode;
            $Buyers->vilcode =$request->vilcode;
            $Buyers->latitude =$request->latitude;
            $Buyers->longitude =$request->longitude;
            $Buyers->note =$request->note;

            $scaleoperation = $request->scaleoperation;
            if( ($scaleoperation) > 0 ){
                $Buyers->scaleoperation = implode(',', $scaleoperation);
            }

            $cropnames = $request->cropnames;
            if( ($cropnames) > 0 ){
                $Buyers->cropnames = implode(',', $cropnames);
            }

            $youth = $request->age;
            if($youth<=29 && $youth>=18 ){
                $Buyers->youth = 'Youth';
            }else{
                $Buyers->youth = 'Non-Youth';
            }

            $Buyers->update();
            notify()->preset('user-updated');
            return redirect()->route('buyers.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Http\Response
     */
    public function hide(Buyers $Buyers,$id)
    {
        $this->authorize('candelete', $Buyers);
        Buyers::where('id', $id)->delete();
        return redirect(route('buyers.index'))->with('success','delete successfully');
    }

    public function destroy(Buyers $Buyers,$id)
    {
        $this->authorize('candelete', $Buyers);
        Buyers::where('id', $id)->delete();
        return redirect(route('buyers.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Buyers  $Buyers
     * @return \Illuminate\Http\Response
     */
    public function approve( Buyers $Buyers, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Buyers);
        // $Buyerss = Buyers::where("id",$request->id)->first();
        $Buyers =Buyers::findOrFail($request->id);
        $Buyers->recordstatus=$request->recordstatus;
        $Buyers->save();
//        notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
}
