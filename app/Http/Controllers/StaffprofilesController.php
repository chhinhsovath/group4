<?php

namespace App\Http\Controllers;

use App\Models\Staffprofiles;
use Illuminate\Http\Request;

class StaffprofilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Staffprofiles  $Staffprofiles
     * @return \Illuminate\Http\Response
     */
    public function show(Staffprofiles $Staffprofiles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Staffprofiles  $Staffprofiles
     * @return \Illuminate\Http\Response
     */
    public function edit(Staffprofiles $Staffprofiles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Staffprofiles  $Staffprofiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staffprofiles $Staffprofiles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Staffprofiles  $Staffprofiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staffprofiles $Staffprofiles)
    {
        //
    }
}
