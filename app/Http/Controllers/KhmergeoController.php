<?php

namespace App\Http\Controllers;

use App\Models\Khmergeo;
// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Symfony\Component\HttpFoundation\Response;
// use DataTables;
// Import and Export
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\KhmergeoImport;
use App\Exports\KhmergeoExport;
use DB;

class KhmergeoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        // $khmergeos = Khmergeo::all();
        $khmergeos = Khmergeo::paginate(100);

        return view('references.khmergeos.index', compact('khmergeos'));
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKhmergeos(Request $request, Khmergeo $khmergeo)
    {
        $data = $khmergeo->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {

                $btnEdit = '<button type="button" class="btn btn-success btn-sm" id="getEditData" data-id="'.$data->id.'">Edit</button> ';
                $btnDelete = '<button class="btn btn-danger btn-sm" onclick="deleteConfirmation('.$data->id.')">Delete</button>';
                    if(auth()->user()->hasPermissionTo('role-edit')){
                            $btn = $btnEdit;
                    }

                    if(auth()->user()->hasPermissionTo('role-delete')){
                            $btn .= $btnDelete;
                    }
                    return $btn;

                })
            ->rawColumns(['Actions'])
            ->make(true);
    }

    public function create()
    {
        $provinces = Khmergeo::select('pro_code','pro_name')->whereRaw('pro_name <> ""')->groupBy('pro_code')->get();
        return view('references.khmergeos.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        $khmergeo = Khmergeo::create($request->all());

        return redirect()->route('index');
    }

    public function edit(khmergeo $khmergeo)
    {

        return view('references.khmergeos.edit', compact('khmergeo'));
    }

    public function update(Request $request, khmergeo $khmergeo)
    {
        $khmergeo->update($request->all());

        return redirect()->route('references.khmergeos.index');
    }

    public function show(khmergeo $khmergeo)
    {

        return view('references.khmergeos.show', compact('khmergeo'));
    }

    public function destroy(khmergeo $khmergeo)
    {

        $khmergeo->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Khmergeo::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

        /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImportExport()
    {
       return view('file-import');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function file_import(Request $request)
    {
        /*

        $import = new KhmergeoImport();
        $import->import($request->file('file')->store('temp'));

        foreach ($import->failures() as $failure) {
            $failure->row(); // row that went wrong
            $failure->attribute(); // either heading key (if using heading row concern) or column index
            $failure->errors(); // Actual error messages from Laravel validator
            $failure->values(); // The values of the row that has failed.
        }
        */

        Excel::import(new KhmergeoImport, $request->file('file')->store('temp'));
        return back()->with('success', 'Data imported successfully');;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function file_export()
    {
        return Excel::download(new KhmergeoExport, 'khmergeo-collection.xlsx');

    }

    public function getProvince(Request $request)
    {
        $provinces = Khmergeo::select('pro_code','pro_name')->whereRaw('pro_name <> ""')->groupBy('pro_code')
            ->pluck('pro_name','pro_code');
        return response()
            ->json($provinces);
    }

    public function getDistrict(Request $request)
    {
        $districts = Khmergeo::select('dis_code','dis_name')->where("pro_code", $request->pro_code)->groupBy('dis_code')
            ->pluck("dis_name", "dis_code");
        return response()
            ->json($districts);
    }

    public function getCommune(Request $request)
    {
        $communes = Khmergeo::select('com_code','com_name')->where("dis_code", $request->dis_code)->groupBy('com_code')
            ->pluck("com_name", "com_code");
        return response()
            ->json($communes);
    }

    public function getVillage(Request $request)
    {
        $villages = Khmergeo::select('vil_code','vil_name')->where("com_code", $request->com_code)->groupBy('vil_code')
            ->pluck("vil_name", "vil_code");
        return response()
            ->json($villages);
    }
}
