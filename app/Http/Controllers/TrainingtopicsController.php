<?php

namespace App\Http\Controllers;

use App\Models\trainingtopics;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TrainingtopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\trainingtopics  $trainingtopics
     * @return \Illuminate\Http\Response
     */
    public function show(trainingtopics $trainingtopics)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\trainingtopics  $trainingtopics
     * @return \Illuminate\Http\Response
     */
    public function edit(trainingtopics $trainingtopics)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\trainingtopics  $trainingtopics
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, trainingtopics $trainingtopics)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\trainingtopics  $trainingtopics
     * @return \Illuminate\Http\Response
     */
    public function destroy(trainingtopics $trainingtopics)
    {
        //
    }
}
