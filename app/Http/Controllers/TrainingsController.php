<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Loans;
use App\Models\Provinces;
use App\Models\Staffprofiles;
use App\Models\Financialcenters;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\DB;

class TrainingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Loans = QueryBuilder::for(Loans::class)
        ->allowedFilters(['id','partners','sex','phone','procode','subsector'])
        ->where('id','>',0)->orderBy('id','desc')->paginate(15);
        $Provinces = Provinces::pluck('proname', 'procode');
        $Users = Users::pluck('name', 'id');
//        return view('loans.index')->with('Loans',$Loans)->with('Provinces',$Provinces);
        return view('loans.index', [
            'Loans' => $Loans,
            'Provinces' => $Provinces,
            'Users' => $Users,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Users = Users::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Financialcenters = Financialcenters::orderBy('name','asc')->pluck('name', 'id');
        return view('loans.create', [
            'Provinces' => $Provinces,
//            'users' => $users,
            'Users' => $Users,
            'Staffprofiles' => $Staffprofiles,
            'Financialcenters' => $Financialcenters,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $Loansdata = $request->all();
        if(Loans::create($Loansdata)){
            #===store=procedure
            $id = DB::getPdo()->lastInsertId();
            DB::select('CALL laravel_loan('.$id.')');
            #===end=store=procedure
            notify()->preset('user-created');
        }else{
            notify()->preset('user-failed');
        }
        return redirect(route('loans.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loans  $Loans
     * @return \Illuminate\Http\Response
     */
    public function show(Loans $Loans)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loans  $Loans
     * @return \Illuminate\Http\Response
     */
    public function edit(Loans $Loans,$id)
    {
        $this->authorize('canupdate', $Loans);
        $Loans = Loans::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Users = Users::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Financialcenters = Financialcenters::orderBy('name','asc')->pluck('name', 'id');
        return view('loans.edit', [
            'Loans' => $Loans,
            'Provinces' => $Provinces,
            'Users' => $Users,
            'Staffprofiles' => $Staffprofiles,
            'Financialcenters' => $Financialcenters,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loans  $Loans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loans $Loans,$id)
    {

        $this->authorize('canupdate', $Loans);
        $Loans = Loans::find($request->id);
        $Loans->userid =$request->userid;
        $Loans->grantid =$request->grantid;
        $Loans->Usersid=$request->Usersid;
        $Loans->subsector=$request->subsector;
        $Loans->procode=$request->procode;
        $Loans->Staffprofilesid=$request->Staffprofilesid;
        $Loans->note=$request->note;
        $Loans->loanvalue=$request->loanvalue;
        $Loans->period=$request->period;
        $Loans->annualrate=$request->annualrate;
        $Loans->datedisbursed=$request->datedisbursed;
        $Loans->bankormfi=$request->bankormfi;
        $Loans->typeofinvestment=$request->typeofinvestment;
        $Loans->purposeofloan=$request->purposeofloan;
        $Loans->inkind=$request->inkind;
        $Loans->female=$request->female;
        $Loans->youth=$request->youth;
        $Loans->total=$request->total;
        $Loans->update();
            #===store=procedure
            $id = DB::getPdo()->lastInsertId();
            DB::select('CALL laravel_loan('.$request->id.')');
            #===end=store=procedure
            notify()->preset('user-updated');
        return redirect(route('loans.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loans  $Loans
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loans $Loans,$id)
    {
        $this->authorize('candelete', $Loans);
        Loans::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(route('loans.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loans  $Loans
     * @return \Illuminate\Http\Response
     */
    public function approve( Loans $Loans, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Loans);
        // $Loanss = Loans::where("id",$request->id)->first();
        $Loans =Loans::findOrFail($request->id);
        $Loans->recordstatus=$request->recordstatus;
        $Loans->save();
//        notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
}
