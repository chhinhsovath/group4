<?php

namespace App\Http\Controllers;

use App\Models\Trainingtypes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TrainingtypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trainingtypes  $Trainingtypes
     * @return \Illuminate\Http\Response
     */
    public function show(Trainingtypes $Trainingtypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trainingtypes  $Trainingtypes
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainingtypes $Trainingtypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trainingtypes  $Trainingtypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainingtypes $Trainingtypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trainingtypes  $Trainingtypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainingtypes $Trainingtypes)
    {
        //
    }
}
