<?php

namespace App\Http\Controllers;
//use App\Http\Controllers\Controller;

use App\Models\AllinoneContacts;
use App\Models\AllinoneContactsNew;
use App\Models\Provinces;
use App\Models\Staffprofiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;

class AllinoneContactsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $invoice = invoice::where('type',1)->whereDate('created_at', $seldate)->get();
//        $count = $invoice->count();

//        $get_buyers =AllinoneContacts::groupBy(['name_en','proname'])->where('tables','=','buyers')->get();
//        $count_buyers = $get_buyers->count();
//
//        $get_suppliers =AllinoneContacts::groupBy(['name_en','proname'])->where('tables','=','suppliers')->get();
//        $count_suppliers = $get_suppliers->count();
//
//        $get_bmps = AllinoneContacts::groupBy(['name_en','proname'])->where('tables','=','bmps')->get();
//        $count_bmps = $get_bmps->count();
//
//        $get_producers = AllinoneContacts::groupBy(['name_en','proname'])->where('tables','=','producers')->get();
//        $count_producers = $get_producers->count();
//
//        $counts=[];
//        $counts['buyers'] = $count_buyers;
//        $counts['suppliers']= $count_suppliers;
//        $counts['bmps']= $count_bmps;
//        $counts['producers']= $count_producers;


        $all_contacts = QueryBuilder::for(AllinoneContacts::class)
            ->allowedFilters(['name_en','phone','proname','disname','comname','vilname','tables','sex','age','note'])
            ->groupBy(['name_en','proname'])
            ->orderBy('id','desc')->paginate(15);
        $phones = AllinoneContacts::groupBy(['phone'])->pluck('phone', 'id');
        $provinces = AllinoneContacts::groupBy(['proname'])->orderBy('proname','asc')->pluck('proname', 'procode');
        $districts = AllinoneContacts::groupBy(['disname'])->orderBy('disname','asc')->pluck('disname', 'discode');
        $communes = AllinoneContacts::groupBy(['comname'])->orderBy('comname','asc')->pluck('comname', 'comcode');
        $villages = AllinoneContacts::groupBy(['vilname'])->orderBy('vilname','asc')->pluck('vilname', 'vilcode');
        return view('all_contacts.index', [
            'all_contacts' => $all_contacts,
            'provinces' => $provinces,
            'districts' => $districts,
            'communes' => $communes,
            'villages' => $villages,
            'phones' => $phones,
//            'counts'=> $counts,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return IlluminateHttpResponse
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('all_contacts.create', [
            'Provinces' => $Provinces,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

        $AllinoneContactsNewData = $request->all();

        $staffprofiles_name = $request->staffprofiles_name;
        if( ($staffprofiles_name) > 0 ){
            $AllinoneContactsNewData['staffprofiles_name'] = implode(',', $staffprofiles_name);
        }

        if(AllinoneContactsNew::create($AllinoneContactsNewData)){
            #===store=procedure
            $last_affected_row_id = DB::getPdo()->lastInsertId();
            DB::select('CALL allinone_contacts_new('.$last_affected_row_id.')');
            #===end=store=procedure
            notify()->preset('user-created');
//            return redirect(route('allinonecontacts.index'));
            return redirect()->back();
        }else{
            notify()->preset('user-failed');
            return redirect(route('allinonecontacts.index'));
        }
    }

    public function restore(){
        DB::select('CALL allinone_contacts()');
        return redirect()->back();
    }

}


