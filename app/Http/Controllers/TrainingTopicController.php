<?php

namespace App\Http\Controllers;

use App\Models\TrainingTopic;
use Illuminate\Http\Request;

class TrainingTopicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainingtopics = TrainingTopic::get();
        return view('references.training-topics.index', compact('trainingtopics'));
    }



    /**
 * Get the data for listing in yajra.
 *'references.training-topics.index'
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    public function getTrainingTopics(Request $request, TrainingTopic $trainingtopic)
    {
        $data = $trainingtopic->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {

            //     $btnEdit = '<button type="button" class="btn btn-success btn-sm" id="getEditArticleData" data-id="'.$data->id.'">Edit</button> ';
            //     $btnDelete= ' <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteArticleModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
            //         if(auth()->user()->hasPermissionTo('role-edit')){
            //                 $btn = $btnEdit;
            //         }

            //         if(auth()->user()->hasPermissionTo('role-delete')){
            //                 $btn .= $btnDelete;
            //         }
            //         return $btn;

            //     })
            // ->rawColumns(['Actions'])
            // ->make(true);
                // return '<button type="button" class="btn btn-success btn-sm" id="getEditArticleData" data-id="'.$data->id.'">Edit</button>
                //         <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteArticleModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
                // $btn="";
                $btnEdit = '<a class="dropdown-item" id="getEditArticleData" data-id="'.$data->id.'">Edit</a>';
                $btnDelete= '<a class="dropdown-item" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteArticleModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</a>';
                // if(auth()->user()->hasPermissionTo('training-topics-edit')){
                //         $btn = $btnEdit;
                // }else{ $btn.=$btn;}

                // if(auth()->user()->hasPermissionTo('training-topics-delete')){
                //         $btn .= $btnDelete;
                // }else{ $btn.=$btn;}
                // return $btn;


                return '
                <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="nc-icon nc-bullet-list-67"></i> </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="top-end" style="position: absolute; transform: translate3d(-26px, -92px, 0px); top: 0px; left: 0px; will-change: transform;">
                        '.$btnEdit.'
                        '.$btnDelete.'
                    </div>
                </div>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, TrainingTopic $trainingtopic)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'note' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $trainingtopic->storeData($request->all());

        return response()->json(['success'=>'Article added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingTopic  $trainingTopic
     * @return \Illuminate\Http\Response
     */
    public function show(TrainingTopic $trainingTopic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingTopic  $trainingTopic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trainingTopic = new TrainingTopic;
        $data = $trainingTopic->findData($id);

        $html = '<div class="form-group">
                    <label for="Title">Title:</label>
                    <input type="text" class="form-control" name="editname" id="editname" value="'.$data->name.'">
                </div>
                <div class="form-group">
                    <label for="Name">Title Khmer:</label>
                    <textarea class="form-control" name="editname_kh" id="editname_kh">'.$data->name_kh.'
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="Name">Note:</label>
                    <textarea class="form-control" name="editnote" id="editnote">'.$data->note.'
                    </textarea>
                </div>';

        return response()->json(['html'=>$html]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingTopic  $trainingTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'note' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $trainingTopic = new TrainingTopic;
        $trainingTopic->updateData($id, $request->all());

        return response()->json(['success'=>'Article updated successfully']);
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingTopic  $trainingTopic
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $trainingTopic = new TrainingTopic;
        $trainingTopic->deleteData($id);

        return response()->json(['success'=>'Article deleted successfully']);
        // return view('references.training-topics.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingTopic  $trainingTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trainingtopic = new TrainingTopic;
        $trainingtopic->deleteData($id);

        return response()->json(['success'=>'Article deleted successfully']);
    }
}
