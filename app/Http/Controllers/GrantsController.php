<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Models\Grant;
use App\Models\Provinces;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\GrantStoreRequest;
use Spatie\QueryBuilder\QueryBuilder;

class GrantsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        DB::select("CALL global()");

        /*
        $Grants = QueryBuilder::for(Grant::class)
        ->allowedFilters(['businessname','ownername','phone','procode'])
        ->where('id','>',0)->orderBy('id','desc')->paginate(15);
        return view('grants.index')->with(['Grants' =>$Grants]);
        */
        if(Auth::user()->userrole =='admin' || Auth::user()->userrole =='masterapprover'){
            // $collectors = collector::orderby('id','desc')->paginate(15);
            $Grants = QueryBuilder::for(Grant::class)
            ->allowedFilters(['businessname','ownername','phone','procode','recordstatus'])
            ->where('id','>',0)->orderBy('id','desc')->paginate(15);
        }
        if(Auth::user()->userrole =='creator'){
            // $Grants = Grant::orderby('id','desc')->paginate(15);
            $Grants = QueryBuilder::for(Grant::class)
            ->allowedFilters(['businessname','ownername','phone','procode','recordstatus'])
            // ->where('Grantid',Auth::user()->Grantid)->orderBy('id','desc')->paginate(15);
            ->where('userid',Auth::id())->orderBy('id','desc')->paginate(15);
        }
        if(Auth::user()->userrole =='ownerapprover'){
            // $Grants = Grant::orderby('id','desc')->paginate(15);
            $Grants = QueryBuilder::for(Grant::class)
            ->allowedFilters(['businessname','ownername','phone','procode','recordstatus'])
            ->where('Grantid',Auth::user()->Grantid)->orderBy('id','desc')->paginate(15);
            // ->where('userid',Auth::id())->orderBy('id','desc')->paginate(15);
        }
        return view('grants.index')->with(['Grants' =>$Grants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::pluck('proname', 'procode');
        return view('grants.create', [
            'Provinces' => $Provinces
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrantStoreRequest $request)
    {
        $Grantdata = $request->all();
        Grant::create($Grantdata);

        //Automatically generate youth base on age
        DB::select("CALL global()");
        notify()->preset('user-created');
        return redirect(route('grants.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Grant  $Grant
     * @return \Illuminate\Http\Response
     */
    public function show(Grant $Grant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Grant  $Grant
     * @return \Illuminate\Http\Response
     */
    public function edit(Grant $Grant, Request $request)
    {
        $this->authorize('updatePolicy', $Grant);
        $Provinces = Provinces::pluck('proname', 'procode');
        return view('grants.edit', [
            'Provinces' => $Provinces,
            'Grant' => $Grant
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Grant  $Grant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grant $Grant)
    {
        $this->authorize('updatePolicy', $Grant);
        $Grant->update($request->all());
        notify()->preset('user-updated');
        return redirect(route('grants.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Grant  $Grant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grant $Grant)
    {
        $this->authorize('deletePolicy', $Grant);
        $Grant->delete();
        notify()->preset('user-deleted');
        return redirect(route('grants.index'));
    }
}
