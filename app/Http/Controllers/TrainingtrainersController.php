<?php

namespace App\Http\Controllers;

use App\Models\Trainingtrainers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TrainingtrainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trainingtrainers  $Trainingtrainers
     * @return \Illuminate\Http\Response
     */
    public function show(Trainingtrainers $Trainingtrainers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trainingtrainers  $Trainingtrainers
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainingtrainers $Trainingtrainers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trainingtrainers  $Trainingtrainers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainingtrainers $Trainingtrainers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trainingtrainers  $Trainingtrainers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainingtrainers $Trainingtrainers)
    {
        //
    }
}
