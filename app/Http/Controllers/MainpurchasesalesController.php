<?php

namespace App\Http\Controllers;

use App\Models\Buyers;
use App\Models\Suppliers;
use App\Models\Crops;
use App\Models\Mainpurchasesales;
use App\Models\Purchasesales;
use App\Models\Provinces;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class MainpurchasesalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Mainpurchasesales = QueryBuilder::for(Mainpurchasesales::class)
            ->allowedFilters(['id','name','namekh','sex','phone','procode'])
            ->orderBy('id','desc')->paginate(15);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $AllBuyers = Buyers::all();

        return view('mainpurchasesales.index', [
            'AllBuyers' => $AllBuyers,
            'Mainpurchasesales' => $Mainpurchasesales,
            'Provinces' => $Provinces,
        ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Crops = Crops::orderBy('cropname','asc')->pluck('cropname', 'id');
        $users = User::orderBy('name','asc')->pluck('name', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Suppliers = Suppliers::orderBy('name','asc')->pluck('name', 'name');

        return view('mainpurchasesales.create', [
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'users' => $users,
            'Buyers' => $Buyers,
            'Suppliers' => $Suppliers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        if($request->buyernames!=NULL){
            $Mainpurchasesales =new Mainpurchasesales();
            $Mainpurchasesales->userid =$request->userid;
            $Mainpurchasesales->grantid =$request->grantid;
            $Mainpurchasesales->buyernames =$request->buyernames;
            $Mainpurchasesales->procode =$request->procode;
            $Mainpurchasesales->discode =$request->discode;
            $Mainpurchasesales->comcode =$request->comcode;
            $Mainpurchasesales->vilcode =$request->vilcode;
            $Mainpurchasesales->startdate =$request->startdate;
            $Mainpurchasesales->enddate =$request->enddate;
            $Mainpurchasesales->note =$request->note;

            $suppliernames = $request->suppliernames;
            if( ($suppliernames) > 0 ){
                $Mainpurchasesales->suppliernames = implode(',', $suppliernames);
            }

            $cropnames = $request->cropnames;
            if( ($cropnames) > 0 ){
                $Mainpurchasesales->cropnames = implode(',', $cropnames);
            }

            $Mainpurchasesales->save();
            notify()->preset('user-created');
        }
        notify()->preset('user-failed');
        return redirect()->route('mainpurchasesales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mainpurchasesales  $Mainpurchasesales
     * @return \Illuminate\Http\Response
     */
    public function show(Mainpurchasesales $Mainpurchasesales,$id, Request $request)
    {
        $this->authorize('canupdate', $Mainpurchasesales);

//        dd($request->itemsid);


//        $Mainpurchasesales = Mainpurchasesales::where('id','=',$id)->first();
//        $main_uuid = $Mainpurchasesales->uuid;

//        dd($Mainpurchasesales->uuid);
        if($request->itemsid>0){
        $Purchasesaleoneitem = Purchasesales::where('id','=',$request->itemsid)->first();
        }else{
            $Purchasesaleoneitem='';
        }


        $Mainpurchasesales = Mainpurchasesales::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Crops = Crops::orderBy('cropname','asc')->pluck('cropname', 'id');
        $Users = User::orderBy('name','asc')->pluck('name', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Suppliers = Suppliers::orderBy('name','asc')->pluck('name', 'name');
        $Purchasesales = Purchasesales::where('Mainpurchasesalesid','=',$id)->orderBy('id','desc')->paginate(15);

        return view('mainpurchasesales.show', [
            'mainpurchasesale' => $Mainpurchasesales,
            'Purchasesales' => $Purchasesales,
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'Users' => $Users,
            'Suppliers' => $Suppliers,
            'Buyers' => $Buyers,
            'Purchasesaleoneitem' => $Purchasesaleoneitem

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mainpurchasesales  $Mainpurchasesales
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Mainpurchasesales $Mainpurchasesales)
    {
        $this->authorize('canupdate', $Mainpurchasesales);
        $Mainpurchasesales = Mainpurchasesales::find($id);
        // return view('mainpurchasesales.edit', compact('Mainpurchasesales'));
//        $Provinces = Provinces::pluck('proname', 'procode');
//        $Crops = Crop::pluck('cropname', 'id');
//        $users = User::pluck('name', 'id');

        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Crops = Crops::orderBy('cropname','asc')->pluck('cropname', 'id');
        $users = User::orderBy('name','asc')->pluck('name', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Suppliers = Suppliers::orderBy('name','asc')->pluck('name', 'name');

        return view('mainpurchasesales.edit', [
            'mainpurchasesale' => $Mainpurchasesales,
            'Provinces' => $Provinces,
            'Crops' => $Crops,
            'users' => $users,
            'Suppliers' => $Suppliers,
            'Buyers' => $Buyers

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mainpurchasesales  $Mainpurchasesales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mainpurchasesales $Mainpurchasesales)
    {
        $this->authorize('canupdate', $Mainpurchasesales);
        $Mainpurchasesales = Mainpurchasesales::findOrFail($request->id);
        if($request->buyernames!=NULL){
            $Mainpurchasesales->userid =$request->userid;
            $Mainpurchasesales->grantid =$request->grantid;
            $Mainpurchasesales->buyernames =$request->buyernames;
            $Mainpurchasesales->procode =$request->procode;
            $Mainpurchasesales->discode =$request->discode;
            $Mainpurchasesales->comcode =$request->comcode;
            $Mainpurchasesales->vilcode =$request->vilcode;
            $Mainpurchasesales->startdate =$request->startdate;
            $Mainpurchasesales->enddate =$request->enddate;
            $Mainpurchasesales->note =$request->note;

            $suppliernames = $request->suppliernames;
            if( ($suppliernames) > 0 ){
                $Mainpurchasesales->suppliernames = implode(',', $suppliernames);
            }

            $cropnames = $request->cropnames;
            if( ($cropnames) > 0 ){
                $Mainpurchasesales->cropnames = implode(',', $cropnames);
            }

            $Mainpurchasesales->update();
            notify()->preset('user-created');
        }
        return redirect()->route('mainpurchasesales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mainpurchasesales  $Mainpurchasesales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mainpurchasesales $Mainpurchasesales,$id)
    {
        $this->authorize('candelete', $Mainpurchasesales);
        Mainpurchasesales::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(route('mainpurchasesales.index'));
    }

    public function approve( Mainpurchasesales $Mainpurchasesales, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Mainpurchasesales);
        $Mainpurchasesales =Mainpurchasesales::findOrFail($request->id);
        $Mainpurchasesales->recordstatus=$request->recordstatus;
        $Mainpurchasesales->save();
        //notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
}
