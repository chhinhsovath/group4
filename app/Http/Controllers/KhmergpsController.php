<?php

namespace App\Http\Controllers;

use App\Models\khmergps;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KhmergpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\khmergps  $khmergps
     * @return \Illuminate\Http\Response
     */
    public function show(khmergps $khmergps)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\khmergps  $khmergps
     * @return \Illuminate\Http\Response
     */
    public function edit(khmergps $khmergps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\khmergps  $khmergps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, khmergps $khmergps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\khmergps  $khmergps
     * @return \Illuminate\Http\Response
     */
    public function destroy(khmergps $khmergps)
    {
        //
    }

    public function getProvince(Request $request)
    {
        $provinces = Khmergps::select('procode','proname')->whereRaw('proname <> ""')->groupBy('procode')
            ->pluck('proname','procode');
        return response()
            ->json($provinces);
    }

    public function getDistrict(Request $request)
    {
        $districts = Khmergps::select('discode','disname')->where("procode", $request->procode)->groupBy('discode')
            ->pluck("disname", "discode");
        return response()
            ->json($districts);
    }

    public function getCommune(Request $request)
    {
        $communes = Khmergps::select('comcode','comname')->where("discode", $request->discode)->groupBy('comcode')
            ->pluck("comname", "comcode");
        return response()
            ->json($communes);
    }

    public function getVillage(Request $request)
    {
        $villages = Khmergps::select('vilcode','vilname')->where("comcode", $request->comcode)->groupBy('vilcode')
            ->pluck("vilname", "vilcode");
        return response()
            ->json($villages);
    }
}
