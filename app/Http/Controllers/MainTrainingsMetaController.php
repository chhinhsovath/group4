<?php

namespace App\Http\Controllers;

use App\Models\MainTrainingsMeta;
use Illuminate\Http\Request;

class MainTrainingsMetaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MainTrainingsMeta  $MainTrainingsMeta
     * @return \Illuminate\Http\Response
     */
    public function show(MainTrainingsMeta $MainTrainingsMeta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MainTrainingsMeta  $MainTrainingsMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(MainTrainingsMeta $MainTrainingsMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainTrainingsMeta  $MainTrainingsMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainTrainingsMeta $MainTrainingsMeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainTrainingsMeta  $MainTrainingsMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainTrainingsMeta $MainTrainingsMeta,$id)
    {
        MainTrainingsMeta::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(url()->previous());
    }

}
