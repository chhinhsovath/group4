<?php

namespace App\Http\Controllers;

use App\Models\Buyers;
//use App\Models\crops;
use App\Models\Investments;
use App\Models\Provinces;
//use App\Models\suppliers;
use App\Models\User;
use App\Models\Staffprofiles;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class InvestmentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        $Investments = QueryBuilder::for(Investments::class)
        ->allowedFilters(['name','proname'])
        ->orderBy('id','desc')->paginate(15);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'id');
//        $allInvestments = Investments::orderBy('name','asc')->pluck('name', 'id');
        return view('investments.index', [
//            'allInvestments' => $allInvestments,
            'Provinces' => $Provinces,
            'Investments' => $Investments,
            'Buyers' => $Buyers
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
//        $users = User::orderBy('name','asc')->pluck('name', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('investments.create', [
            'Provinces' => $Provinces,
//            'users' => $users,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $Investmentsdata = $request->all();
        Investments::create($Investmentsdata);
        notify()->preset('user-created');
        return redirect(route('investments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Investments  $Investments
     * @return \Illuminate\Http\Response
     */
    public function show(Investments $Investments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Investments  $Investments
     * @return \Illuminate\Http\Response
     */
    public function edit(Investments $Investments,$id)
    {
        $Investments = Investments::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        return view('investments.edit', [
            'Investments' => $Investments,
            'Provinces' => $Provinces,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Investments  $Investments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Investments $Investments)
    {
//        dd($request->all());
        $Investments->update($request->all());
        notify()->preset('user-updated');
        return redirect(route('investments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Investments  $Investments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Investments $Investments,$id)
    {
        $this->authorize('candelete', $Investments);
        Investments::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(route('investments.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Investments  $Investments
     * @return \Illuminate\Http\Response
     */
    public function approve( Investments $Investments, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Investments);
        // $Investmentss = Investments::where("id",$request->id)->first();
        $Investments =Investments::findOrFail($request->id);
        $Investments->recordstatus=$request->recordstatus;
        $Investments->save();
//        notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
}
