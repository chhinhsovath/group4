<?php

namespace App\Http\Controllers;

use App\Models\AllinoneContacts;
use App\Models\MainTrainings;
use App\Models\MainTrainingsMeta;
use App\Models\Buyers;
use App\Models\Suppliers;
use App\Models\Bmps;
use App\Models\TrainingTopic;
use App\Models\Provinces;
use Illuminate\Http\Request;
use App\Models\Staffprofiles;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;


class MainTrainingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Maintrainings = QueryBuilder::for(Maintrainings::class)
            ->allowedFilters(['id','training_type','training_topics','procode'])
            ->allowedSorts(['id'])
            ->orderBy('id','DESC')->paginate(15);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');

        return view('maintrainings.index', [
            'Maintrainings' => $Maintrainings,
            'Provinces' => $Provinces,
            'Staffprofiles' => $Staffprofiles,
            'Buyers' => $Buyers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'id');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Trainingtopic = TrainingTopic::orderBy('topics_title_en','asc')->pluck('topics_title_en', 'id');
        return view('maintrainings.create', [
            'Provinces' => $Provinces,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
            'Trainingtopic' => $Trainingtopic,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());


        $Maintrainingsdata = $request->all();

        $topics_title_en = $request->topics_title_en;
        if( ($topics_title_en) > 0 ){
            $Maintrainingsdata['topics_title_en'] = implode(',', $topics_title_en);
        }
        $trainers_name = $request->trainers_name;
        if( ($trainers_name) > 0 ){
            $Maintrainingsdata['trainers_name'] = implode(',', $trainers_name);
        }


        if(Maintrainings::create($Maintrainingsdata)){
            #===store=procedure
            $main_trainings_id = DB::getPdo()->lastInsertId();
            DB::select('CALL fn_update_admin_area('.$main_trainings_id.')');
            #===end=store=procedure
            notify()->preset('user-created');
            return redirect(route('maintrainings.index'));
        }else{
            notify()->preset('user-failed');
            return redirect(route('maintrainings.index'));
        }
//        notify()->preset('user-created');
//        return redirect(route('maintrainings.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function show(Maintrainings $Maintrainings,$id)
    {
        $this->authorize('canupdate', $Maintrainings);

        $all_contacts = QueryBuilder::for(AllinoneContacts::class)
            ->allowedFilters(['name_en','phone','proname','disname','comname','vilname','tables','sex','age'])
            ->groupBy(['name_en','proname'])
            ->orderBy('id','desc')->paginate(25);


        $MainTrainings = MainTrainings::find($id);
        $MainTrainingsMeta = MainTrainingsMeta::where('mts_id','=',$id)->orderBy('id','desc')->get();
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Districts = AllinoneContacts::groupBy(['disname'])->orderBy('disname','asc')->pluck('disname', 'discode');
        $Communes = AllinoneContacts::groupBy(['comname'])->orderBy('comname','asc')->pluck('comname', 'comcode');
        $Villages = AllinoneContacts::groupBy(['vilname'])->orderBy('vilname','asc')->pluck('vilname', 'vilcode');
        $Phones = AllinoneContacts::groupBy(['phone'])->pluck('phone', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Suppliers = Suppliers::orderBy('name','asc')->pluck('name', 'name');
        $Bmps = Bmps::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Trainingtopic = TrainingTopic::orderBy('topics_title_en','asc')->pluck('topics_title_en', 'id');
        return view('maintrainings.show', [
            'MainTrainings' => $MainTrainings,
            'all_contacts' => $all_contacts,
            'Provinces' => $Provinces,
            'Districts' => $Districts,
            'Communes' => $Communes,
            'Villages' => $Villages,
            'Phones' => $Phones,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
            'Trainingtopic' => $Trainingtopic,
            'Suppliers' => $Suppliers,
            'Bmps' => $Bmps,
            'MainTrainingsMeta' => $MainTrainingsMeta,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function edit(Maintrainings $Maintrainings,$id)
    {
        //
        $this->authorize('canupdate', $Maintrainings);
        $Maintrainings = Maintrainings::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Trainingtopic = TrainingTopic::orderBy('topics_title_en','asc')->pluck('topics_title_en', 'id');
        return view('maintrainings.edit', [
            'Maintrainings' => $Maintrainings,
            'Provinces' => $Provinces,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
            'Trainingtopic' => $Trainingtopic,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainTrainings  $MainTrainings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainTrainings $MainTrainings)
    {
//        dd($request->all());
        $this->authorize('canupdate', $MainTrainings);
        $MainTrainings = MainTrainings::find($request->id);

//        $MainTrainingsdata = $request->all();
        $topics_title_en = $request->topics_title_en;
        if( ($topics_title_en) > 0 ){
            $MainTrainings->topics_title_en = implode(',', $topics_title_en);
        }
        $trainers_name = $request->trainers_name;
        if( ($trainers_name) > 0 ){
            $MainTrainings->trainers_name = implode(',', $trainers_name);
        }
        $MainTrainings->userid = $request->userid;
        $MainTrainings->grantid = $request->grantid;
        $MainTrainings->training_type = $request->training_type;
        $MainTrainings->training_to = $request->training_to;
        $MainTrainings->start_date = $request->start_date;
        $MainTrainings->end_date = $request->end_date;
        $MainTrainings->procode = $request->procode;
        $MainTrainings->discode = $request->discode;
        $MainTrainings->comcode = $request->comcode;
        $MainTrainings->vilcode = $request->vilcode;
        $MainTrainings->note = $request->note;
        $MainTrainings->location = $request->location;



        if($MainTrainings->save()){
            #===store=procedure
            $main_trainings_id = $request->id;
            DB::select('CALL fn_update_admin_area('.$main_trainings_id.')');
            #===end=store=procedure
            notify()->preset('user-created');
            return redirect(route('maintrainings.index'));
        }else{
            notify()->preset('user-failed');
            return redirect(route('maintrainings.index'));
        }
//
//        $Maintrainings->update();
//        notify()->preset('user-updated');
//        return redirect(route('maintrainings.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainTrainings  $MainTrainings
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainTrainings $MainTrainings,$id)
    {
        $this->authorize('candelete', $MainTrainings);
        MainTrainings::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(route('maintrainings.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainTrainings  $MainTrainings
     * @return \Illuminate\Http\Response
     */
    public function approve( MainTrainings $MainTrainings, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $MainTrainings);
        // $MainTrainingss = MainTrainings::where("id",$request->id)->first();
        $MainTrainings =MainTrainings::findOrFail($request->id);
        $MainTrainings->recordstatus=$request->recordstatus;
        $MainTrainings->save();
//        notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
    public function addparticipants( MainTrainings $MainTrainings, Request $request,$id)
    {
        $this->authorize('canupdate', $MainTrainings);
        $MainTrainings = MainTrainings::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Districts = AllinoneContacts::groupBy(['disname'])->orderBy('disname','asc')->pluck('disname', 'discode');
        $Communes = AllinoneContacts::groupBy(['comname'])->orderBy('comname','asc')->pluck('comname', 'comcode');
        $Villages = AllinoneContacts::groupBy(['vilname'])->orderBy('vilname','asc')->pluck('vilname', 'vilcode');
        $Phones = AllinoneContacts::groupBy(['phone'])->pluck('phone', 'id');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Suppliers = Suppliers::orderBy('name','asc')->pluck('name', 'name');
        $Bmps = Bmps::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Trainingtopic = TrainingTopic::orderBy('topics_title_en','asc')->pluck('topics_title_en', 'id');
        return view('maintrainings.addparticipants', [
            'MainTrainings' => $MainTrainings,
            'Provinces' => $Provinces,
            'Districts' => $Districts,
            'Communes' => $Communes,
            'Villages' => $Villages,
            'Phones' => $Phones,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
            'Trainingtopic' => $Trainingtopic,
            'Suppliers' => $Suppliers,
            'Bmps' => $Bmps
        ]);
    }
    public function storeparticipant(MainTrainings $MainTrainings , MainTrainingsMeta $MainTrainingsMeta, Request $request)
    {
//        dd($request->all());
        $this->authorize('canupdate', $MainTrainings);

        $participant_info = $request->all();
        MainTrainingsMeta::create($participant_info);

        smilify('success', 'Participant was successfully submitted');
        return redirect(url()->previous());

    }

    public function removeparticipant(MainTrainingsMeta $MainTrainingsMeta ,$id,$mts_id)
    {
        dd($request->all());
//        $this->authorize('candelete', $MainTrainings);
//        MainTrainingsMeta::where('id', $id)->delete();
//        smilify('danger', 'Record was successfully deleted');
//        return redirect(route('maintrainings.show',$mts_id));
    }
}
