<?php

namespace App\Http\Controllers;

use App\Imports\ContactsImport;
use App\Models\AllinoneContactsImport;
use Illuminate\Http\Request;
//use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

class ImportsController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        $preview_data = AllinoneContactsImport::paginate(150);
        return view('all_contacts.upload_file', compact('preview_data'));
    }

    public function finalImport(){

        DB::select('CALL all_in_one_contact_final_import()');
//        DB::select('CALL allinone_contacts()');
//        DB::select('CALL allinone_contacts_import_contacts_new_to_main()');
        smilify('success', 'Contact was successfully imported and cleared tmp');
        return redirect(url()->previous());

    }

    public function clearall(){
        DB::select('CALL all_in_one_contact_clear_tmp_import()');
        smilify('success', 'Contact was successfully cleared');
        return redirect(url()->previous());
    }

//    public function preview()
//    {
//        $preview_data = AllinoneContactsImport::get();
//        return view('all_contacts.upload_file', compact('preview_data'));
//    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function export()
    {
//        return Excel::download(new UsersExport, 'test.xlsx');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function import()
    {

        Excel::import(new ContactsImport,request()->file('file'));

        $userid = auth()->user()->id;
        $grantid = auth()->user()->grantid;

        DB::select('CALL allinone_contacts_update_userid_grantid('.$userid.','.$grantid.')');

        smilify('success', 'Contact was successfully import to temporary table');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AppModelsArticle  $article
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        AllinoneContactsImport::where('id', $id)->delete();
        smilify('success', 'Contact was successfully delete');
        return redirect(url()->previous());
    }

    public function show(){
        DB::select('CALL all_in_one_contact_clear_tmp_import()');
        smilify('success', 'Contact was successfully cleared');
        return redirect(url()->previous());

    }

    public function edit(){

    }

    public function update(){

    }



}
