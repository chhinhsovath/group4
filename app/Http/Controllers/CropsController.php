<?php

namespace App\Http\Controllers;

use App\Models\Crops;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CropsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crops  $Crops
     * @return \Illuminate\Http\Response
     */
    public function show(Crops $Crops)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Crops  $Crops
     * @return \Illuminate\Http\Response
     */
    public function edit(Crops $Crops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Crops  $Crops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crops $Crops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crops  $Crops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crops $Crops)
    {
        //
    }

    public function getCrops(Request $request)
    {
    	$Crops = [];

        if($request->has('q')){
            $search = $request->q;
            $Crops =Crops::select("id", "cropname")
            		->where('cropname', 'LIKE', "%$search%")
            		->get();
        }
        return response()->json($Crops);
    }
}
