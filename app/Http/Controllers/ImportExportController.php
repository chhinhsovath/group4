<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportExportController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        return view('import_export.index');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function export()
    {
        return Excel::download(new UsersExport, 'test.xlsx');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function import()
    {
        Excel::import(new UsersImport,request()->file('file'));

        return back();
    }

}
