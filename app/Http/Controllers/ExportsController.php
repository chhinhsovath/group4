<?php

namespace App\Http\Controllers;

use App\Exports\LoansExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BuyersExport;
use App\Exports\PurchaseSalesExport;
use App\Exports\MainpurchaseSalesExport;
use App\Exports\SuppliersExport;
use App\Exports\ProducersExport;
use App\Exports\BmpsExport;
use App\Exports\InvestmentsExport;
use App\Exports\JobsExport;
use App\Exports\AllinoneContactsExport;
use Carbon\Carbon;

class ExportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buyerExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new BuyersExport, 'Buyers'.$current_date_time.'.xlsx');
    }

    public function purchasesalesExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new PurchaseSalesExport, 'PurchaseSalesExport'.$current_date_time.'.xlsx');
    }
    public function mainpurchasesalesExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new MainpurchasesalesExport, 'MainpurchasesalesExport'.$current_date_time.'.xlsx');
    }

    public function suppliersExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new SuppliersExport, 'SuppliersExport'.$current_date_time.'.xlsx');
    }

    public function producersExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new ProducersExport, 'ProducersExport'.$current_date_time.'.xlsx');
    }

    public function bmpsExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new BmpsExport, 'BmpsExport'.$current_date_time.'.xlsx');
    }
    public function investmentsExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new InvestmentsExport, 'InvestmentsExport'.$current_date_time.'.xlsx');
    }

    public function jobsExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new JobsExport, 'JobsExport'.$current_date_time.'.xlsx');
    }

    public function loansExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new LoansExport, 'LoansExport'.$current_date_time.'.xlsx');
    }
    public function allinonecontactsExport()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        return Excel::download(new AllinoneContactsExport, 'AllinoneContactsExport'.$current_date_time.'.xlsx');
    }
}
