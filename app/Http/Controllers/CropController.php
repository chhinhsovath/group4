<?php

namespace App\Http\Controllers;

use App\Models\Crops;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
// use Brian2694\Toastr\Facades\Toastr;

class CropController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    function __construct()
//    {
        //  $this->middleware('permission:crop-list|crop-create|crop-edit|crop-delete', ['only' => ['index','show']]);
        //  $this->middleware('permission:crop-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:crop-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:crop-delete', ['only' => ['destroy']]);
        //  $this->middleware('auth');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Crops = Crops::paginate(20);
        return view('references.Crops.index',compact('Crops'))
            ->with('i', (request()->input('page', 1) - 1) * 20);
    }

    /**
     * Get the data for listing in yajra.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCrops(Request $request, Crops $Crop)
    {
        $data = $Crop->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {

                // $btnEdit = '<button type="button" class="btn btn-success btn-sm" id="getEditData" data-id="'.$data->id.'">Edit</button> ';
                // $btnDelete= ' <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
                //     if(auth()->user()->hasPermissionTo('role-edit')){
                //             $btn = $btnEdit;
                //     }

                //     if(auth()->user()->hasPermissionTo('role-delete')){
                //             $btn .= $btnDelete;
                //     }
                //     return $btn;

                $btnall = '<a href="Crops/'.$data->id.'" class="btn btn-info btn-link btn-icon btn-sm like" data-id="'.$data->id.'"><i class="fa fa-eye"></i></a>
                           <a href="Crops/'.$data->id.'/edit/" class="btn btn-warning btn-link btn-icon btn-sm edit" data-id="'.$data->id.'"><i class="fa fa-edit"></i></a>
                           <a href="Crops/softdelete/'.$data->id.'" class="btn btn-danger btn-link btn-icon btn-sm delete getDeleteId" data-id="'.$data->id.'"><i class="fa fa-remove"></i></a>
                        ';
                return $btnall;
                })
            ->rawColumns(['Actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('references.Crops.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);
        // Crop::create($request->all());
        // return redirect()->route('Crops.index')->with('success','Crop created successfully.');

        // try{
            $name     = $request->name;
            $name_kh          = $request->name_kh;
            $base_price = $request->base_price;
            $description = $request->description;
            $created_id     = Auth::check() ? Auth::id() : true; // Retrieve the currently authenticated user's ID...

            $Crop = new Crops();
            $Crop->name     = $name;
            $Crop->name_kh           = $name_kh;
            $Crop->base_price = $base_price;
            $Crop->description  = $description;
            $Crop->created_id      = $created_id;
            $Crop->save();

            return redirect()->route('Crops.index')->with('success','Crop created successfully.');

        // }catch(\Exception $e){

        //     return redirect()->route('Crops.index')->with('warning','Failed to create crop.');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crop  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Crops $Crop)
    {
        // return view('Crops.show', compact('crop'));
        // dd($Crop);
        return view('references.Crops.show',compact('crop'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crop  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Crops $Crop)
    {
        return view('references.Crops.edit',compact('crop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crop  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         request()->validate([
            'name' => 'required',
        ]);

        // $product->update($request->all());
        // return redirect()->route('Crops.index')->with('success','Crop updated successfully');
        try{
            // $id           = $request->id;
            $name         = $request->name;
            $name_kh      = $request->name_kh;
            $base_price   = $request->base_price;
            $description  = $request->description;
            $updated_id   = Auth::check() ? Auth::id() : true; // Retrieve the currently authenticated user's ID...

            $update = [
                // 'id'          => $id,
                'name'        => $name,
                'name_kh'     => $name_kh,
                'base_price'  => $base_price,
                'description' => $description,
                'updated_id'  => $updated_id,

            ];
            Crops::where('id',$id)->update($update);
            return redirect()->route('Crops.index')->with('success','Crop updated successfully.');
        }catch(\Exception $e){
            return redirect()->route('Crops.index')->with('warning','Crop updated failed.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crop  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crops $Crop)
    {
        $Crop->delete();

        return redirect()->route('Crops.index')
                        ->with('success','Crop deleted successfully');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crop  $product
     * @return \Illuminate\Http\Response
     */
    public function softdelete($id)
    {
        $update = [
                // 'id'          => $id,
                'deleted_id' => Auth::check() ? Auth::id() : true,
                'deleted_at'  =>  Carbon::now()->toDateTimeString(),

            ];
        Crops::where('id',$id)->update($update);

        return redirect()->route('Crops.index')
                        ->with('success','Crop deleted successfully');
    }
}
