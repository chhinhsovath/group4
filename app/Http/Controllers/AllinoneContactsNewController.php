<?php

namespace App\Http\Controllers;

use App\Models\Provinces;
use Illuminate\Http\Request;
use App\Models\Staffprofiles;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;

class AllinoneContactsNewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        return view('all_contacts.create', [
            'Provinces' => $Provinces
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());


        $Maintrainingsdata = $request->all();

        $topics_title_en = $request->topics_title_en;
        if( ($topics_title_en) > 0 ){
            $Maintrainingsdata['topics_title_en'] = implode(',', $topics_title_en);
        }
        $trainers_name = $request->trainers_name;
        if( ($trainers_name) > 0 ){
            $Maintrainingsdata['trainers_name'] = implode(',', $trainers_name);
        }


        if(Maintrainings::create($Maintrainingsdata)){
            #===store=procedure
            $main_trainings_id = DB::getPdo()->lastInsertId();
            DB::select('CALL fn_update_admin_area('.$main_trainings_id.')');
            #===end=store=procedure
            notify()->preset('user-created');
//            return redirect(route('maintrainings.index'));
            return redirect()->back();
        }else{
            notify()->preset('user-failed');
            return redirect(route('maintrainings.index'));
        }
//        notify()->preset('user-created');
//        return redirect(route('maintrainings.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function show(Maintrainings $Maintrainings)
    {
        //        dd($request->all());

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function edit(Maintrainings $Maintrainings,$id)
    {
        //
        $this->authorize('canupdate', $Maintrainings);
        $Maintrainings = Maintrainings::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Trainingtopic = TrainingTopic::orderBy('topics_title_en','asc')->pluck('topics_title_en', 'id');
        return view('maintrainings.edit', [
            'Maintrainings' => $Maintrainings,
            'Provinces' => $Provinces,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
            'Trainingtopic' => $Trainingtopic,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Maintrainings $Maintrainings)
    {
//        dd($request->all());
        $this->authorize('canupdate', $Maintrainings);
        $Maintrainings = Maintrainings::find($request->id);

//        $Maintrainingsdata = $request->all();
        $topics_title_en = $request->topics_title_en;
        if( ($topics_title_en) > 0 ){
            $Maintrainings->topics_title_en = implode(',', $topics_title_en);
        }
        $trainers_name = $request->trainers_name;
        if( ($trainers_name) > 0 ){
            $Maintrainings->trainers_name = implode(',', $trainers_name);
        }
        $Maintrainings->userid = $request->userid;
        $Maintrainings->grantid = $request->grantid;
        $Maintrainings->training_type = $request->training_type;
        $Maintrainings->training_to = $request->training_to;
        $Maintrainings->start_date = $request->start_date;
        $Maintrainings->end_date = $request->end_date;
        $Maintrainings->procode = $request->procode;
        $Maintrainings->discode = $request->discode;
        $Maintrainings->comcode = $request->comcode;
        $Maintrainings->vilcode = $request->vilcode;
        $Maintrainings->note = $request->note;
        $Maintrainings->location = $request->location;



        if($Maintrainings->save()){
            #===store=procedure
            $main_trainings_id = $request->id;
            DB::select('CALL fn_update_admin_area('.$main_trainings_id.')');
            #===end=store=procedure
            notify()->preset('user-created');
            return redirect(route('maintrainings.index'));
        }else{
            notify()->preset('user-failed');
            return redirect(route('maintrainings.index'));
        }
//
//        $Maintrainings->update();
//        notify()->preset('user-updated');
//        return redirect(route('maintrainings.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Maintrainings $Maintrainings,$id)
    {
        $this->authorize('candelete', $Maintrainings);
        Maintrainings::where('id', $id)->delete();
        smilify('danger', 'Record was successfully deleted');
        return redirect(route('maintrainings.index'))->with('success','Buyer deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Maintrainings  $Maintrainings
     * @return \Illuminate\Http\Response
     */
    public function approve( Maintrainings $Maintrainings, Request $request)
    {
        // dd($request);
        $this->authorize('canapprove', $Maintrainings);
        // $Maintrainingss = Maintrainings::where("id",$request->id)->first();
        $Maintrainings =Maintrainings::findOrFail($request->id);
        $Maintrainings->recordstatus=$request->recordstatus;
        $Maintrainings->save();
//        notify()->preset('user-approved');
        smilify('success', 'Record was successfully approved');
        return redirect(url()->previous());

    }
    public function addparticipants( Maintrainings $Maintrainings, Request $request,$id)
    {
        $this->authorize('canupdate', $Maintrainings);
        $Maintrainings = Maintrainings::find($id);
        $Provinces = Provinces::orderBy('proname','asc')->pluck('proname', 'procode');
        $Buyers = Buyers::orderBy('name','asc')->pluck('name', 'name');
        $Suppliers = Suppliers::orderBy('name','asc')->pluck('name', 'name');
        $Bmps = Bmps::orderBy('name','asc')->pluck('name', 'name');
        $Staffprofiles = Staffprofiles::orderBy('name','asc')->pluck('name', 'id');
        $Trainingtopic = TrainingTopic::orderBy('topics_title_en','asc')->pluck('topics_title_en', 'id');
        return view('maintrainings.addparticipants', [
            'Maintrainings' => $Maintrainings,
            'Provinces' => $Provinces,
            'Buyers' => $Buyers,
            'Staffprofiles' => $Staffprofiles,
            'Trainingtopic' => $Trainingtopic,
            'Suppliers' => $Suppliers,
            'Bmps' => $Bmps
        ]);
    }
}
