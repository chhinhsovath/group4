<?php

namespace App\Http\Controllers;

//use App\Models\communes;
//use App\Models\districts;
use App\Models\Khmergeo;
use App\Models\Provinces;
use App\Http\Controllers\Controller;
//use App\Models\villages;
use Illuminate\Http\Request;

class ProvincesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provinces  $Provinces
     * @return \Illuminate\Http\Response
     */
    public function show(Provinces $Provinces)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provinces  $Provinces
     * @return \Illuminate\Http\Response
     */
    public function edit(Provinces $Provinces)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provinces  $Provinces
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provinces $Provinces)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provinces  $Provinces
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provinces $Provinces)
    {
        //
    }

    public function getProvince(Request $request)
    {
        $Provinces = Provinces::select('procode','proname')->whereRaw('proname <> ""')->groupBy('procode')
            ->pluck('proname','procode');
        return response()
            ->json($Provinces);
    }

//    public function getDistrict(Request $request)
//    {
//        $districts = districts::select('discode','disname')->where("procode", $request->procode)->groupBy('discode')
//            ->pluck("disname", "discode");
//        return response()
//            ->json($districts);
//    }
//
//    public function getCommune(Request $request)
//    {
//        $communes = communes::select('comcode','comname')->where("discode", $request->discode)->groupBy('comcode')
//            ->pluck("comname", "comcode");
//        return response()
//            ->json($communes);
//    }
//
//    public function getVillage(Request $request)
//    {
//        $villages = villages::select('vilcode','vilname')->where("comcode", $request->comcode)->groupBy('vilcode')
//            ->pluck("vilname", "vilcode");
//        return response()
//            ->json($villages);
//    }
}
