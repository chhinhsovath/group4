<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buyers extends Model
{
     use HasFactory, SoftDeletes;

     protected $dates = ['created_at','updated_at','deleted_at'];

     protected $table = 'buyers';
     protected $fillable = [
        'userid',
        'grantid',
        'name',
        'namekh',
        'sex',
        'age',
        'phone',
        'email',
        'lead',
        'businessduration',
        'scaleoperation',
        'sourceofsupply',
        'cropnames',
        'subsector',
        'buyertatus',
        'statusdate',
        'saletarget',
        'mounumber',
        'moustartdate',
        'mouenddate',
        'mouextendeddate',
        'typeofagreement',
        'address',
        'procode',
        'discode',
        'comcode',
        'vilcode',
        'latitude',
        'longitude',
        'note'
     ];

    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

    public function districts()
    {
        return $this->hasOne(Districts::class,'discode','discode');
    }

    public function communes()
    {
        return $this->hasOne(Communes::class,'comcode','comcode');
    }

    public function villages()
    {
        return $this->hasOne(Villages::class,'vilcode','vilcode');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'userid','id');
    }
}
