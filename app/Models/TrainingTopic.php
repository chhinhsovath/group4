<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingTopic extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['created_at','updated_at'];
    protected $table = 'training_topics';

    protected $fillable = [
        'topics_title_en',
        'topics_title_kh',
        'note',
    ];
}
