<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseSaleProvinces extends Model
{
    use HasFactory;

    protected $table = 'purchase_sale_provinces';
}
