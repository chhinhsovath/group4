<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suppliers extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['created_at','updated_at'];

    protected $table = 'suppliers';
    protected $fillable = [
        'userid',
        'grantid',
        'name',
        'namekh',
        'sex',
        'age',
        'phone',
        'email',
        'lead',
        'areaplantedha',
        'smallholder',
        'cropnames',
        'subsector',
        'buyernames',
        'address',
        'procode',
        'discode',
        'comcode',
        'vilcode',
        'latitude',
        'longitude',
        'note'
    ];

    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

    public function districts()
    {
        return $this->hasOne(Districts::class,'discode','discode');
    }

    public function communes()
    {
        return $this->hasOne(Communes::class,'comcode','comcode');
    }

    public function villages()
    {
        return $this->hasOne(Villages::class,'vilcode','vilcode');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'userid','id');
    }
}
