<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllinoneContactsImport extends Model
{
    use HasFactory;
    protected $dates = ['created_at','updated_at'];
    protected $table = 'allinone_contacts_tmp_import';

    public $fillable = [
        'name_en',
        'name_kh',
        'sex',
        'age',
        'phone',
        'email',
//        'subsector',
//        'procode',
        'proname',
//        'discode',
        'disname',
//        'comcode',
        'comname',
//        'vilcode',
        'vilname',
//        'address',
        'note',
        'tables',
//        'staffprofilesid',
//        'staffprofiles_name',
//        'latitude',
//        'longtitude',
//        'userid',
//        'grantid',
    ];

}
