<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loans extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['created_at','updated_at','deleted_at'];

    public $fillable = [
        'userid',
        'grantid',
        'buyersid',
        'subsector',
        'procode',
        'staffprofilesid',
        'note',
        'loanvalue',
        'period',
        'annualrate',
        'datedisbursed',
        'bankormfi',
        'typeofinvestment',
        'purposeofloan',
        'inkind',
        'female',
        'youth',
        'total',
    ];


    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

//    public function districts()
//    {
//        return $this->hasOne(districts::class,'discode','discode');
//    }
//
//    public function communes()
//    {
//        return $this->hasOne(communes::class,'comcode','comcode');
//    }
//
//    public function villages()
//    {
//        return $this->hasOne(villages::class,'vilcode','vilcode');
//    }

    public function leads()
    {
        return $this->belongsTo(Staffprofiles::class,'staffprofilesid','id');
    }

    public function buyers()
    {
        return $this->belongsTo(Buyers::class,'buyersid','id');
    }
}
