<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProvinceProjectYearsSale extends Model
{
    use HasFactory;

    protected $table = 'province_project_years_sale';
}

/*
"Province"	        "PY1"	            "PY2"	            "PY3"	                "PY4"	                "PY5"
"Banteay Meanchey"	"0"	                "0"	                "0"	                    "7762.5000015000005"	"0"
"Battambang"	    "1388454.522800189"	"4932795.56631266"	"6307693.9043539995"	"9403501.236320984"	    "2200180.2465353794"
"Kampong Speu"	    "0"	                "0"	                "0"	                    "813551.5285425"	    "201842.75513312596"
"Kampong Thom"	    "687336.68"	        "4275622.3601"	    "3705233.437482"	    "6200813"	            "3332620"
"Pailin"	        "0"	                "0"	                "49125"	                "0"	                    "14400"
"Phnom Penh"	    "0"	                "0"	                "199295.5675"	        "330362.056840319"	    "789477.4399402764"
"Preah Vihear"	    "0"	                "0"	                "0"	                    "0"	                    "0"
"Pursat"	        "8000"	            "1180987.8352"	    "1211761.25"	        "1107247.2"	            "59225"
"Siem Reap"	        "549212.6357440001"	"955663.1809249996"	"1413066.695"	        "2022792.5780000002"	"891683.166018653"
 */
