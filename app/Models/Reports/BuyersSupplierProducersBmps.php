<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyersSupplierProducersBmps extends Model
{
    use HasFactory;

    protected $table = 'dashboards';
}
