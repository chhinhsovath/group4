<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopFiveCrop extends Model
{
    use HasFactory;

    protected $table = 'top_five_crops';
}
