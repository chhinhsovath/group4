<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseSaleSubsector extends Model
{
    use HasFactory;

    protected $table = 'purchase_sale_subsector';
}
