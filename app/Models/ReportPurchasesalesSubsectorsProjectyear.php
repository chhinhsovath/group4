<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportPurchasesalesSubsectorsProjectyear extends Model
{
    use HasFactory;

    protected $table = 'report_purchasesales_subsectors_projectyear';
}
