<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportPurchasesalesProvincesProjectyear extends Model
{
    use HasFactory;

    protected $table = 'report_purchasesales_provinces_projectyear';
}
