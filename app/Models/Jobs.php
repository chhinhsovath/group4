<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jobs extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['created_at','updated_at','deleted_at'];

    public $fillable = [
        'userid',
        'grantid',
        'buyersid',
        'name',
        'subsector',
        'procode',
        'staffprofilesid',
        'staffprofilename',
        'startdate',
        'enddate',
        'note',
        'totalexistingfulltimestaff',
        'totalnewfulltimestaff',
        'totalfulltimeyouth',
        'totalfulltimefemale',
        'datehirefulltimestaff',
        'fulltimenote',
        'totalparttimestaff',
        'numberofdaysparttimestaff',
        'totalpartimefemale',
        'totalparttimeyouth',
        'datehireparttimestaff',
        'parttimenote',
    ];


    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

//    public function districts()
//    {
//        return $this->hasOne(districts::class,'discode','discode');
//    }
//
//    public function communes()
//    {
//        return $this->hasOne(communes::class,'comcode','comcode');
//    }
//
//    public function villages()
//    {
//        return $this->hasOne(villages::class,'vilcode','vilcode');
//    }

    public function leads()
    {
        return $this->hasOne(Staffprofiles::class,'id','staffprofilesid');
    }

    public function buyers()
    {
        return $this->hasOne(Buyers::class,'id','buyersid');
    }
}
