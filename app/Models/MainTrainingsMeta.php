<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainTrainingsMeta extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $table = 'main_trainings_meta';
    protected $fillable = [
        'userid',
        'grantid',
        'allinone_uuid',
        'mts_id',
        'mts_uuid',
    ];

    public function info()
    {
        return $this->belongsTo(AllinoneContacts::class,'allinone_uuid','uuid');
    }
}
