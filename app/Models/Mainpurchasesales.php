<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mainpurchasesales extends Model
{
    use HasFactory, SoftDeletes;

    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

    public function districts()
    {
        return $this->hasOne(Districts::class,'discode','discode');
    }

    public function communes()
    {
        return $this->hasOne(Communes::class,'comcode','comcode');
    }

    public function villages()
    {
        return $this->hasOne(Villages::class,'vilcode','vilcode');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'userid','id');
    }
}
