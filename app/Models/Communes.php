<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Communes extends Model
{
    use HasFactory;
    protected $table = "communes";

    public $fillable = [
        'comcode',
        'comname'
    ];
}
