<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AllinoneContacts extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['created_at','updated_at'];
    protected $table = 'allinone_contacts';

    public $fillable = [
        'name_en',
        'name_kh',
        'sex',
        'age',
        'phone',
        'email',
        'subsector',
        'procode',
        'proname',
        'discode',
        'disname',
        'comcode',
        'comname',
        'vilcode',
        'vilname',
        'address',
        'note',
        'staffprofilesid',
        'staffprofiles_name',
        'latitude',
        'longtitude',
        'userid',
        'grantid',
    ];


    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

        public function districts()
    {
        return $this->hasOne(Districts::class,'discode','discode');
    }

        public function communes()
    {
        return $this->hasOne(Communes::class,'comcode','comcode');
    }

        public function villages()
    {
        return $this->hasOne(Villages::class,'vilcode','vilcode');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
