<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainTrainings extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $table ='main_trainings';
    protected $fillable = [
        'userid',
        'grantid',
        'training_type',
        'training_to',
        'training_location',
        'start_date',
        'end_date',
        'topics_title_en',
        'trainers_name',
        'procode',
        'discode',
        'comcode',
        'vilcode',
        'note',
        'location'
    ];

    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

    public function districts()
    {
        return $this->hasOne(Districts::class,'discode','discode');
    }

    public function communes()
    {
        return $this->hasOne(Communes::class,'comcode','comcode');
    }

    public function villages()
    {
        return $this->hasOne(Villages::class,'vilcode','vilcode');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'userid','id');
    }
}
