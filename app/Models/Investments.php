<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Investments extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['created_at','updated_at','deleted_at'];

    public $fillable = [
        'userid',
        'grantid',
        'buyersid',
//        'name',
        'subsector',
        'procode',
//        'proname',
        'staffprofilesid',
        'staffprofilename',
        'startdate',
        'enddate',
        'note',
        'workingcapitaldate',
        'workingcapitalamount',
        'workingcapitaltnote',
        'capitalinvestmentdate',
        'capitalinvestmentamount',
        'capitalinvestmentnote',
        'supplycreditdate',
        'supplycreditamount',
        'supplycreditnote',
        'operationexpensedate',
        'operationexpenseamount',
        'operationexpensenote',
    ];





    public function provinces()
    {
        return $this->hasOne(Provinces::class,'procode','procode');
    }

    public function districts()
    {
        return $this->hasOne(Districts::class,'discode','discode');
    }

    public function communes()
    {
        return $this->hasOne(Communes::class,'comcode','comcode');
    }

    public function villages()
    {
        return $this->hasOne(Villages::class,'vilcode','vilcode');
    }

    public function leads()
    {
        return $this->belongsTo(Staffprofiles::class,'staffprofilesid','id');
    }

    public function buyers()
    {
        return $this->belongsTo(buyers::class,'buyersid','id');
    }
}
