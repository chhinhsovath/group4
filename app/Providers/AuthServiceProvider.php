<?php

namespace App\Providers;

use App\Models\MainTrainings;
use App\Models\user;
use App\Models\buyers;
use App\Policies\BuyersPolicy;
use App\Models\suppliers;
use App\Policies\MainTrainingsPolicy;
use App\Policies\SuppliersPolicy;
use App\Models\mainpurchasesales;
use App\Policies\MainpurchasesalesPolicy;
use App\Models\purchasesales;
use App\Policies\PurchasesalesPolicy;
use App\Models\investments;
use App\Policies\InvestmentsPolicy;
use App\Models\jobs;
use App\Policies\JobsPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        buyers::class => BuyersPolicy::class,
        suppliers::class => SuppliersPolicy::class,
        mainpurchasesales::class => MainpurchasesalesPolicy::class,
        purchasesales::class => PurchasesalesPolicy::class,
        investments::class => InvestmentsPolicy::class,
        jobs::class => JobsPolicy::class,
        MainTrainings::class => MainTrainingsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //Set user to access CRUD
        Gate::define('admin', function(User $user) {
            return $user->roles === 'admin';
        });
        // create-view-edit only her belongs but not delete or approve
        Gate::define('creator', function(User $user) {
            return $user->roles === 'creator';
        });
        // create-view-edit-approve only her belongs and her team but not delete
        Gate::define('approver', function(User $user) {
            return $user->roles === 'approver';
        });

        // can access only limited content and can not do any CRUD
        Gate::define('guest', function(User $user) {
            return $user->roles === 'guest';
        });
    }
}
