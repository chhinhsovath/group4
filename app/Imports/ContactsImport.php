<?php

namespace App\Imports;

use App\Models\AllinoneContactsImport;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContactsImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new AllinoneContactsImport([
            'name_en' => $row['name_en'],
            'name_kh' => $row['name_kh'],
            'sex' => $row['sex'],
            'age' => $row['age'],
            'phone' => $row['phone'],
            'email' => $row['email'],
            'proname' => $row['proname'],
            'disname' => $row['disname'],
            'comname' => $row['comname'],
            'vilname' => $row['vilname'],
            'note' => $row['note'],
            'tables' => $row['tables']
        ]);
    }
}
