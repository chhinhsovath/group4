@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'edit-suppliers'
])
@section('content')

    <div class="content">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    {{-- containter --}}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--  end container--}}
                </div>
                <form action="{{ route('suppliers.update',$Suppliers->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" class="form-control" name="id" value="{{ $Suppliers->id }}">
                    <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
                    <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Personel </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 550px;">
                                    {{-- start --}}
                                    <label class="form-label">Name</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ $Suppliers->name }}" name="name" id="name" placeholder="Name">
                                    </div>

                                    <label class="form-label">Name Khmer</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control khmer​" value=" {{ $Suppliers->namekh }} "name="namekh" placeholder="Name Khmer">
                                    </div>

                                    <label class="form-label">Sex </label>
                                    <div class="form-group">
                                        <select class="form-control" name="sex"  style=" height:45px;">
                                            <option value='{{ $Suppliers->sex }}' selected>{{ $Suppliers->sex }}</option>
                                            <option value='Male'>Male</option>
                                            <option value='Female'>Female</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Age</label>
                                    <div class="form-group">
                                        <input type="number" class="form-control" value="{{ $Suppliers->age }}" name="age" placeholder="age">
                                    </div>

                                    <label class="form-label">Phone</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ $Suppliers->phone }}" name="phone" placeholder="phone">
                                    </div>

                                    <br>
                                    {{-- end --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Farming Information </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 550px;">

                                    <label class="form-label">Area Planted (ha)</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ $Suppliers->areaplantedha }}" name="areaplantedha" placeholder="area planted (ha)">
                                    </div>

                                    <label class="form-label">Smallholder </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="smallholder" value="{{ $Suppliers->smallholder }}" placeholder="smallholder">
                                    </div>

                                    <label class="form-label">Crop Names</label>
                                    <div class="form-group">
                                        <select name="cropnames[]" multiple class="form-control select2multipls">
                                            @if ($Suppliers->cropnames != "")
                                                @foreach (array_map('trim', explode(',', $Suppliers->cropnames)) as $cropnames)
                                                    <option value="{{ trim($cropnames,'"')}}" selected> {{ trim($cropnames,'"')}}</option>
                                                @endforeach
                                            @endif
                                            @foreach($Crops as $cropid => $cropname)
                                                <option value="{{$cropname}}"> {{$cropname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="form-label">Sub Sector </label>
                                    <div class="form-group">
                                        <select class="form-control" name="subsector" id="subsector" style=" height:45px;">
                                            <option value="{{$Suppliers->subsector}}" selected>{{$Suppliers->subsector}}</option>
                                            <option value="Cashew">Cashew</option>
                                            <option value="Longan">Longan</option>
                                            <option value="Mango">Mango</option>
                                            <option value="Mixed Fruit">Mixed Fruit</option>
                                            <option value="Pepper Corn">Pepper Corn</option>
                                            <option value="Vegetable">Vegetable</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Buyers</label>
                                    <div class="form-group">
                                        <select name="buyernames[]" multiple class="form-control select2multipls">
                                            @if ($Suppliers->buyernames != "")
                                                @foreach (array_map('trim', explode(',', $Suppliers->buyernames)) as $buyernames)
                                                    <option value="{{ trim($buyernames,'"')}}" selected> {{ trim($buyernames,'"')}}</option>
                                                @endforeach
                                            @endif
                                            @foreach($Buyers as $id => $name)
                                                <option value="{{$name}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <br>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Extra Information </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 550px;">
                                    {{-- start --}}

                                    <label class="form-label">E-mail</label>
                                    <div class="form-group">
                                        <input type="email" class="form-control" value="{{$Suppliers->email}}" name="email" placeholder="e-mail">
                                    </div>

                                    <label class="form-label">Address Detail </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{$Suppliers->address}}" name="address" placeholder="address ">
                                    </div>

                                    <label class="form-label">Note </label>
                                    <div class="form-group">
                                        <input id="note" class="form-control" style="width: 100%;" value="{{$Suppliers->note}}" name="note"></input>
                                    </div>

                                    <label class="form-label">Lead</label>
                                    <div class="form-group">
                                        <select id="lead" name="lead" class="form-control"  style=" height:45px;">
                                            <option value="{{$Suppliers->lead}}" selected>{{$Suppliers->lead}}</option>
                                            @foreach($Users as $id => $name)
                                                <option value="{{$name}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <br>
                                    {{-- end --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Geographic </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 550px;">
                                    <label class="form-label">Province</label>
                                    <div class="form-group">
                                        <select id="procode" name="procode" class="form-control"  style=" height:45px;">
                                            <option value="{{$Suppliers->procode}}" selected>{{$Suppliers->provinces->proname}}</option>
                                            @foreach($Provinces as $procode => $proname)
                                                <option value="{{$procode}}"> {{$proname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="form-label">District</label>
                                    <div class="form-group">
                                        <select id="discode" name="discode" class="form-control"  style=" height:45px;">
                                            <option value="{{$Suppliers->discode}}" selected>{{$Suppliers->districts->disname}}</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Commune</label>
                                    <div class="form-group">
                                        <select id="comcode" name="comcode" class="form-control"  style=" height:45px;">
                                            <option value="{{$Suppliers->comcode}}" selected>{{$Suppliers->communes->comname}}</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Village</label>
                                    <div class="form-group">
                                        <select id="vilcode" name="vilcode" class="form-control"  style=" height:45px;">
                                            <option value="{{$Suppliers->vilcode}}" selected>{{$Suppliers->villages->vilname}}</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Latitude</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{$Suppliers->latitude}}" name="latitude" placeholder="latitude">
                                    </div>
                                    <label class="form-label">Longitude</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{$Suppliers->longitude}}" name="longitude" placeholder="longitude">
                                    </div>

                                    <br>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="pull-left">
                            <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary ms-auto">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2multipls').select2({
                placeholder: "Select multiple",
                allowClear: true,
                tags: true,
                tokenSeparators: [',',]
            });
        });

        $('#procode').change(function() {
            var procode = $(this).val();
            $("#discode").empty();
            $("#comcode").empty();
            $("#vilcode").empty();
            if (procode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getDistrict')}}/" + procode,
                    success: function(res) {
                        if (res) {
                            $("#discode").empty();
                            $("#discode").append('<option>Select District</option>');
                            $.each(res, function(key, value) {
                                $("#discode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#discode").empty();
                        }
                    }
                });
            } else {
                $("#discode").empty();
                $("#comcode").empty();
                $("#vilcode").empty();
            }
        });
        $('#discode').on('change', function() {
            var discode = $(this).val();
            $("#comcode").empty();
            $("#vilcode").empty();
            if (discode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getCommune')}}/" + discode,
                    success: function(res) {
                        if (res) {
                            $("#comcode").empty();
                            $("#comcode").append('<option>Select Commune</option>');
                            $.each(res, function(key, value) {
                                $("#comcode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#comcode").empty();
                        }
                    }
                });
            } else {
                $("#comcode").empty();
                $("#vilcode").empty();
            }

        });
        $('#comcode').on('change', function() {
            var comcode = $(this).val();
            $("#vilcode").empty();
            if (comcode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getVillage')}}/" + comcode,
                    success: function(res) {
                        if (res) {
                            $("#vilcode").empty();
                            $("#vilcode").append('<option>Select Village</option>');
                            $.each(res, function(key, value) {
                                $("#vilcode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#vilcode").empty();
                        }
                    }
                });
            } else {
                // $("#vilcode").empty();
            }

        });
    </script>
@endpush
