@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'show-suppliers'
])
@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

{{--                        <div class="row">--}}
{{--                            <div class="col">Label 1</div>--}}
{{--                            <div class="col">Value 1</div>--}}
{{--                            <div class="col">Label 2</div>--}}
{{--                            <div class="col">Value 2</div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col">Label 1</div>--}}
{{--                            <div class="col">Value 1</div>--}}
{{--                        </div>--}}

                        <div class="row">
                            {{-- containter --}}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-3">
                                <div class="card card-sm">
                                    <div class="card-body" style="min-height: 400px;">
                                        <h4 class="card-title">Personal</h4>
{{--                                        <div class="row">--}}
{{--                                            <div class="col">Name</div>--}}
{{--                                            <div class="col"><strong>{{$Suppliers->name}}</strong></div>--}}
{{--                                        </div>--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col">Name Khmer</div>--}}
{{--                                            <div class="col"><strong>{{$Suppliers->namekh}}</strong></div>--}}
{{--                                        </div>--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col">Sex</div>--}}
{{--                                            <div class="col"><strong>{{$Suppliers->sex}}</strong></div>--}}
{{--                                        </div>--}}

                                        <table class="table">
                                            <tr>
                                                <td>Name</td>
                                                <td><strong>{{$Suppliers->name}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Name Khmer</td>
                                                <td class="khmer"><strong>{{$Suppliers->namekh}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Sex</td>
                                                <td><strong>{{$Suppliers->sex}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Age</td>
                                                <td><strong>{{$Suppliers->age}}</strong> years old</td>
                                            </tr>
                                            <tr>
                                                <td>Youth</td>
                                                <td><strong>{{$Suppliers->youth}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td><strong>{{$Suppliers->phone}}</strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="card card-sm">
                                    <div class="card-body" style="min-height: 400px;">
                                        <h4 class="card-title"> Farming Information </h4>
                                        <table class="table">
                                            <tr>
                                                <td>Area Planted (ha)</td>
                                                <td><strong>{{$Suppliers->areaplantedha}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Crop Names</td>
                                                <td><strong>{{$Suppliers->cropnames}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Sub Sector</td>
                                                <td><strong>{{$Suppliers->subsector}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Buyers</td>
                                                <td><strong>{{$Suppliers->buyernames}}</strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="card card-sm">
                                    <div class="card-body" style="min-height: 400px;">
                                        <h4 class="card-title"> Extra Information </h4>
                                        <table class="table">
                                            <tr>
                                                <td>E-mail</td>
                                                <td><strong>{{$Suppliers->email}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td><strong>{{$Suppliers->address}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Note</td>
                                                <td><strong>{{$Suppliers->note}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Lead</td>
                                                <td><strong>{{$Suppliers->lead}}</strong></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <div class="col-3">
                                <div class="card card-sm">
                                    <div class="card-body" style="min-height: 400px;">
                                        <h4 class="card-title"> Geographic </h4>
                                        <table class="table">
                                            <tr>
                                                <td>Geography</td>
                                                <td><strong>{{$Suppliers->provinces->proname}}, {{$Suppliers->districts->disname}}, {{$Suppliers->communes->comname}}, {{$Suppliers->villages->vilname}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Lattitude</td>
                                                <td><strong>{{$Suppliers->lattitude}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Longtitude</td>
                                                <td><strong>{{$Suppliers->longtitude}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Created By</td>
                                                <td><strong>{{$Suppliers->users->name}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Created Date</td>
                                                <td><strong>{{$Suppliers->created_at}}</strong></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="pull-left">
                                <a href="{{ url()->previous() }}" class="btn btn-defaultk ms-auto">Back</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('suppliers.edit',$Suppliers->id)}}" class="btn btn-primary ms-auto">Edit</a>
                            </div>
                        </div>
                    </div>
                    {{--  end container--}}

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
