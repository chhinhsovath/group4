@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-suppliers'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> List of Suppliers </h4>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="{{ route('suppliers.create') }}"> Create New Supplier </a>
                        </div>
                    </div>
                    <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px;">
                            <form action="{{ route('suppliers.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <select class="form-control" id="name" name="filter[id]" style=" height:45px;">
                                                <option value="">Name</option>
                                                @foreach($AllSuppliers as $Supplier)
                                                    <option value="{{$Supplier->id}}">{{$Supplier->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Name Khmer</label>
                                            <input type="text" class="form-control" name="filter[namekh]" placeholder="Name Khmer">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Sex</label>
                                            <select class="form-control" id="sex" name="filter[sex]" style=" height:45px;">
                                                <option value="">Sex</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Phone</label>
                                            <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control" id="name" name="filter[procode]" style=" height:45px;">
                                                <option value="">Province</option>
                                                @foreach($Provinces as $procode => $proname)
                                                    <option value="{{$procode}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px;"> Search </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="{{ route('suppliers-export') }}" class="form-control btn btn-warning" style="margin-top: 24px;">Export</a>
                                        </div>
                                        {{-- <form action="{{route('export.buyers')}}" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <input type="hidden" name="export" value="buyers">
                                                <button class="form-control btn btn-warning" style="margin-top: 24px;" type="submit">Export</button>
                                            </div>
                                        </form> --}}
                                    </div>

                                </div>
                                </form>

                        </div>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Name Khmer
                                    </th>

                                    <th>
                                        Sex
                                    </th>

                                    <th>
                                        Age
                                    </th>

                                    <th>
                                        Youth
                                    </th>

                                    <th>
                                        Phone
                                    </th>

                                    {{-- <th>
                                        E-mail
                                    </th> --}}

                                    <th>
                                        Province
                                    </th>

                                    <th>
                                        District
                                    </th>

                                    <th>
                                        Commune
                                    </th>

                                    {{-- <th>
                                        Village
                                    </th> --}}

                                    <th>
                                        Subsector
                                    </th>

                                    <th>
                                        Lead by
                                    </th>

                                    <th class="text-center">
                                        Status
                                    </th>

                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Suppliers as $Supplier)
                                    <tr>
                                        <td> {{ $Supplier->id }} </td>
{{--                                        <td>--}}
{{--                                            <a class="color:#206bc4" href="{{ route('suppliers.show', $Supplier->id) }}">{{ $Supplier->name }}</a>--}}
{{--                                        </td>--}}
                                        <td>
                                            {{ $Supplier->name }}
                                        </td>
                                        <td class="khmer">
                                            {{ $Supplier->namekh }}
                                        </td>
                                        <td>
                                            {{ $Supplier->sex }}
                                        </td>
                                        <td>
                                            {{ $Supplier->age }}
                                        </td>
                                        <td>
                                            {{ $Supplier->youth }}
                                        </td>
                                        <td>
                                            {{ $Supplier->phone }}
                                        </td>
                                        {{-- <td>
                                            {{ $Supplier->email }}
                                        </td> --}}
                                        <td>
                                            @if($Supplier->procode=='')
                                                {{ $Supplier->proname }}
                                            @else
                                                {{ $Supplier->provinces->proname }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($Supplier->discode=='')
                                                {{ $Supplier->disname }}
                                            @else
                                                {{ $Supplier->districts->disname }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($Supplier->comcode=='')
                                                {{ $Supplier->comname }}
                                            @else
                                                {{ $Supplier->communes->comname }}
                                            @endif
                                        </td>
                                        {{-- <td>
                                            {{ $Supplier->vilname }}
                                        </td> --}}
                                        <td>
                                            {{ $Supplier->subsector }}
                                        </td>
                                        <td>
                                            {{ $Supplier->lead }}
                                        </td>
                                        <td class="text-center">
                                            @can('creator')
                                                @if($buyer->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <label class="form-check form-switch m-0">
                                                        Pending
                                                    </label>
                                                @endif
                                            @endcan
                                            @can('approver')
                                                @if($buyer->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <form action="{{route('suppliers.approve')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$Supplier->id}}">
                                                        <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($Supplier->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                    </form>
                                                @endif

                                            @endcan
                                            @can('admin')
                                                <form action="{{route('suppliers.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$Supplier->id}}">
                                                    <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($Supplier->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                </form>
                                            @endcan
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route("suppliers.edit",$Supplier->id) }}">Edit</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('suppliers.destroy', $Supplier->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$Supplier->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Suppliers->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Suppliers->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@include("layouts.footer_scrips")
