@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-bmps'
])
@section('content')
<div class="content">
    <div class="row">

        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> List of Business Management Practice </h4>
                    </div>
                    <div class="card-body">

                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px;">
                            <form action="{{ route('bmps.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <select class="form-control select2" id="name" name="filter[id]" style=" height:48px;">
                                                <option value="">Name</option>
                                                @foreach($Bmps as $bmp)
                                                    <option value="{{$bmp->id}}">{{$bmp->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Name Khmer</label>
                                            <input type="text" class="form-control" name="filter[namekh]" placeholder="Name Khmer">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Sex</label>
                                            <select class="form-control" id="sex" name="filter[sex]" style=" height:48px;">
                                                <option value="">Sex</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Phone</label>
                                            <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control select2-selection" id="name" name="filter[procode]" style=" height:48px;">
                                                <option value="">Province</option>
                                                @foreach($Provinces as $procode => $proname)
                                                    <option value="{{$procode}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px;"> Search </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="{{ route('bmps-export') }}" class="form-control btn btn-warning" style="margin-top: 24px;">Export</a>
                                        </div>
                                        {{-- <form action="{{route('export.buyers')}}" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <input type="hidden" name="export" value="buyers">
                                                <button class="form-control btn btn-warning" style="margin-top: 24px;" type="submit">Export</button>
                                            </div>
                                        </form> --}}
                                    </div>

                                </div>
                                </form>

                        </div>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Name Khmer
                                    </th>

                                    <th>
                                        Sex
                                    </th>

                                    <th>
                                        Age
                                    </th>

                                    <th>
                                        Youth
                                    </th>

                                    <th>
                                        Phone
                                    </th>

                                    <th>
                                        Province
                                    </th>

                                    <th>
                                        Position
                                    </th>

                                    <th>
                                        From
                                    </th>

                                    <th>
                                        T.Topic 1
                                    </th>

                                    <th>
                                        T.Topics 2
                                    </th>

                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Bmps as $bmp)
                                    <tr>
                                        <td> {{ $bmp->id }} </td>
                                        <td>
                                            {{ $bmp->name }}
                                        </td>
                                        <td>
                                            {{ $bmp->namekh }}
                                        </td>
                                        <td>
                                            {{ $bmp->sex }}
                                        </td>
                                        <td>
                                            {{ $bmp->age }}
                                        </td>
                                        <td>
                                            {{ $bmp->youth }}
                                        </td>
                                        <td>
                                            {{ $bmp->phone }}
                                        </td>
                                        <td>
                                            {{ $bmp->proname }}
                                        </td>
                                        <td>
                                            {{ $bmp->position }}
                                        </td>
                                        <td>
                                            {{ $bmp->institute }}
                                        </td>
                                        <td>
                                            {{ $bmp->topic1 }}
                                        </td>
                                        <td>
                                            {{ $bmp->topic2 }}
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" data-toggle="tooltip" title='Edit' href="{{ route("bmps.edit",$bmp->id) }}">Edit</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('bmps.destroy', $bmp->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$bmp->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Bmps->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Bmps->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
@endsection
@include("layouts.footer_scrips")
