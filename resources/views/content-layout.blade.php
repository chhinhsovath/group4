@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-buyers'
])
@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> Create New Buyers </h4>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="{{ route('buyers.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="row">
                        {{-- containter --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {{--  end container--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
