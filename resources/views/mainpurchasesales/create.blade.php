@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'create-mainpurchasesales'
])
@section('content')

    <div class="content">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    {{-- containter --}}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--  end container--}}
                </div>
                <form action="{{ route('mainpurchasesales.store') }}" method="POST">
                    @csrf
                    <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
                    <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Buyer and Geographic Areas </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 500px;">
                                    {{-- start --}}
                                    <label class="form-label">Name</label>
                                    <div class="form-group">
                                        <select id="buyernames" name="buyernames" class="form-control" style=" height:45px;">
                                            <option value="0">--buyer name--</option>
                                            @foreach($Buyers as $id => $name)
                                                <option value="{{$name}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="form-label">Province</label>
                                    <div class="form-group">
                                        <select id="procode" name="procode" class="form-control" style=" height:45px;">
                                            <option value="0">--province--</option>
                                            @foreach($Provinces as $procode => $proname)
                                                <option value="{{$procode}}"> {{$proname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="form-label">District</label>
                                    <div class="form-group">
                                        <select id="discode" name="discode" class="form-control" style=" height:45px;">
                                            <option value="0">--district--</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Commune</label>
                                    <div class="form-group">
                                        <select id="comcode" name="comcode" class="form-control" style=" height:45px;">
                                            <option value="0">--commune--</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Village</label>
                                    <div class="form-group">
                                        <select id="vilcode" name="vilcode" class="form-control" style=" height:45px;">
                                            <option value="0">--village--</option>
                                        </select>
                                    </div>


                                    <br>
                                    {{-- end --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Suppliers and Transaction Description </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 500px;">

                                    <label class="form-label">Suppliers</label>
                                    <select id="suppliernames" name="suppliernames[]" multiple class="form-control select2multipls" style=" height:45px;">
                                        <option value="0">--supplier name--</option>
                                        @foreach($Suppliers as $name => $name)
                                            <option value="{{$name}}"> {{$name}}</option>
                                        @endforeach
                                    </select>

                                    <label class="form-label">Crop Names</label>
                                    <div class="form-group">
                                        <select name="cropnames[]" multiple class="form-control select2multipls" style=" height:45px;">
                                            <option value="">--crop names--</option>
                                            @foreach($Crops as $cropid => $cropname)
                                                <option value="{{$cropname}}"> {{$cropname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="form-label">Start Date </label>
                                    <div class="form-group">
                                        <input type="date"  class="form-control datepicker" name="startdate" placeholder="Start Date">
                                    </div>

                                    <label class="form-label">End Date </label>
                                    <div class="form-group">
                                        <input type="date"  class="form-control datepicker" name="enddate" placeholder="End Date">
                                    </div>

                                    <label class="form-label">Note </label>
                                    <div class="form-group">
                                        <input type="input"  class="form-control datepicker" name="note" placeholder="Note">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="pull-left">
                            <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary ms-auto">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2multipls').select2({
                placeholder: "Select multiple",
                allowClear: true,
                tags: true,
                tokenSeparators: [',',]
            });
        });

        $('#procode').change(function() {
            var procode = $(this).val();
            $("#discode").empty();
            $("#comcode").empty();
            $("#vilcode").empty();
            if (procode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getDistrict')}}/" + procode,
                    success: function(res) {
                        if (res) {
                            $("#discode").empty();
                            $("#discode").append('<option>Select District</option>');
                            $.each(res, function(key, value) {
                                $("#discode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#discode").empty();
                        }
                    }
                });
            } else {
                $("#discode").empty();
                $("#comcode").empty();
                $("#vilcode").empty();
            }
        });
        $('#discode').on('change', function() {
            var discode = $(this).val();
            $("#comcode").empty();
            $("#vilcode").empty();
            if (discode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getCommune')}}/" + discode,
                    success: function(res) {
                        if (res) {
                            $("#comcode").empty();
                            $("#comcode").append('<option>Select Commune</option>');
                            $.each(res, function(key, value) {
                                $("#comcode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#comcode").empty();
                        }
                    }
                });
            } else {
                $("#comcode").empty();
                $("#vilcode").empty();
            }

        });
        $('#comcode').on('change', function() {
            var comcode = $(this).val();
            $("#vilcode").empty();
            if (comcode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getVillage')}}/" + comcode,
                    success: function(res) {
                        if (res) {
                            $("#vilcode").empty();
                            $("#vilcode").append('<option>Select Village</option>');
                            $.each(res, function(key, value) {
                                $("#vilcode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#vilcode").empty();
                        }
                    }
                });
            } else {
                // $("#vilcode").empty();
            }

        });
    </script>
@endpush
