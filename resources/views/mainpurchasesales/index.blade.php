@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-mainpurchasesales'
])
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> List of Purchases and Sales </h4>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="{{ route('mainpurchasesales.create') }}"> Create New Purchase and Sale</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px; padding-top: 8px;">
                            <form action="{{ route('buyers.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <select class="form-control" id="name" name="filter[id]" style=" height:45px;">
                                                <option value="" selected>Name</option>
                                                @foreach($AllBuyers as $buyer)
                                                    <option value="{{$buyer->id}}">{{$buyer->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Name Khmer</label>
                                            <input type="text" class="form-control" name="filter[namekh]" placeholder="Name Khmer">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Sex</label>
                                            <select class="form-control" id="sex" name="filter[sex]" style=" height:45px;">
                                                <option value="" selected>Sex</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Phone</label>
                                            <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control" id="name" name="filter[procode]" style=" height:45px;">
                                                <option value="" selected>Province</option>
                                                @foreach($Provinces as $procode => $proname)
                                                    <option value="{{$procode}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                        </div>
                                    </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <a href="{{ route('mainpurchasesales-export') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Export</a>
                                </div>
                            </div>
                        </div>
                            </form>
                        </div>



                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Buyer.Name
                                    </th>
                                    <th>
                                        Province
                                    </th>

                                    <th>
                                        Suppliers.Name
                                    </th>

                                    <th>
                                        Crop.Names
                                    </th>
                                    <th>
                                        Start.Date
                                    </th>

                                    <th>
                                        Total.Items
                                    </th>
                                    <th>
                                        Month.Year
                                    </th>
                                    <th>
                                        Quarter
                                    </th>
                                    <th>
                                        Creator
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>

                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Mainpurchasesales as $Mainpurchasesale)
                                    <tr>
                                        <td> {{ $Mainpurchasesale->id }} </td>
{{--                                        <td>--}}
{{--                                            <a class="color:#206bc4" href="{{ route('mainpurchasesales.show', $Mainpurchasesale->id) }}">{{ $Mainpurchasesale->buyernames }}</a>--}}
{{--                                        </td>--}}
                                        <td>
                                            {{ $Mainpurchasesale->buyernames }}
                                        </td>
                                        <td>
                                            {{ $Mainpurchasesale->Provinces->proname }}
                                        </td>
                                        <td>
                                            {{ $Mainpurchasesale->suppliernames }}
                                        </td>
                                        <td>
                                            {{ $Mainpurchasesale->cropnames }}
                                        </td>
                                        <td>
                                            {{ $Mainpurchasesale->startdate }}
                                        </td>
                                        <td class="text-center">
                                            {{ $Mainpurchasesale->totalitems }}
                                        </td>
                                        <td>
                                            {{ $Mainpurchasesale->monthyear }}
                                        </td>
                                        <td>
                                            {{ $Mainpurchasesale->quarter }}
                                        </td>

                                        <td>
                                            {{ $Mainpurchasesale->usr_cre }}
                                        </td>
                                        <td class="text-center">
                                            @can('creator')
                                                @if($Mainpurchasesale->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <label class="form-check form-switch m-0">
                                                        Pending
                                                    </label>
                                                @endif
                                            @endcan
                                            @can('approver')
                                                @if($Mainpurchasesale->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <form action="{{route('mainpurchasesales.approve')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$Mainpurchasesale->id}}">
                                                        <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($Mainpurchasesale->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                    </form>
                                                @endif
                                            @endcan
                                            @can('admin')
                                                <form action="{{route('mainpurchasesales.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$Mainpurchasesale->id}}">
                                                    <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($Mainpurchasesale->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                </form>
                                            @endcan
                                        </td>

                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route("mainpurchasesales.edit",$Mainpurchasesale->id) }}">Edit</a>
                                                    <a class="dropdown-item" data-toggle="tooltip" title='Add Items' href="{{ route("mainpurchasesales.show",$Mainpurchasesale->id) }}">Add Items</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('mainpurchasesales.destroy', $Mainpurchasesale->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$Mainpurchasesale->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Mainpurchasesales->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Mainpurchasesales->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
@endsection
@include("layouts.footer_scrips")
