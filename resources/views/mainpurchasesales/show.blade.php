@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'show-mainpurchasesales'
])
@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-6">
                        <div class="card card-sm">
                            <div class="card-body" style="min-height: 100px;">
                                <h4 class="card-title">Buyer and Geographic Areas</h4>
                                <table class="table">
                                    <tr>
                                        <td>Name</td>
                                        <td><strong>{{$mainpurchasesale->buyernames}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Geography</td>
                                        <td><strong>{{$mainpurchasesale->Provinces->proname}}, {{$mainpurchasesale->districts->disname}}, {{$mainpurchasesale->communes->comname}}, {{$mainpurchasesale->villages->vilname}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col">Note</div>
                                                <div class="col"><strong><strong>{{$mainpurchasesale->note}}</strong></strong></div>
                                                <div class="col">Created</div>
                                                <div class="col"><strong>{{$mainpurchasesale->created_at}}</strong></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="card card-sm">
                            <div class="card-body" style="min-height: 100px;">
                                <h4 class="card-title"> Suppliers and Transaction Description </h4>
                                <table class="table">
                                    <tr>
                                        <td>Suppliers Name</td>
                                        <td><strong>{{$mainpurchasesale->buyernames}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Crops Name</td>
                                        <td><strong>{{$mainpurchasesale->cropnames}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col">Start</div>
                                                <div class="col"><strong>{{$mainpurchasesale->startdate}}</strong></div>
                                                <div class="col">End</div>
                                                <div class="col"><strong>{{$mainpurchasesale->enddate}}</strong></div>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @php
            $purchasesaleid = request()->has('itemsid') ? request()->get('itemsid') : '';
            $mainpurchasesaleid = Request::segment(2);
        @endphp

        @if ($purchasesaleid !=null)
            {{--Update Item--}}
            <div class="row ">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-body">
                            <form action="{{ route('purchasesales.update', $purchasesaleid) }}" method="POST" class="table-warning">
                                @csrf
                                @method('PUT')
                                <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
                                <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
                                <input type="hidden" class="form-control" name="id" value="{{ $purchasesaleid }}">
{{--                                $Purchasesaleoneitem--}}
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label class="form-label">Date </label>
                                        <input type="date" value="{{$Purchasesaleoneitem->date}}"  class="form-control datepicker" name="date" placeholder="Date" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="form-label">Crop Names</label>
                                        <select name="cropnames" class="form-control" style=" height:45px;" required>
                                            <option value="{{$Purchasesaleoneitem->cropnames}}" selected>{{$Purchasesaleoneitem->cropnames}}</option>
                                            @foreach($Crops as $cropid => $cropname)
                                                <option value="{{$cropname}}"> {{$cropname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="purchaseunit">P.Unit</label>
                                        <input type="number" step="any" value="{{$Purchasesaleoneitem->purchaseunit}}" class="form-control" name="purchaseunit" id="purchaseunit" placeholder="Purchase Unit" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="purchaseprice">P.Price</label>
                                        <input type="number" step="any" value="{{$Purchasesaleoneitem->purchaseprice}}" class="form-control" name="purchaseprice" id="purchaseprice" placeholder="Purchase Price" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="purchasetotal">P.Total</label>
                                        <input type="number" step="any" value="{{$Purchasesaleoneitem->purchasetotal}}" class="form-control" name="purchasetotal" id="purchasetotal" placeholder="Purchase Total" readonly>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <a href="{{url(Request::segment(1).'/'.Request::segment(2))}}" class="form-control btn btn-primary" style="margin-top: 24px;">add new</a>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Note </label>
                                        <input type="input" value="{{$Purchasesaleoneitem->salenote}}" class="form-control" name="salenote" id="salenote" placeholder="Note">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="saleunit">S.Unit</label>
                                        <input type="number" step="any" value="{{$Purchasesaleoneitem->saleunit}}" class="form-control" name="saleunit" id="saleunit" placeholder="Sale Unit" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="saleprice">S.Price</label>
                                        <input type="number" step="any" value="{{$Purchasesaleoneitem->saleprice}}" class="form-control" name="saleprice" id="saleprice" placeholder="Sale Price" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="saletotal">S.Total</label>
                                        <input type="number" step="any" value="{{$Purchasesaleoneitem->saletotal}}" class="form-control" name="saletotal" id="saletotal" placeholder="Sale Total" readonly>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button type="submit" class="form-control btn btn-primary" style="margin-top: 24px; background-color: #51cbce">UPDATE</button>
                                    </div>
                        </div>
                            </form>
                    </div>
                </div>
            </div>
            {{--End Update--}}
        @else
            {{--Create New Item--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('purchasesales.store') }}" method="POST">
                                @csrf
                                <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
                                <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
                                <input type="hidden" class="form-control" name="mainpurchasesalesid" value="{{ $mainpurchasesale->id }}">
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label class="form-label">Date </label>
                                        <input type="date"  class="form-control datepicker" name="date" placeholder="Date" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="form-label">Crop Names</label>
                                        <select name="cropnames" class="form-control " style=" height:45px;" required>
                                            <option value="">--crop names--</option>
                                            @foreach($Crops as $cropid => $cropname)
                                                <option value="{{$cropname}}"> {{$cropname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="purchaseunit">P.Unit</label>
                                        <input type="number" class="form-control" name="purchaseunit" id="purchaseunit" placeholder="Purchase Unit" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="purchaseprice">P.Price</label>
                                        <input type="number" class="form-control" name="purchaseprice" id="purchaseprice" placeholder="Purchase Price" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="purchasetotal">P.Total</label>
                                        <input type="number" class="form-control" name="purchasetotal" id="purchasetotal" placeholder="Purchase Total" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Note </label>
                                        <input type="text"  class="form-control" name="salenote" id="salenote" placeholder="Note">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="saleunit">S.Unit</label>
                                        <input type="number" class="form-control" name="saleunit" id="saleunit" placeholder="Sale Unit" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="saleprice">S.Price</label>
                                        <input type="number" class="form-control" name="saleprice" id="saleprice" placeholder="Sale Price" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="saletotal">S.Total</label>
                                        <input type="number" class="form-control" name="saletotal" id="saletotal" placeholder="Sale Total" readonly>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button type="submit" class="form-control btn btn-primary" style="margin-top: 24px; background-color: #51cbce">ADD ITEM</button>
                                    </div>
                        </div>
                            </form>
                    </div>
                </div>
            </div>
            {{--            End Create New--}}
        @endif

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="card-body">

                                <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px; padding-top: 8px;">
                                    <form action="{{ route('buyers.index') }}" method="get">
                                        <div class="row" style=" margin:10px;">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-control-label">Name Khmer</label>
                                                    <input type="text" class="form-control" name="filter[namekh]" placeholder="Name Khmer">
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="name">Sex</label>
                                                    <select class="form-control" id="sex" name="filter[sex]" style=" height:45px;">
                                                        <option value="" selected>Sex</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-control-label">Phone</label>
                                                    <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="name">Province</label>
                                                    <select class="form-control" id="name" name="filter[procode]" style=" height:45px;">
                                                        <option value="" selected>Province</option>
                                                        @foreach($Provinces as $procode => $proname)
                                                            <option value="{{$procode}}"> {{$proname}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                            </div>


                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        Row
                                    </th>
                                    <th>
                                        Code
                                    </th>

                                    <th>
                                        Date
                                    </th>

                                    <th>
                                        Crop Name
                                    </th>

                                    <th>
                                        P.Unit
                                    </th>

                                    <th>
                                        P.Price
                                    </th>

                                    <th>
                                        P.Total
                                    </th>

                                    <th>
                                        P.Note
                                    </th>

                                    <th>
                                        S.Unit
                                    </th>
                                    <th>
                                        S.Price
                                    </th>
                                    <th>
                                        S.Total
                                    </th>
                                    <th>
                                        S.Note
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>
                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                    </thead>
                                    <tbody>
                                    @php
                                    $purchase_unit =0;
                                    $total_purchase = 0;
                                    $sale_unit =0;
                                    $total_sale = 0;
                                    @endphp
                                    @foreach ($Purchasesales as $purchasesale)
                                        @php
                                            $purchase_unit += $purchasesale->purchaseunit;
                                            $total_purchase += $purchasesale->purchasetotal;
                                            $sale_unit += $purchasesale->saleunit;
                                            $total_sale += $purchasesale->saletotal;
                                        @endphp
                                        <tr id="{{ $purchasesale->id }}" class="{{ $purchasesale->id== $purchasesaleid ? 'table-warning' : ''}}">
{{--                                            background-color: #CFF5FF;--}}
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td> {{ $purchasesale->id }} </td>
                                            <td>
                                                {{ $purchasesale->date }}
                                            </td>
                                            <td>
                                                {{ $purchasesale->cropnames }}
                                            </td>
                                            <td>
                                                {{ $purchasesale->purchaseunit }}
                                            </td>
                                            <td>
                                                {{ $purchasesale->purchaseprice }}
                                            </td>
                                            <td>
                                                $ {{number_format($purchasesale->purchasetotal,2)}}
                                            </td>
                                            <td>
                                                {{ $purchasesale->purchasenote }}
                                            </td>
                                            <td>
                                                {{ $purchasesale->saleunit }}
                                            </td>
                                            <td>
                                                {{ $purchasesale->saleprice }}
                                            </td>
                                            <td>
                                                $ {{number_format($purchasesale->saletotal,2)}}
                                            </td>
                                            <td>
                                                {{ $purchasesale->salenote }}
                                            </td>
                                            <td class="text-center">
                                                @can('creator')
                                                    @if($purchasesale->recordstatus=='1')
                                                        <label class="form-check form-switch m-0">
                                                            Approved
                                                        </label>
                                                    @else
                                                        <label class="form-check form-switch m-0">
                                                            Pending
                                                        </label>
                                                    @endif
                                                @endcan
                                                @can('approver')
                                                    @if($purchasesale->recordstatus=='1')
                                                        <label class="form-check form-switch m-0">
                                                            Approved
                                                        </label>
                                                    @else
                                                        <form action="{{route('purchasesales.approve')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$purchasesale->id}}">
                                                            <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($purchasesale->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                        </form>
                                                    @endif

                                                @endcan
                                                @can('admin')
                                                    <form action="{{route('purchasesales.approve')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$purchasesale->id}}">
                                                        <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($purchasesale->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                    </form>
                                                @endcan
                                            </td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="nc-icon nc-bullet-list-67"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route("mainpurchasesales.show",[$mainpurchasesale->id,'itemsid='.$purchasesale->id]) }}">Edit</a>
                                                        @can('admin')
                                                            <form method="POST" action="{{ route('purchasesales.destroy', $purchasesale->id) }}">
                                                                @csrf
                                                                @method('DELETE')
                                                                <input type="hidden" value="{{$purchasesale->id}}" name="id">
                                                                <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                            </form>
                                                        @endcan
                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Total Purchase Unit</td>
                                        <td>{{$purchase_unit}}</td>
                                        <td>Total Purchase</td>
                                        <td>$ {{number_format($total_purchase,2)}}</td>
                                        <td>Total Purchase Unit</td>
                                        <td>{{$sale_unit}}</td>
                                        <td>Total Sale</td>
                                        <td>$ {{number_format($total_sale,2)}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tfoot>

                                </table>
                                <div class="card-footer d-flex align-items-center">
                                    <p class="m-0 text-muted">Showing   {{$Purchasesales->count()}} entries</p>
                                    <ul class="pagination m-0 ms-auto">
                                        {{$Purchasesales->links()}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
{{--                            <button type="submit" class="btn btn-primary ms-auto">SAVE</button>--}}
                            <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                        </div>
                    </div>
                    {{--  end container--}}

                </div>
            </div>
    </div>

@endsection
                    @include("layouts.footer_scrips")
@push('scripts')
    <script>
        $("input").change(function(){
            gettotalps();
        });
        function gettotalps(){
            let pqty =  document.getElementById("purchaseunit").value;
            let pamount =  document.getElementById("purchaseprice").value;
            let ptotal =  pqty * pamount;
            document.getElementById("purchasetotal").value = round(ptotal,2);

            let sqty =  document.getElementById("saleunit").value;
            let samount =  document.getElementById("saleprice").value;
            let stotal =  sqty * samount;
            document.getElementById("saletotal").value = round(stotal,2);
        }
        function round(value, exp) {
            if (typeof exp === 'undefined' || +exp === 0)
                return Math.round(value);

            value = +value;
            exp = +exp;

            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
                return NaN;

            // Shift
            value = value.toString().split('e');
            value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
        }
    </script>
@endpush

