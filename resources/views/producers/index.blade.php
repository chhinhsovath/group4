@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-producers'
])
@section('content')
<div class="content">
    <div class="row">

        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> List of Producers </h4>
                    </div>
                    <div class="card-body">

                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px;">
                            <form action="{{ route('producers.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <select class="form-control" id="name" name="filter[id]" style=" height:45px;">
                                                <option value="" selected>Name</option>
                                                @foreach($AllProducers as $id => $name)
                                                    <option value="{{$id}}"> {{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Name Khmer</label>
                                            <input type="text" class="form-control" name="filter[namekh]" placeholder="Name Khmer">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Sex</label>
                                            <select class="form-control" id="sex" name="filter[sex]" style=" height:45px;">
                                                <option value="" selected>Sex</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Phone</label>
                                            <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control" id="name" name="filter[procode]" style=" height:45px;">
                                                <option value="" selected>Province</option>
                                                @foreach($Provinces as $procode => $proname)
                                                    <option value="{{$procode}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="{{ route('producers-export') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Export</a>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Name Khmer
                                    </th>

                                    <th>
                                        Sex
                                    </th>

                                    <th>
                                        Age
                                    </th>

                                    <th>
                                        Youth
                                    </th>

                                    <th>
                                        Phone
                                    </th>

                                    {{-- <th>
                                        E-mail
                                    </th> --}}

                                    <th>
                                        Province
                                    </th>

                                    <th>
                                        District
                                    </th>

                                    <th>
                                        Commune
                                    </th>

                                    {{-- <th>
                                        Village
                                    </th> --}}

                                    <th>
                                        T.Type
                                    </th>

                                    <th>
                                        T.Topics
                                    </th>

                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Producers as $producer)
                                    <tr>
                                        <td> {{ $producer->id }} </td>
                                        <td>
                                            {{ $producer->name }}
                                        </td>
                                        <td>
                                            {{ $producer->namekh }}
                                        </td>
                                        <td>
                                            {{ $producer->sex }}
                                        </td>
                                        <td>
                                            {{ $producer->age }}
                                        </td>
                                        <td>
                                            {{ $producer->youth }}
                                        </td>
                                        <td>
                                            {{ $producer->phone }}
                                        </td>
                                        {{-- <td>
                                            {{ $producer->email }}
                                        </td> --}}
                                        <td>
                                            {{ $producer->proname }}
                                        </td>
                                        <td>
                                            {{ $producer->disname }}
                                        </td>
                                        <td>
                                            {{ $producer->comname }}
                                        </td>
                                        {{-- <td>
                                            {{ $producer->vilname }}
                                        </td> --}}
                                        <td>
                                            {{ $producer->trainingtype }}
                                        </td>
                                        <td>
                                            {{ $producer->topic1 }}
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" data-toggle="tooltip" title='Edit' href="{{ route("producers.edit",$producer->id) }}">Edit</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('producers.destroy', $producer->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$producer->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Producers->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Producers->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
@endsection
@include("layouts.footer_scrips")
