<form action="{{ route('allinonecontacts.store') }}" method="POST">
    @csrf
    <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
    <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">

                    <div class="row">
                        <div class="col-md-2">
                            <label class="form-label">Name English</label>
                            <div class="form-group">
                                <input type="text" class="form-control typeahead" name="name_en" id="name_en" placeholder="Name English" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="form-label">Name Khmer</label>
                            <div class="form-group">
                                <input type="text" class="form-control khmer" name="name_kh" id="name_kh" placeholder="Name Khmer">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label class="form-label">Sex </label>
                            <div class="form-group">
                                <select class="form-control" name="sex" style=" height:45px;" required>
                                    <option value=''>sex</option>
                                    <option value='Male'>Male</option>
                                    <option value='Female'>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="form-label">Age</label>
                            <div class="form-group">
                                <input type="number" class="form-control" name="age" placeholder="age">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="form-label">Phone</label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" placeholder="phone">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="form-label">E-mail</label>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="e-mail">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="form-label">Contact Type</label>
                            <div class="form-group">
                                <select class="form-control" name="tables" style=" height:45px;"  required>
                                    <option value=''>Type</option>
                                    <option value='buyers'>Buyers</option>
                                    <option value='suppliers'>Suppliers</option>
                                    <option value='producers'>Producers</option>
                                    <option value='bmps'>BMPs</option>
                                    <option value='grantees'>Grantees</option>
                                    <option value='collectors'>Collectors</option>
                                    <option value='general'>General</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Note </label>
                                <input type="text"  class="form-control" name="note" placeholder="Note">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Lead</label>
                            <div class="form-group">
                                <select id="staffprofiles_name" name="staffprofiles_name[]" multiple class="form-control select2multipls" style=" height:45px;"  required>
                                    <option value="">--lead--</option>
                                    @foreach($Staffprofiles as $id => $name)
                                        <option value="{{$name}}"> {{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-label">Province</label>
                                <select id="procode" name="procode" class="form-control" style=" height:45px;"  required  required>
                                    <option value="0">--province--</option>
                                    @foreach($Provinces as $procode => $proname)
                                        <option value="{{$procode}}"> {{$proname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-control-label" for="training_to">Disctrict</label>
                                <select id="discode" name="discode" class="form-control" style=" height:45px;">
                                    <option value="0">--district--</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-control-label" for="training_to">Commune</label>
                                <select id="comcode" name="comcode" class="form-control" style=" height:45px;">
                                    <option value="0">--commune--</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-control-label" for="training_to">Village</label>
                                <select id="vilcode" name="vilcode" class="form-control" style=" height:45px;">
                                    <option value="0">--village--</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label">Address Detail </label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="address" placeholder="address ">
                            </div>
                        </div>

                    </div>

                <div class="card-footer">
                    <div class="float-right">
                        <button type="submit" class="btn btn-primary ms-auto">SAVE</button>
                        <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                    </div>
                </div>
</form>
