@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'create-maintrainings'
])
@section('content')
    <div class="content">
        <div class="row">
            {{-- containter --}}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--  end container--}}
        </div>
        <form action="{{ route('maintrainings.store') }}" method="POST">
            @csrf
            <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
            <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"> Create New Training </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="training_type">Training Type</label>
                                        <select class="form-control" name="training_type" style=" height:48px;"  required>
                                            <option value="">--training type--</option>
                                            <option value="Workshop">Workshop</option>
                                            <option value="Village Training">Village Training</option>
                                            <option value="Meeting">Meeting</option>
                                            <option value="Field Day">Field Day</option>
                                            <option value="Core Training">Core Training</option>
                                            <option value="Core Group">Core Group</option>
                                            <option value="Online">Online</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="training_to">Training To</label>
                                        <select class="form-control" name="training_to" style=" height:48px;"  required>
                                            <option value="">--training to--</option>
                                            <option value="Farmers">Farmers</option>
                                            <option value="Suppliers">Suppliers</option>
                                            <option value="Buyers">Buyers</option>
                                            <option value="BMPs">BMPs</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label">Start Date</label>
                                        <input type="date" class="form-control datepicker" name="start_date" id="start_date" placeholder="start date">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label">End Date</label>
                                        <input type="date" class="form-control" name="end_date" id="end_date" placeholder="end date">
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="form-label">Trainers</label>
                                    <div class="form-group">
                                        <select id="trainers_name" name="trainers_name[]" multiple class="form-control select2multipls" style=" height:45px;"  required>
                                            <option value="">--lead--</option>
                                            @foreach($Staffprofiles as $id => $name)
                                                <option value="{{$name}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Topics</label>
                                        <div class="form-group">
                                            <select name="topics_title_en[]" multiple class="form-control select2multipls" style=" height:45px;">
                                                <option value="">--topics title--</option>
                                                @foreach($Trainingtopic as $id => $topics_title_en)
                                                    <option value="{{$topics_title_en}}"> {{$topics_title_en}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-label">Province</label>
                                        <select id="procode" name="procode" class="form-control" style=" height:45px;"  required>
                                            <option value="0">--province--</option>
                                            @foreach($Provinces as $procode => $proname)
                                                <option value="{{$procode}}"> {{$proname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="training_to">Disctrict</label>
                                        <select id="discode" name="discode" class="form-control" style=" height:45px;">
                                            <option value="0">--district--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="training_to">Commune</label>
                                        <select id="comcode" name="comcode" class="form-control" style=" height:45px;">
                                            <option value="0">--commune--</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="training_to">Village</label>
                                        <select id="vilcode" name="vilcode" class="form-control" style=" height:45px;">
                                            <option value="0">--village--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Note </label>
                                        <input type="text"  class="form-control" name="note" placeholder="Note">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Location Description </label>
                                        <input type="text"  class="form-control" name="location" placeholder="Location Description">
                                    </div>
                                </div>
                            </div>
                        </div>

{{--                        <div class="card-body">--}}
{{--                            <button class="btn">Default</button>--}}
{{--                            <button class="btn btn-primary">Primary</button>--}}
{{--                            <button class="btn btn-info">Info</button>--}}
{{--                            <button class="btn btn-success">Success</button>--}}
{{--                            <button class="btn btn-warning">Warning</button>--}}
{{--                            <button class="btn btn-danger">Danger</button>--}}
{{--                        </div>--}}

                        <div class="card-footer">
                            <div class="float-right">
                                <button type="submit" class="btn btn-primary ms-auto">SAVE</button>
                                <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </form>
        @endsection
        @push('scripts')
            <script>
                $(document).ready(function() {
                    $('.select2multipls').select2({
                        placeholder: "Select multiple",
                        allowClear: true,
                        tags: true,
                        tokenSeparators: [',',]
                    });
                });

                $('#procode').change(function() {
                    var procode = $(this).val();
                    $("#discode").empty();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (procode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getDistrict')}}/" + procode,
                            success: function(res) {
                                if (res) {
                                    $("#discode").empty();
                                    $("#discode").append('<option>Select District</option>');
                                    $.each(res, function(key, value) {
                                        $("#discode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#discode").empty();
                                }
                            }
                        });
                    } else {
                        $("#discode").empty();
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }
                });

                $('#discode').on('change', function() {
                    var discode = $(this).val();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (discode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getCommune')}}/" + discode,
                            success: function(res) {
                                if (res) {
                                    $("#comcode").empty();
                                    $("#comcode").append('<option>Select Commune</option>');
                                    $.each(res, function(key, value) {
                                        $("#comcode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#comcode").empty();
                                }
                            }
                        });
                    } else {
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }

                });
                $('#comcode').on('change', function() {
                    var comcode = $(this).val();
                    $("#vilcode").empty();
                    if (comcode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getVillage')}}/" + comcode,
                            success: function(res) {
                                if (res) {
                                    $("#vilcode").empty();
                                    $("#vilcode").append('<option>Select Village</option>');
                                    $.each(res, function(key, value) {
                                        $("#vilcode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#vilcode").empty();
                                }
                            }
                        });
                    } else {
                        // $("#vilcode").empty();
                    }

                });
            </script>
    @endpush
