@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add-participants'
])
@section('content')
    <div class="content">
        <div class="row">
            {{-- containter --}}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--  end container--}}
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">


{{--                    <div class="card-header">--}}
{{--                        <h2 class="navbar-brand">Add Participants to Training</h2>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul id="tabs" class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#new_contact" role="tab"
                                           aria-expanded="true"><span class="btn btn-sm btn-warning"> Create New Contact </span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#farmers" role="tab"
                                           aria-expanded="true"><span class="btn btn-sm btn-primary"> Existing Contact </span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="my-tab-content" class="tab-content">
                            <div class="tab-pane" id="new_contact" role="tabpanel" aria-expanded="false">



                                                <form action="{{ route('allinonecontacts.store') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
                                                    <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label class="form-label">Name English</label>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control typeahead" name="name_en" id="name_en" placeholder="Name English" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="form-label">Name Khmer</label>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control khmer" name="name_kh" id="name_kh" placeholder="Name Khmer">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label class="form-label">Sex </label>
                                                            <div class="form-group">
                                                                <select class="form-control" name="sex" style=" height:45px;" required>
                                                                    <option value=''>sex</option>
                                                                    <option value='Male'>Male</option>
                                                                    <option value='Female'>Female</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="form-label">Age</label>
                                                            <div class="form-group">
                                                                <input type="number" class="form-control" name="age" placeholder="age">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="form-label">Phone</label>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="phone" placeholder="phone">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="form-label">E-mail</label>
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" name="email" placeholder="e-mail">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label class="form-label">Contact Type</label>
                                                            <div class="form-group">
                                                                <select class="form-control" name="tables" style=" height:45px;"  required>
                                                                    <option value='' selected>Type</option>
                                                                    <option value='buyers'>Buyers</option>
                                                                    <option value='suppliers'>Suppliers</option>
                                                                    <option value='producers'>Producers</option>
                                                                    <option value='bmps'>BMPs</option>
                                                                    <option value='grantees'>Grantees</option>
                                                                    <option value='collectors'>Collectors</option>
                                                                    <option value='general'>General</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-label">Note </label>
                                                                <input type="text"  class="form-control" name="note" placeholder="Note">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="form-label">Lead</label>
                                                            <div class="form-group">
                                                                <select id="staffprofiles_name" name="staffprofiles_name[]" multiple class="form-control select2multipls" style=" height:45px; width: 100%"  required>
                                                                    <option value="">--lead--</option>
                                                                    @foreach($Staffprofiles as $id => $name)
                                                                        <option value="{{$name}}"> {{$name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="form-label">Province</label>
                                                                <select id="procode" name="procode" class="form-control" style=" height:45px;"  required  required>
                                                                    <option value="0">--province--</option>
                                                                    @foreach($Provinces as $procode => $proname)
                                                                        <option value="{{$procode}}"> {{$proname}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="form-control-label" for="training_to">Disctrict</label>
                                                                <select id="discode" name="discode" class="form-control" style=" height:45px;">
                                                                    <option value="0">--district--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="form-control-label" for="training_to">Commune</label>
                                                                <select id="comcode" name="comcode" class="form-control" style=" height:45px;">
                                                                    <option value="0">--commune--</option>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="form-control-label" for="training_to">Village</label>
                                                                <select id="vilcode" name="vilcode" class="form-control" style=" height:45px;">
                                                                    <option value="0">--village--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="form-label">Address Detail </label>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="address" placeholder="address ">
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="float-right">
                                                        <button type="submit" class="btn btn-primary ms-auto">SAVE</button>
                                                        <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                                                    </div>
                                                </form>




                            </div>
                            <div class="tab-pane active" id="farmers" role="tabpanel" aria-expanded="true">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="navbar-brand">
                                                        Find Participants and Transfer to Training
                                                    </div>
                                                    <form action="{{ route("maintrainings.show",$MainTrainings->id) }}" method="get">
                                                        <div class="row" style=" margin:10px;">

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="name">Name</label>
                                                                    <input type="text" class="form-control" name="filter[name_en]" placeholder="Name English">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="phone">Phone</label>
                                                                    <input type="text" class="form-control" id="phone" name="filter[phone]" placeholder="Phone">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="sex">Sex</label>
                                                                    <select class="form-control" id="tables" name="filter[sex]" style=" height:45px;">
                                                                        <option value="" selected>sex</option>
                                                                        <option value="Male">Male</option>
                                                                        <option value="Female">Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="phone">Age</label>
                                                                    <input type="text" class="form-control" id="age" name="filter[age]" placeholder="Age">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="vilname">Contact Type</label>
                                                                    <select class="form-control" id="tables" name="filter[tables]" style=" height:45px;">
                                                                        <option value="" selected>Contact Type</option>
                                                                        <option value="bmps">BMP</option>
                                                                        <option value="buyers">Buyers</option>
                                                                        <option value="grantees">Grantees</option>
                                                                        <option value="producers">Producers</option>
                                                                        <option value="suppliers">Suppliers</option>
                                                                        <option value="general">General</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="proname">Province</label>
                                                                    <select class="form-control" id="proname" name="filter[proname]" style=" height:45px;">
                                                                        <option value="" selected>Province</option>
                                                                        @foreach($Provinces as $procode => $proname)
                                                                            <option value="{{$proname}}"> {{$proname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="disname">District</label>
                                                                    <select class="form-control" id="disname" name="filter[disname]" style=" height:45px;">
                                                                        <option value="" selected>District</option>
                                                                        @foreach($Districts as $discode => $disname)
                                                                            <option value="{{$disname}}"> {{$disname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="comname">Commune</label>
                                                                    <select class="form-control" id="comname" name="filter[comname]" style=" height:45px;">
                                                                        <option value="" selected>Commune</option>
                                                                        @foreach($Communes as $comcode => $comname)
                                                                            <option value="{{$comname}}"> {{$comname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="vilname">Village</label>
                                                                    <select class="form-control" id="vilname" name="filter[vilname]" style=" height:45px;">
                                                                        <option value="" selected>Village</option>
                                                                        @foreach($Villages as $vilcode => $vilname)
                                                                            <option value="{{$vilname}}"> {{$vilname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                                    <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="card">

                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <div class="navbar-brand">
                                                                                Participants Join This Training

                                                                            </div>
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <thead class=" text-primary">
                                                                        <th>
                                                                            ID
                                                                        </th>
                                                                        <th>
                                                                            Name.English
                                                                        </th>
                                                                        {{--                                                            <th>--}}
                                                                        {{--                                                                Name.Khmer--}}
                                                                        {{--                                                            </th>--}}

                                                                        <th>
                                                                            Sex
                                                                        </th>

                                                                        <th>
                                                                            Age
                                                                        </th>

                                                                        <th>
                                                                            Phone
                                                                        </th>

                                                                        <th>
                                                                            Province
                                                                        </th>

                                                                        <th>
                                                                            District
                                                                        </th>

                                                                        <th>
                                                                            Commune
                                                                        </th>
                                                                        <th>
                                                                            Village
                                                                        </th>
                                                                        <th>
                                                                            Type
                                                                        </th>
                                                                        <th>
                                                                            Transfer
                                                                        </th>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach ($all_contacts as $contact)
                                                                            <tr>
                                                                                <td> {{ $contact->id }} </td>
                                                                                <td>
                                                                                    {{ $contact->name_en }}
                                                                                </td>
                                                                                {{--                                                                    <td>--}}
                                                                                {{--                                                                        {{ $contact->name_kh }}--}}
                                                                                {{--                                                                    </td>--}}
                                                                                <td>
                                                                                    {{ $contact->sex }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->age }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->phone }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->proname }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->disname }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->comname }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->vilname }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $contact->tables }}
                                                                                </td>
                                                                                <td class="text-right">

                                                                                    <form action="{{route('maintrainings.storeparticipant')}}" onsubmit="preload_show()" method="post" name="frm_{{$contact->uuid}}" id="frm_{{$contact->uuid}}">
                                                                                        @csrf
                                                                                        <input type="hidden" name="userid" value="{{auth()->user()->id}}">
                                                                                        <input type="hidden" name="grantid" value="{{auth()->user()->grantid}}">
                                                                                        <input type="hidden" name="allinone_uuid" value="{{$contact->uuid}}">
                                                                                        <input type="hidden" name="mts_id" value="{{$MainTrainings->id}}">
                                                                                        <button type="submit"><i class="nc-icon nc-minimal-right btn btn-sm btn-primary"></i></button>
                                                                                    </form>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="card-footer d-flex align-items-center">
                                                                        <p class="m-0 text-muted">Showing   {{$all_contacts->count()}} entries</p>
                                                                        <ul class="pagination m-0 ms-auto">
                                                                            {{$all_contacts->links()}}
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                                    </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="card">

                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="navbar-brand">
                                                                                    Participants Join This Training

                                                                                </div>
                                                                                <div class="table-responsive">
                                                                                    <table class="table">
                                                                                        <thead class=" text-primary">
                                                                                        <th>
                                                                                            ID
                                                                                        </th>
                                                                                        <th>
                                                                                            Name.English
                                                                                        </th>

                                                                                        <th>
                                                                                            Sex
                                                                                        </th>

                                                                                        <th>
                                                                                            Age
                                                                                        </th>

                                                                                        <th>
                                                                                            Phone
                                                                                        </th>
                                                                                        <th>
                                                                                            Delete
                                                                                        </th>

                                                                                        </thead>
                                                                                        <tbody>
                                                                                        @foreach ($MainTrainingsMeta as $person)
                                                                                            <tr>
                                                                                                <td> {{ $person->id }} </td>
                                                                                                <td>
                                                                                                    {{ $person->info->name_en }}
                                                                                                </td>
                                                                                                <td>
                                                                                                    {{ $person->info->sex }}
                                                                                                </td>
                                                                                                <td>
                                                                                                    {{ $person->info->age }}
                                                                                                </td>
                                                                                                <td>
                                                                                                    {{ $person->info->phone }}
                                                                                                </td>

                                                                                                <td class="text-right">
                                                                                                    <form method="POST" name="frm_{{$person->id}}" action="{{ route('maintrainingsmeta.destroy',$person->id) }}">
                                                                                                        @csrf
                                                                                                        @method('DELETE')
                                                                                                        <input type="hidden" value="{{$MainTrainings->id}}" name="mts_id">
                                                                                                        <input type="hidden" value="{{$person->id}}" name="id">
                                                                                                        <button type="submit" class="show_confirm btn btn-sm btn-danger" data-toggle="tooltip" title='Delete'><i class="nc-icon nc-simple-remove"></i></button>
                                                                                                    </form>
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endforeach
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <div class="card-footer d-flex align-items-center">
                                                                                        <p class="m-0 text-muted">Showing   {{$MainTrainingsMeta->count()}} entries</p>
                                                                                        {{--                                                            <ul class="pagination m-0 ms-auto">--}}
                                                                                        {{--                                                                {{$MainTrainingsMeta->links()}}--}}
                                                                                        {{--                                                            </ul>--}}
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

        </div>

        @endsection
        @push('scripts')
            <script>

                $('.show_confirm').click(function(event) {
                    var form =  $(this).closest("form");
                    var id = $(this).data("id");
                    event.preventDefault();
                    swal({
                        title: `Are you sure you want to delete this record?`,
                        text: "If you delete this, it will be gone forever.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        showCancelButton: true,
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                form.submit();
                            }
                            // $(".preloader").fadeIn();
                        });
                });

                // function popup(){
                //     $('#modalid').modal('show');
                //     // $('#modalid').modal('hide');
                //     Swal.fire({
                //         position: 'top-end',
                //         icon: 'success',
                //         title: 'Success',
                //         showConfirmButton: false,
                //         timer: 1500
                //     })
                // }


                $(document).ready(function() {
                    $('.select2multipls').select2({
                        placeholder: "Select multiple",
                        allowClear: true,
                        tags: true,
                        tokenSeparators: [',',]
                    });
                });

                $('#procode').change(function() {
                    var procode = $(this).val();
                    $("#discode").empty();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (procode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getDistrict')}}/" + procode,
                            success: function(res) {
                                if (res) {
                                    $("#discode").empty();
                                    $("#discode").append('<option>Select District</option>');
                                    $.each(res, function(key, value) {
                                        $("#discode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#discode").empty();
                                }
                            }
                        });
                    } else {
                        $("#discode").empty();
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }
                });

                $('#discode').on('change', function() {
                    var discode = $(this).val();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (discode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getCommune')}}/" + discode,
                            success: function(res) {
                                if (res) {
                                    $("#comcode").empty();
                                    $("#comcode").append('<option>Select Commune</option>');
                                    $.each(res, function(key, value) {
                                        $("#comcode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#comcode").empty();
                                }
                            }
                        });
                    } else {
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }

                });
                $('#comcode').on('change', function() {
                    var comcode = $(this).val();
                    $("#vilcode").empty();
                    if (comcode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getVillage')}}/" + comcode,
                            success: function(res) {
                                if (res) {
                                    $("#vilcode").empty();
                                    $("#vilcode").append('<option>Select Village</option>');
                                    $.each(res, function(key, value) {
                                        $("#vilcode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#vilcode").empty();
                                }
                            }
                        });
                    } else {
                        // $("#vilcode").empty();
                    }

                });
            </script>
    @endpush
