@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-maintrainings'
])
@section('content')
    <div class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> List of Trainings </h4>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="{{ route('maintrainings.create') }}"> Create New Training</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px; padding-top: 8px;">
                            <form action="{{ route('maintrainings.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <select class="form-control" id="name" name="filter[id]" style=" height:45px;">
                                                <option value="" selected>Name</option>
                                                @foreach($Buyers as $id => $name)
                                                    <option value="{{$name}}"> {{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Name Khmer</label>
                                            <input type="text" class="form-control" name="filter[namekh]" placeholder="Name Khmer">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Sex</label>
                                            <select class="form-control" id="sex" name="filter[sex]" style=" height:45px;">
                                                <option value="" selected>Sex</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Phone</label>
                                            <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control" id="name" name="filter[procode]" style=" height:45px;">
                                                <option value="" selected>Province</option>
                                                @foreach($Provinces as $procode => $proname)
                                                    <option value="{{$procode}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                        </div>
                                    </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <a href="{{ route('maintrainings-export') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Export</a>
                                </div>
                            </div>

                        </div>
                            </form>
                    </div>


                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <th>
                                ID
                            </th>
                            <th>
                                Training.Type
                            </th>
                            <th>
                                Training.To
                            </th>
                            <th>
                                Topics
                            </th>
                            <th>
                                Trainers
                            </th>
                            <th>
                                Province.Name
                            </th>
                            <th>
                                Note
                            </th>
                            <th>
                                Start.Date
                            </th>
                            <th>
                                End.Date
                            </th>
                            <th>
                                Month.Year
                            </th>
                            <th>
                                Quarter
                            </th>
                            <th class="text-center">
                                Status
                            </th>

                            <th class="text-right">
                                <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($Maintrainings as $maintraining)
                                <tr>
                                    <td> {{ $maintraining->id }}/{{ $maintraining->oid }}</td>
                                    <td>
                                        {{ $maintraining->training_type }}
                                    </td>
                                    <td>
                                        {{ $maintraining->training_to }}
                                    </td>
                                    <td>
                                        {{ $maintraining->topics_title_en }}
                                    </td>
                                    <td>
                                        {{ $maintraining->trainers_name }}
                                    </td>
                                    <td>
                                        {{ $maintraining->proname }}
                                    </td>
                                    <td>
                                        {{ $maintraining->note }}
                                    </td>
                                    <td>
                                        {{ $maintraining->start_date }}
                                    </td>
                                    <td>
                                        {{ $maintraining->end_date }}
                                    </td>
                                    <td>
                                        {{ $maintraining->monthyear }}
                                    </td>
                                    <td>
                                        {{ $maintraining->quarter }}
                                    </td>
                                    <td class="text-center">
                                        @can('creator')
                                            @if($maintraining->recordstatus=='1')
                                                <label class="form-check form-switch m-0">
                                                    Approved
                                                </label>
                                            @else
                                                <label class="form-check form-switch m-0">
                                                    Pending
                                                </label>
                                            @endif
                                        @endcan
                                        @can('approver')
                                            @if($maintraining->recordstatus=='1')
                                                <label class="form-check form-switch m-0">
                                                    Approved
                                                </label>
                                            @else
                                                <form action="{{route('maintrainings.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$maintraining->id}}">
                                                    <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($maintraining->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                </form>
                                            @endif

                                        @endcan
                                        @can('admin')
                                            <form action="{{route('maintrainings.approve')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$maintraining->id}}">
                                                <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($maintraining->recordstatus=='1') value="0" checked @else value="1" @endif >
                                            </form>
                                        @endcan
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="nc-icon nc-bullet-list-67"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" data-toggle="tooltip" title='Edit' href="{{ route("maintrainings.edit",$maintraining->id) }}">Edit</a>
{{--                                                <a class="dropdown-item" data-toggle="tooltip" title='Add Participants' href="{{ route("maintrainings.addparticipants",$maintraining->id) }}">Add Participants</a>--}}
                                                <a class="dropdown-item" data-toggle="tooltip" title='Add Participants' href="{{ route("maintrainings.show",$maintraining->id) }}">Add Participants</a>
                                                @can('admin')
                                                    <form method="POST" action="{{ route('maintrainings.destroy', $maintraining->id) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        {{--                                                            <input name="_method" type="hidden" value="DELETE">--}}
                                                        <input type="hidden" value="{{$maintraining->id}}" name="id">
                                                        <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                    </form>
                                                @endcan
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Maintrainings->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Maintrainings->links()}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@include("layouts.footer_scrips")
