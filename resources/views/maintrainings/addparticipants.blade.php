@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'maintrainings-participants'
])
@section('content')
    <div class="content">
        <div class="row">
            {{-- containter --}}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--  end container--}}
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">


                    <div class="card-header">
                        <h5>Add Participants to Training</h5>
                    </div>
                    <div class="card-body">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul id="tabs" class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#farmers" role="tab"
                                           aria-expanded="true">General Farmers</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#buyers" role="tab"
                                           aria-expanded="false">Buyers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#suppliers" role="tab"
                                           aria-expanded="false">Suppliers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#bmps" role="tab"
                                           aria-expanded="false">BMPs</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div id="my-tab-content" class="tab-content text-center">
                            <div class="tab-pane" id="farmers" role="tabpanel" aria-expanded="true">
                                <p>Here are your farmers.</p>
                            </div>
                            <div class="tab-pane active" id="buyers" role="tabpanel" aria-expanded="false">
                                <p>Here is your buyers</p>
                            </div>
                            <div class="tab-pane" id="suppliers" role="tabpanel" aria-expanded="false">
                                <p>Here is your suppliers.</p>
                            </div>
                            <div class="tab-pane" id="bmps" role="tabpanel" aria-expanded="false">
                                <p>Here are your bmps.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>



        @endsection
        @push('scripts')
            <script>


                // $(document).ready(function() {
                //     // var hash = window.location.hash;
                //     // alert(has);
                //     // $('.nav-link'+hash+':first').show();
                //
                //     $(".nav-link").click(function () {
                //         alert(window.location.hash.substr(1));
                //     });
                //
                // });


                $(document).ready(function() {
                    $('.select2multipls').select2({
                        placeholder: "Select multiple",
                        allowClear: true,
                        tags: true,
                        tokenSeparators: [',',]
                    });
                });

                $('#procode').change(function() {
                    var procode = $(this).val();
                    $("#discode").empty();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (procode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getDistrict')}}/" + procode,
                            success: function(res) {
                                if (res) {
                                    $("#discode").empty();
                                    $("#discode").append('<option>Select District</option>');
                                    $.each(res, function(key, value) {
                                        $("#discode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#discode").empty();
                                }
                            }
                        });
                    } else {
                        $("#discode").empty();
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }
                });

                $('#discode').on('change', function() {
                    var discode = $(this).val();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (discode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getCommune')}}/" + discode,
                            success: function(res) {
                                if (res) {
                                    $("#comcode").empty();
                                    $("#comcode").append('<option>Select Commune</option>');
                                    $.each(res, function(key, value) {
                                        $("#comcode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#comcode").empty();
                                }
                            }
                        });
                    } else {
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }

                });
                $('#comcode').on('change', function() {
                    var comcode = $(this).val();
                    $("#vilcode").empty();
                    if (comcode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getVillage')}}/" + comcode,
                            success: function(res) {
                                if (res) {
                                    $("#vilcode").empty();
                                    $("#vilcode").append('<option>Select Village</option>');
                                    $.each(res, function(key, value) {
                                        $("#vilcode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#vilcode").empty();
                                }
                            }
                        });
                    } else {
                        // $("#vilcode").empty();
                    }

                });
            </script>
    @endpush
