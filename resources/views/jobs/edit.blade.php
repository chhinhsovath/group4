@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'edit-jobs'
])
@section('content')
    <div class="content">
        <div class="row">
            {{-- containter --}}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--  end container--}}
        </div>
        <form action="{{ route('jobs.update',$Jobs->id) }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
            <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
            <input type="hidden" class="form-control" name="id" value="{{ $Jobs->id }}">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="pull-left">
                                <h4 class="card-title"> Edit Jobs </h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label class="form-control-label" for="name">Name</label>
                                    <select class="form-control" name="name" style=" height:45px;" required>
                                        <option value="{{$Jobs->name}}" selected>{{$Jobs->name}}</option>
                                        @foreach($Buyers as $id => $name)
                                            <option value="{{$name}}"> {{$name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label">Sub Sector </label>
                                    <div class="form-group">
                                        <select class="form-control" name="subsector" style=" height:45px;"  required>
                                            <option value="{{$Jobs->subsector}}">{{$Jobs->subsector}}</option>
                                            <option value="Cashew">Cashew</option>
                                            <option value="Longan">Longan</option>
                                            <option value="Mango">Mango</option>
                                            <option value="Mixed Fruit">Mixed Fruit</option>
                                            <option value="Pepper Corn">Pepper Corn</option>
                                            <option value="Vegetable">Vegetable</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label">Province</label>
                                    <div class="form-group">
                                        <select id="procode" name="procode" class="form-control" style=" height:45px;"  required>
                                            <option value="{{$Jobs->procode}}">{{$Jobs->provinces->proname}}</option>
                                            @foreach($Provinces as $procode => $proname)
                                                <option value="{{$procode}}"> {{$proname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label">Lead</label>
                                    <div class="form-group">
                                        <select id="staffprofilename" name="staffprofilename" class="form-control" style=" height:45px;"  required>
                                            <option value="{{$Jobs->staffprofilename}}">{{$Jobs->staffprofilename}}</option>
                                            @foreach($Staffprofiles as $id => $name)
                                                <option value="{{$name}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="startdate">Start.Date</label>
                                    <input type="date" class="form-control" value="{{$Jobs->startdate}}" name="startdate" id="startdate" placeholder="startdate">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="enddate">End.Date</label>
                                    <input type="date" class="form-control" value="{{$Jobs->enddate}}" name="enddate" id="enddate" placeholder="enddate">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="form-label">Note </label>
                                    <input type="text"  class="form-control" value="{{$Jobs->note}}" name="note" placeholder="Note">
                                </div>
                                {{--                                        <div class="form-group col-md-2">--}}
                                {{--                                            <button type="submit" class="form-control btn btn-primary" style="margin-top: 24px; background-color: #51cbce">SAVE</button>--}}
                                {{--                                        </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="pull-left">
                                    <h4 class="card-title"> Full-Time Jobs </h4>
                                </div>
                            </div>
                            <div class="card-body" style="min-height: 310px;">

                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Number of Existing Full-time Jobs</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Jobs->totalexistingfulltimestaff}}" name="totalexistingfulltimestaff" placeholder="Existing"  required>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Number of New Full-time Jobs</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Jobs->totalnewfulltimestaff}}" name="totalnewfulltimestaff" placeholder="Recruite New"  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Number of Female</label>
                                        <div class="form-group">
                                            <input type="number" class="form-control" value="{{$Jobs->totalfulltimeyouth}}" name="totalfulltimeyouth" placeholder="Number of Female">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Number of Youth</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Jobs->totalfulltimefemale}}" name="totalfulltimefemale" placeholder="Number of Youth" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Date</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" value="{{$Jobs->datehirefulltimestaff}}" name="datehirefulltimestaff">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Note</label>
                                        <div class="form-group">
                                            <input type="text"  class="form-control" value="{{$Jobs->fulltimenote}}" name="fulltimenote" placeholder="Note">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="pull-left">
                                    <h4 class="card-title"> Part-Time Jobs </h4>
                                </div>
                            </div>
                            <div class="card-body" style="min-height: 310px;">
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Number of Part-time Jobs</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Jobs->totalparttimestaff}}" name="totalparttimestaff" placeholder="Number of Partime Job"  required>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Number of Working Days</label>
                                        <div class="form-group">
                                            <input type="number" class="form-control" value="{{$Jobs->numberofdaysparttimestaff}}" name="numberofdaysparttimestaff" placeholder="Number of Working Days">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Number of Female</label>
                                        <div class="form-group">
                                            <input type="number" class="form-control" value="{{$Jobs->totalpartimefemale}}" name="totalpartimefemale" placeholder="Number of Female - Optional">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Number of Youth</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Jobs->totalparttimeyouth}}" name="totalparttimeyouth" placeholder="Number of Youth - Optional" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Date</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" value="{{$Jobs->datehireparttimestaff}}" name="datehireparttimestaff">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Note</label>
                                        <div class="form-group">
                                            <input type="text"  class="form-control" value="{{$Jobs->parttimenote}}" name="parttimenote" placeholder="Note">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <div class="pull-left">
                                        <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary ms-auto">UPDATE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @endsection
        @push('scripts')
            <script>
                // $(document).ready(function() {
                //     $('.select2multipls').select2({
                //         placeholder: "Select multiple",
                //         allowClear: true,
                //         tags: true,
                //         tokenSeparators: [',',]
                //     });
                // });

                $('#procode').change(function() {
                    var procode = $(this).val();
                    $("#discode").empty();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (procode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getDistrict')}}/" + procode,
                            success: function(res) {
                                if (res) {
                                    $("#discode").empty();
                                    $("#discode").append('<option>Select District</option>');
                                    $.each(res, function(key, value) {
                                        $("#discode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#discode").empty();
                                }
                            }
                        });
                    } else {
                        $("#discode").empty();
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }
                });
                {{--$('#discode').on('change', function() {--}}
                {{--    var discode = $(this).val();--}}
                {{--    $("#comcode").empty();--}}
                {{--    $("#vilcode").empty();--}}
                {{--    if (discode) {--}}
                {{--        $.ajax({--}}
                {{--            type: "GET",--}}
                {{--            url: "{{url('getCommune')}}?discode=" + discode,--}}
                {{--            success: function(res) {--}}
                {{--                if (res) {--}}
                {{--                    $("#comcode").empty();--}}
                {{--                    $("#comcode").append('<option>Select Commune</option>');--}}
                {{--                    $.each(res, function(key, value) {--}}
                {{--                        $("#comcode").append('<option value="' + key + '">' + value + '</option>');--}}
                {{--                    });--}}

                {{--                } else {--}}
                {{--                    $("#comcode").empty();--}}
                {{--                }--}}
                {{--            }--}}
                {{--        });--}}
                {{--    } else {--}}
                {{--        $("#comcode").empty();--}}
                {{--        $("#vilcode").empty();--}}
                {{--    }--}}

                {{--});--}}
                {{--$('#comcode').on('change', function() {--}}
                {{--    var comcode = $(this).val();--}}
                {{--    $("#vilcode").empty();--}}
                {{--    if (comcode) {--}}
                {{--        $.ajax({--}}
                {{--            type: "GET",--}}
                {{--            url: "{{url('getVillage')}}?comcode=" + comcode,--}}
                {{--            success: function(res) {--}}
                {{--                if (res) {--}}
                {{--                    $("#vilcode").empty();--}}
                {{--                    $("#vilcode").append('<option>Select Village</option>');--}}
                {{--                    $.each(res, function(key, value) {--}}
                {{--                        $("#vilcode").append('<option value="' + key + '">' + value + '</option>');--}}
                {{--                    });--}}

                {{--                } else {--}}
                {{--                    $("#vilcode").empty();--}}
                {{--                }--}}
                {{--            }--}}
                {{--        });--}}
                {{--    } else {--}}
                {{--        // $("#vilcode").empty();--}}
                {{--    }--}}

                // });
            </script>
    @endpush
