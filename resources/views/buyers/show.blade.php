@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'show-buyers'
])
@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <div class="pull-left">--}}
{{--                            <h4 class="card-title"> Detail-Buyers </h4>--}}
{{--                        </div>--}}
{{--                        <div class="pull-right">--}}
{{--                            <a class="btn btn-sm btn-success" href="{{ route('buyers.index') }}"> Back</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="card-body">
                    <div class="row">
                        {{-- containter --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                        <div class="row">
                        <div class="col-3">
                            <div class="card card-sm">
                                <div class="card-body" style="min-height: 500px;">
                                    <h4 class="card-title">Personal</h4>
                                    <table class="table">
                                        <tr>
                                            <td>Name</td>
                                            <td><strong>{{$Buyers->name}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Name Khmer</td>
                                            <td class="khmer"><strong>{{$Buyers->namekh}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Sex</td>
                                            <td><strong>{{$Buyers->sex}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Age</td>
                                            <td><strong>{{$Buyers->age}}</strong> years</td>
                                        </tr>
                                        <tr>
                                            <td>Youth</td>
                                            <td><strong>{{$Buyers->youth}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td><strong>{{$Buyers->phone}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><strong>{{$Buyers->email}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td><strong>{{$Buyers->address}}</strong></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="card card-sm">
                                <div class="card-body" style="min-height: 500px;">
                                    <h4 class="card-title"> Business </h4>
                                    <table class="table">
                                        <tr>
                                            <td>Business Duration</td>
                                            <td><strong>{{$Buyers->businessduration}}</strong> years</td>
                                        </tr>
                                        <tr>
                                            <td>Scale of Operation</td>
                                            <td><strong>{{$Buyers->scaleoperation}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Source of Supply</td>
                                            <td><strong>{{$Buyers->sourceofsupply}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Crops Name</td>
                                            <td><strong>{{$Buyers->cropnames}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Subsector</td>
                                            <td><strong>{{$Buyers->subsector}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Buyer Status</td>
                                            <td><strong>{{$Buyers->buyertatus}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td><strong>{{$Buyers->statusdate}}</strong></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                            <div class="col-3">
                                <div class="card card-sm">
                                    <div class="card-body" style="min-height: 500px;">
                                        <h4 class="card-title"> Agreements </h4>
                                        <table class="table">
                                            <tr>
                                                <td>Sale Target</td>
                                                <td><strong>{{$Buyers->saletarget}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>MOU Number</td>
                                                <td><strong>{{$Buyers->mounumber}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>MOU Start Date</td>
                                                <td><strong>{{$Buyers->moustartdate}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>MOU End Date</td>
                                                <td><strong>{{$Buyers->mouenddate}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>MOU Extended Date</td>
                                                <td><strong>{{$Buyers->mouextendeddate}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Type of Agreement</td>
                                                <td><strong>{{$Buyers->typeofagreement}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Lead</td>
                                                <td>{{$Buyers->users->name}}</td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        <div class="col-3">
                            <div class="card card-sm">
                                <div class="card-body" style="min-height: 500px;">
                                    <h4 class="card-title"> Geographic </h4>
                                    <table class="table">
                                        <tr>
                                            <td>Geography</td>
                                            <td><strong>{{$Buyers->provinces->proname}}, {{$Buyers->districts->disname}}, {{$Buyers->communes->comname}}, {{$Buyers->villages->vilname}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Latitude</td>
                                            <td><strong>{{$Buyers->latitude}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Longitude</td>
                                            <td><strong>{{$Buyers->longitude}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Note</td>
                                            <td><strong>{{$Buyers->note}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Created By</td>
                                            <td><strong>{{$Buyers->users->name}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Created Date</td>
                                            <td><strong>{{$Buyers->created_at}}</strong></td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                        <div class="card-footer">
                            <div class="pull-left">
                                <a href="{{ url()->previous() }}" class="btn btn-defaultk ms-auto">Back</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('buyers.edit',$Buyers->id)}}" class="btn btn-primary ms-auto">Edit</a>
                            </div>
                        </div>
                    </div>
                        {{--  end container--}}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
