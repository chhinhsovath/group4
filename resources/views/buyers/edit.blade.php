@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'edit-buyers'
])
@section('content')

    <div class="content">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    {{-- containter --}}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--  end container--}}
                </div>
                <form action="{{ route('buyers.update',$Buyers->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" class="form-control" name="id" value="{{ $Buyers->id }}">
                    <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
                    <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Personel </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 630px;">
                                    {{-- start --}}
                                    <label class="form-label">Name</label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->name}}" class="form-control typeahead" name="name" id="name" placeholder="Name">
                                    </div>

                                    <label class="form-label">Name Khmer</label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->namekh}}"  class="form-control khmer" name="namekh" placeholder="Name Khmer">
                                    </div>

                                    <label class="form-label">Sex </label>
                                    <div class="form-group">
                                        <select class="form-control" name="sex" style=" height:45px;">
                                            <option value="{{$Buyers->sex}}" selected>{{$Buyers->sex}}</option>
                                            <option value=''>sex</option>
                                            <option value='Male'>Male</option>
                                            <option value='Female'>Female</option>
                                        </select>
                                    </div>
                                    <label class="form-label">Age</label>
                                    <div class="form-group">
                                        <input type="number"  value="{{$Buyers->age}}"   class="form-control" name="age" placeholder="age">
                                    </div>
                                    <label class="form-label">Phone</label>
                                    <div class="form-group">
                                        <input type="text"  value="{{$Buyers->phone}}"  class="form-control" name="phone" placeholder="phone">
                                    </div>
                                    <label class="form-label">E-mail</label>
                                    <div class="form-group">
                                        <input type="email"  value="{{$Buyers->email}}"  class="form-control" name="email" placeholder="e-mail">
                                    </div>

                                    <label class="form-label">Address Detail </label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->address}}"   class="form-control" name="address" placeholder="address ">
                                    </div>


                                    <br>
                                    {{-- end --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Business </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 630px;">

                                    <label class="form-label">Business Duration</label>
                                    <div class="form-group">
                                        <input type="number" value="{{$Buyers->businessduration}}" class="form-control" name="businessduration" placeholder="business duration">
                                    </div>

                                    <label class="form-label">Scale of Operation </label>
                                    <div class="form-group">
                                        <select class="form-control select2multipls" multiple name="scaleoperation[]" style=" height:45px;">
                                            @if ($Buyers->scaleoperation != "")
                                                @foreach (array_map('trim', explode(',', $Buyers->scaleoperation)) as $scaleoperation)
                                                    <option value="{{ trim($scaleoperation,'"')}}" selected> {{ trim($scaleoperation,'"')}}</option>
                                                @endforeach
                                            @endif
                                            <option value="Agricultural Cooperative">Agricultural Cooperative</option>
                                            <option value="Collector">Collector</option>
                                            <option value="Company">Company</option>
                                            <option value="Distributor">Distributor</option>
                                            <option value="Processor">Processor</option>
                                            <option value="Retailer">Retailer</option>
                                            <option value="Wholesaler">Wholesaler</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Source of Supply </label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->address}}" class="form-control" name="sourceofsupply" placeholder="source of supply ">
                                    </div>

                                    <label class="form-label">Crop Names</label>
                                    <div class="form-group">
                                        <select name="cropnames[]" multiple class="form-control select2multipls" style=" height:45px;">
                                            @if ($Buyers->cropnames != "")
                                                @foreach (array_map('trim', explode(',', $Buyers->cropnames)) as $cropnames)
                                                    <option value="{{ trim($cropnames,'"')}}" selected> {{ trim($cropnames,'"')}}</option>
                                                @endforeach
                                            @endif
                                            @foreach($Crops as $cropid => $cropname)
                                                <option value="{{$cropname}}"> {{$cropname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="form-label">Sub Sector </label>
                                    <div class="form-group">
                                        <select class="form-control" name="subsector" id="subsector" style=" height:45px;">
                                            <option value="{{$Buyers->subsector}}" selected> {{$Buyers->subsector}} </option>
                                            <option value="">--subsector--</option>
                                            <option value="Cashew">Cashew</option>
                                            <option value="Longan">Longan</option>
                                            <option value="Mango">Mango</option>
                                            <option value="Mixed Fruit">Mixed Fruit</option>
                                            <option value="Pepper Corn">Pepper Corn</option>
                                            <option value="Vegetable">Vegetable</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Status </label>
                                    <div class="form-group">
                                        <select class="form-control" name="buyertatus" style=" height:45px;">
                                            <option value="{{$Buyers->buyertatus}}" selected> {{$Buyers->buyertatus}} </option>
                                            <option value="New">New</option>
                                            <option value="Active">Active</option>
                                            <option value="Graduated">Graduated</option>
                                            <option value="Extended">Extended</option>
                                            <option value="Dropped">Dropped</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Status Date </label>
                                    <div class="form-group">
                                        <input type="date"  value="{{$Buyers->statusdate}}" class="form-control datepicker" name="statusdate" placeholder="Start Date">
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Agreements </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 630px;">

                                    <label class="form-label">Sale Target</label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->saletarget}}" class="form-control" name="saletarget" placeholder="sale target">
                                    </div>

                                    <label class="form-label">MOU Number</label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->mounumber}}" class="form-control" name="mounumber" placeholder="mou number">
                                    </div>

                                    <label class="form-label">MOU Start Date</label>
                                    <div class="form-group">
                                        <input type="date"  value="{{$Buyers->moustartdate}}" class="form-control" name="moustartdate" placeholder="mou start date">
                                    </div>

                                    <label class="form-label">MOU End Date</label>
                                    <div class="form-group">
                                        <input type="date"  value="{{$Buyers->mouenddate}}" class="form-control" name="mouenddate" placeholder="mou end date">
                                    </div>

                                    <label class="form-label">MOU Extended Date</label>
                                    <div class="form-group">
                                        <input type="date"  value="{{$Buyers->mouextendeddate}}" class="form-control" name="mouextendeddate" placeholder="mou extende ddate">
                                    </div>

                                    <label class="form-label">Type of Agreement </label>
                                    <div class="form-group">
                                        <select class="form-control" name="typeofagreement" style=" height:45px;">
                                            <option value="{{$Buyers->typeofagreement}}" selected> {{$Buyers->typeofagreement}} </option>
                                            <option value="MoU">MoU</option>
                                            <option value="NDA">NDA</option>
                                            <option value="Growthplan">Growthplan</option>
                                            <option value="Grant Agreeement">Grant Agreeement</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Lead</label>
                                    <div class="form-group">
                                        <select id="lead" name="lead" multiple class="form-control select2multipls" style=" height:45px;"  required>
                                            @if($Buyers->lead != "")
                                                @foreach (array_map('trim', explode(',', $Buyers->lead)) as $lead)
                                                    <option value="{{ trim($lead,'"')}}" selected> {{ trim($lead,'"')}}</option>
                                                @endforeach
                                            @endif
                                            @foreach($Staffprofiles as $id => $name)
                                                <option value="{{$name}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <br>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <div class="pull-left">
                                        <h4 class="card-title"> Geographic </h4>
                                    </div>
                                </div>
                                <div class="card-body" style="min-height: 630px;">
                                    <label class="form-label">Province</label>
                                    <div class="form-group">
                                        <select id="procode" name="procode" class="form-control" style=" height:45px;">
                                            <option value="{{$Buyers->procode}}" selected> {{$Buyers->provinces->proname}} </option>
                                            @foreach($Provinces as $procode => $proname)
                                                <option value="{{$procode}}"> {{$proname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="form-label">District</label>
                                    <div class="form-group">
                                        <select id="discode" name="discode" class="form-control" style=" height:45px;">
                                            <option value="{{$Buyers->discode}}" selected> {{$Buyers->districts->disname}} </option>
                                        </select>
                                    </div>

                                    <label class="form-label">Commune</label>
                                    <div class="form-group">
                                        <select id="comcode" name="comcode" class="form-control" style=" height:45px;">
                                            <option value="{{$Buyers->comcode}}" selected> {{$Buyers->communes->comname}} </option>
                                        </select>
                                    </div>

                                    <label class="form-label">Village</label>
                                    <div class="form-group">
                                        <select id="vilcode" name="vilcode" class="form-control" style=" height:45px;">
                                            <option value="{{$Buyers->vilcode}}" selected> {{$Buyers->villages->vilname}} </option>
                                        </select>
                                    </div>

                                    <label class="form-label">Latitude</label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->latitude}}" class="form-control" name="latitude" placeholder="latitude">
                                    </div>
                                    <label class="form-label">Longitude</label>
                                    <div class="form-group">
                                        <input type="text" value="{{$Buyers->longitude}}" class="form-control" name="longitude" placeholder="longitude">
                                    </div>
                                    <label class="form-label">Note </label>
                                    <div class="form-group">
                                        <input id="note" value="{{$Buyers->note}}" class="form-control" style="width: 100%;" name="note"></input>
                                    </div>
                                    <br>



                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="pull-left">
                            <a href="{{ url()->previous() }}" class="btn btn-defaultk ms-auto">Back</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary ms-auto">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2multipls').select2({
                placeholder: "Select multiple",
                allowClear: true,
                tags: true,
                tokenSeparators: [',',]
            });
        });

        $('#procode').change(function() {
            var procode = $(this).val();
            $("#discode").empty();
            $("#comcode").empty();
            $("#vilcode").empty();
            if (procode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getDistrict')}}/" + procode,
                    success: function(res) {
                        if (res) {
                            $("#discode").empty();
                            $("#discode").append('<option>Select District</option>');
                            $.each(res, function(key, value) {
                                $("#discode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#discode").empty();
                        }
                    }
                });
            } else {
                $("#discode").empty();
                $("#comcode").empty();
                $("#vilcode").empty();
            }
        });
        $('#discode').on('change', function() {
            var discode = $(this).val();
            $("#comcode").empty();
            $("#vilcode").empty();
            if (discode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getCommune')}}/" + discode,
                    success: function(res) {
                        if (res) {
                            $("#comcode").empty();
                            $("#comcode").append('<option>Select Commune</option>');
                            $.each(res, function(key, value) {
                                $("#comcode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#comcode").empty();
                        }
                    }
                });
            } else {
                $("#comcode").empty();
                $("#vilcode").empty();
            }

        });
        $('#comcode').on('change', function() {
            var comcode = $(this).val();
            $("#vilcode").empty();
            if (comcode) {
                $.ajax({
                    type: "GET",
                    url: "{{url('getVillage')}}/" + comcode,
                    success: function(res) {
                        if (res) {
                            $("#vilcode").empty();
                            $("#vilcode").append('<option>Select Village</option>');
                            $.each(res, function(key, value) {
                                $("#vilcode").append('<option value="' + key + '">' + value + '</option>');
                            });

                        } else {
                            $("#vilcode").empty();
                        }
                    }
                });
            } else {
                // $("#vilcode").empty();
            }

        });
    </script>
@endpush
