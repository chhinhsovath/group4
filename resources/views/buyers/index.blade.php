@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-buyers'
])
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> List of Buyers </h4>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="{{ route('buyers.create') }}"> Create New Buyer</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px; padding-top: 8px;">
                            <form action="{{ route('buyers.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <select class="form-control" id="name" name="filter[name]" style=" height:45px;">
                                                <option value="" selected>Name</option>
                                                @foreach($allBuyers as $allBuyer)
                                                    <option value="{{$allBuyer->name}}">{{$allBuyer->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label">Phone</label>
                                            <input type="text" class="form-control" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control" id="name" name="filter[proname]" style=" height:45px;">
                                                <option value="" selected>Province</option>
                                                @foreach($Provinces as $procode => $proname)
                                                    <option value="{{$proname}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="{{ route('buyers-export') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Export</a>
                                        </div>
                                    </div>

                                </div>
                                </form>

                        </div>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Name Khmer
                                    </th>

                                    <th>
                                        Sex
                                    </th>

                                    <th>
                                        Age
                                    </th>

                                    <th>
                                        Youth
                                    </th>

                                    <th>
                                        Phone
                                    </th>

                                    {{-- <th>
                                        E-mail
                                    </th> --}}

                                    <th>
                                        Province
                                    </th>

                                    {{--
                                    <th>
                                        District
                                    </th>

                                    <th>
                                        Commune
                                    </th>

                                     <th>
                                        Village
                                    </th>
                                    --}}

                                    <th>
                                        Subsector
                                    </th>
                                    <th>
                                        Lead
                                    </th>

                                    <th>
                                        Agreement
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>

                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Buyers as $Buyer)
                                    <tr>
                                        <td> {{ $Buyer->id }} </td>
{{--                                        <td>--}}
{{--                                            <a class="color:#206bc4" href="{{ route('buyers.show', $Buyer->id) }}">{{ $Buyer->name }}</a>--}}
{{--                                        </td>--}}
                                        <td>
                                            {{ $Buyer->name }}
                                        </td>
                                        <td class="khmer">
                                            {{ $Buyer->namekh }}
                                        </td>
                                        <td>
{{--                                            {{ $Buyer->sex }}--}}
                                            @if(!empty($Buyer->sex))
                                                {{$Buyer->sex}}
                                            @else
                                                Na
                                            @endif
                                        </td>
                                        <td>
                                            @if(!empty($Buyer->age))
                                                {{$Buyer->age}}
                                            @else
                                                Na
                                            @endif
                                        </td>
                                        <td>
                                            @if(!empty($Buyer->youth))
                                                {{$Buyer->youth}}
                                            @else
                                                Na
                                            @endif
                                        </td>
                                        <td>
                                            @if(!empty($Buyer->phone))
                                                {{$Buyer->phone}}
                                            @else
                                                Na
                                            @endif
                                        </td>
                                        {{-- <td>
                                            {{ $Buyer->email }}
                                        </td> --}}
                                        <td>
                                            @if(!empty($Buyer->proname))
                                                {{$Buyer->provinces->proname}}
                                            @else
                                                Na
                                            @endif
                                        </td>
                                        {{--
                                        <td>
                                            {{ $Buyer->districts->disname }}
                                        </td>
                                        <td>
                                            {{ $Buyer->communes->comname }}
                                        </td>
                                        <td>
                                            {{ $Buyer->villages->vilname }}
                                        </td>
                                        --}}
                                        <td>
                                            {{ $Buyer->subsector }}
                                        </td>
                                        <td>
                                            @if( $Buyer->lead <>'')
                                                {{$Buyer->lead}}
                                            @else
                                                Na
                                            @endif
                                        </td>
                                        <td>
                                            {{ $Buyer->typeofagreement }}
                                        </td>
                                        <td class="text-center">
                                            @can('creator')
                                                @if($Buyer->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <label class="form-check form-switch m-0">
                                                        Pending
                                                    </label>
                                                @endif
                                            @endcan
                                            @can('approver')
                                                    @if($Buyer->recordstatus=='1')
                                                        <label class="form-check form-switch m-0">
                                                            Approved
                                                        </label>
                                                    @else
                                                        <form action="{{route('buyers.approve')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$Buyer->id}}">
                                                            <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($Buyer->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                        </form>
                                                    @endif

                                            @endcan
                                            @can('admin')
                                                <form action="{{route('buyers.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$Buyer->id}}">
                                                        <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($Buyer->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                </form>
                                            @endcan
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route("buyers.edit",$Buyer->id) }}">Edit</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('buyers.destroy', $Buyer->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$Buyer->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Buyers->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Buyers->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
@endsection
@include("layouts.footer_scrips")
