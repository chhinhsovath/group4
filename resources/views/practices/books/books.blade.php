@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header ">
                        <span class="text-secondary">Laravel 6 Import Export Excel with Heading using Laravel Excel 3.1</span>

                        <div>
                            <div class="float-left" style="padding-left: 50px;">
                                <form class="form-inline" action="{{url('practices/books/import')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <div style="padding-left: 100px;">&nbsp;</div>
                                        <div style="padding-left: 100px;">&nbsp;</div>
                                        <div style="padding-left: 100px;">&nbsp;</div>
                                        <div class="custom-file">
                                            <input type="file"class="custom-file-input" name="imported_file"/>
                                            <label class="custom-file-label">Choose file</label>
                                        </div>

                                    </div>
                                    <div style="padding-left: 100px;">&nbsp;</div>
                                    <button class="btn btn-info" type="submit">Import</button>
                                </form>
                            </div>
                            <div class="float-right">
                                <form action="{{url('practices/books/export')}}" enctype="multipart/form-data">
                                    <button class="btn btn-dark" type="submit">Export</button>
                                </form>
                            </div>
                        </div>
                        <br/>

                        @if(count($books))
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Name</td>
                                    <td>Author</td>
                                </tr>
                                </thead>
                                @foreach($books as $book)
                                    <tr>
                                        <td>{{$book->id}}</td>
                                        <td>{{$book->name}}</td>
                                        <td>{{$book->author}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif

                    </div>

                </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
</script>
@endpush