@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-investments'
])
@section('content')
<div class="content">
    <div class="row">

        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> List of Investments </h4>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="{{ route('investments.create') }}"> Create New Investment</a>
                        </div>
                    </div>
                    <div class="card-body">

                            <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px; padding-top: 8px;">
                                <form action="{{ route('investments.index') }}" method="get">
                                    <div class="row" style=" margin:10px;">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="form-control-label" for="name">Name</label>
                                                <select class="form-control" id="name" name="filter[name]" style=" height:45px;">
                                                    <option value="" selected>Name</option>
                                                    @foreach($Buyers as $id => $name)
                                                        <option value="{{$name}}"> {{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="form-control-label" for="name">Province</label>
                                                <select class="form-control" id="name" name="filter[proname]" style=" height:45px;">
                                                    <option value="" selected>Province</option>
                                                    @foreach($Provinces as $procode => $proname)
                                                        <option value="{{$proname}}"> {{$proname}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                            </div>
                                        </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <a href="{{ route('investments-export') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Export</a>
                                    </div>
                                </div>

                            </div>
                                </form>
                        </div>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        CP.Name
                                    </th>
                                    <th>
                                        Lead.Name
                                    </th>

                                    <th>
                                        Province
                                    </th>


{{--                                    <th>--}}
{{--                                        capitalinvestmentdate--}}
{{--                                    </th>--}}

                                    <th class="text-center">
                                        capital.inv
                                    </th>

{{--                                    <th>--}}
{{--                                        capitalinvestmentnote--}}
{{--                                    </th>--}}

{{--                                    <th>--}}
{{--                                        workingcapitaldate--}}
{{--                                    </th>--}}

                                    <th class="text-center">
                                        working.capital
                                    </th>

{{--                                    <th>--}}
{{--                                        workingcapitalamountnote--}}
{{--                                    </th>--}}

{{--                                    <th>--}}
{{--                                        supplycreditamountdate--}}
{{--                                    </th>--}}

                                    <th class="text-center">
                                        supply.credit
                                    </th>

{{--                                    <th>--}}
{{--                                        supplycreditnote--}}
{{--                                    </th>--}}

                                    <th class="text-center">
                                        operation.expense
                                    </th>

{{--                                    <th>--}}
{{--                                        operationexpensedate--}}
{{--                                    </th>--}}
{{--                                    <th>--}}
{{--                                        operationexpensenote--}}
{{--                                    </th>--}}
                                    <th>
                                        Month.Year
                                    </th>
                                    <th>
                                        Quarter
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>

                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Investments as $investment)
                                    <tr>
                                        <td> {{ $investment->id }} / {{ $investment->oid }}</td>
                                        <td>
                                            @if($investment->name <>'')
                                                {{ $investment->name }}
                                            @else
                                                {{ $investment->buyers->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($investment->staffprofilename <>'')
                                                {{ $investment->staffprofilename }}
                                            @else
                                                {{ $investment->leads->name }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ $investment->provinces->proname }}
                                        </td>
                                        <td class="text-center">
                                            {{ $investment->capitalinvestmentamount }}
                                        </td>
                                        <td class="text-center">
                                            {{ $investment->workingcapitalamount }}
                                        </td>
                                        <td class="text-center">
                                            {{ $investment->supplycreditamount }}
                                        </td>
                                        <td class="text-center">
                                            {{ $investment->operationexpenseamount }}
                                        </td>

                                        <td>
                                            {{ $investment->monthyear }}
                                        </td>
                                        <td>
                                            {{ $investment->quarter }}
                                        </td>

                                        <td class="text-center">
                                            @can('creator')
                                                @if($investment->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <label class="form-check form-switch m-0">
                                                        Pending
                                                    </label>
                                                @endif
                                            @endcan
                                            @can('approver')
                                                @if($investment->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <form action="{{route('investments.approve')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$investment->id}}">
                                                        <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($investment->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                    </form>
                                                @endif

                                            @endcan
                                            @can('admin')
                                                <form action="{{route('investments.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$investment->id}}">
                                                    <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($investment->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                </form>
                                            @endcan
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route("investments.edit",$investment->id) }}">Edit</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('investments.destroy', $investment->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$investment->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Investments->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Investments->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </div>
@endsection
@include("layouts.footer_scrips")
