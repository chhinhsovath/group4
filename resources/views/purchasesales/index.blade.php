@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-purchasesales'
])
@section('content')
<div class="content">
    <div class="row">

        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> List of Purchase and Sale Items Detail </h4>
                    </div>
                    <div class="card-body">

                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px;">
                            <form action="{{ route('purchasesales.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Buyer Name</label>
                                            <select class="form-control select2" name="filter[buyername]">
                                                <option value="">buyer name</option>
                                                @foreach($Purchasesales as $purchasesale)
                                                    <option value="{{$purchasesale->buyername}}">{{$purchasesale->buyername}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Supplier Name</label>
                                            <select class="form-control select2" name="filter[suppliername]">
                                                <option value="">supplier name</option>
                                                @foreach($Purchasesales as $purchasesale)
                                                    <option value="{{$purchasesale->suppliername}}">{{$purchasesale->suppliername}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Subsector</label>
                                            <select class="form-control select2" name="filter[subsector]">
                                                <option value="">subsector</option>
                                                @foreach($Purchasesales as $purchasesale)
                                                    <option value="{{$purchasesale->subsector}}">{{$purchasesale->subsector}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Province</label>
                                            <select class="form-control select2" id="name" name="filter[proname]">
                                                <option value="">Province</option>
                                                @foreach($provinces as $procode => $proname)
                                                    <option value="{{$proname}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px;"> Search </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <form action="{{route('purchasesales-export')}}" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <input type="hidden" name="export" value="purchasesales">
                                                <button class="form-control btn btn-warning" style="margin-top: 24px;" type="submit">Export</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                </form>

                        </div>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Province
                                    </th>
                                    {{-- <th>
                                        District
                                    </th>

                                    <th>
                                        Commune
                                    </th>

                                    <th>
                                        Village
                                    </th>
                                     --}}
                                    <th>
                                        Buyer
                                    </th>

                                    <th>
                                        Supplier
                                    </th>

                                    <th>
                                        Crop
                                    </th>

                                    <th>
                                        Subsector
                                    </th>

                                    <th>
                                        Date
                                    </th>

                                    <th>
                                        P.Unit
                                    </th>

                                    <th>
                                        P.Price
                                    </th>

                                    <th>
                                        P.Total
                                    </th>
                                    <th>
                                        P.Note
                                    </th>

                                    <th>
                                        S.Unit
                                    </th>
                                    <th>
                                        S.Price
                                    </th>
                                    <th>
                                        S.Total
                                    </th>
                                    <th>
                                        S.Note
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>
                                    <th class="text-right">
                                        <button type="button"  onclick="advancedsearch()"class="btn btn-primary btn-sm">Filter</button>
                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($Purchasesales as $purchasesale)
                                    <tr>
                                        <td> {{ $purchasesale->id }} </td>
                                        <td>
                                            {{ $purchasesale->proname }}
                                        </td>
                                        {{-- <td>
                                            {{ $purchasesale->disname }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->comname }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->vilname }}
                                        </td> --}}
                                        <td>
                                            {{ $purchasesale->buyername }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->suppliername }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->cropname }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->subsector }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->date }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->purchaseunit }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->purchaseprice }}
                                        </td>
                                        <td>
                                            $ {{number_format($purchasesale->purchasetotal,2)}}
                                        </td>
                                        <td>
                                            {{ $purchasesale->purchasenote }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->saleunit }}
                                        </td>
                                        <td>
                                            {{ $purchasesale->saleprice }}
                                        </td>
                                        <td>
                                            $ {{number_format($purchasesale->saletotal,2)}}
                                        </td>

                                        <td>
                                            {{ $purchasesale->salenote }}
                                        </td>

                                        <td class="text-center">
                                            @can('creator')
                                                @if($purchasesale->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <label class="form-check form-switch m-0">
                                                        Pending
                                                    </label>
                                                @endif
                                            @endcan
                                            @can('approver')
                                                @if($purchasesale->recordstatus=='1')
                                                    <label class="form-check form-switch m-0">
                                                        Approved
                                                    </label>
                                                @else
                                                    <form action="{{route('purchasesales.approve')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$purchasesale->id}}">
                                                        <input class="form-check-input position-static"  name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($purchasesale->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                    </form>
                                                @endif

                                            @endcan
                                            @can('admin')
                                                <form action="{{route('purchasesales.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$purchasesale->id}}">
                                                    <input class="form-check-input position-static" name="recordstatus" onchange="this.form.submit()"  type="checkbox" @if($purchasesale->recordstatus=='1') value="0" checked @else value="1" @endif >
                                                </form>
                                            @endcan
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route("purchasesales.edit",$purchasesale->id) }}">Edit</a>
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('purchasesales.destroy', $purchasesale->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$purchasesale->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$Purchasesales->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$Purchasesales->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
@endsection
@include("layouts.footer_scrips")
