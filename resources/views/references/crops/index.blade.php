@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-crops'
])
@section('content')
<div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    {{-- start --}}
                    https://www.itsolutionstuff.com/post/laravel-yajra-datatables-export-to-excel-csv-button-exampleexample.html                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2>Crops Management</h2> 
                            </div>
                            <div class="pull-right"> 
                                @can('role-create') 
                                    <a class="btn btn-sm btn-success" href="{{ route('crops.create') }}"> Create New Crop</a> 
                                @endcan 
                            </div>
                        </div>
                    </div> 

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            
                        </div> 
                    @endif
                                    <table class="table table-bordered datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Name Khmer</th>
                                                <th>Base Price</th>
                                                <th>Description</th>
                                                <th width="280px" class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                            
                                    </table>
                                    {{-- @foreach ($crops as $key => $crop)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $crop->name }}</td>
                                                <td>{{ $crop->name_kh }}</td>
                                                <td>{{ $crop->base_price }}</td>
                                                <td>{{ $crop->description }}</td>
                                                <td class="text-right">
                                                    <a href="{{ route('crops.show',$crop->id) }}" class="btn btn-info btn-link btn-icon btn-sm like"><i class="fa fa-eye"></i></a>
                                                    <a href="{{ route('crops.edit',$crop->id) }}" class="btn btn-warning btn-link btn-icon btn-sm edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{ route('references.crops.softdelete',$crop->id) }}" class="btn btn-danger btn-link btn-icon btn-sm delete"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach --}}
                                    {{-- Pagination --}}
                                    {{-- <div class="d-flex justify-content-center">
                                        {{ $crops->links('vendor.pagination.custom') }}
                                    </div> --}}

                    {{-- end --}}
                    </div>
                </div>
          </div>
    </div>
 </div>

@endsection

@push('scripts')
    <script type="text/javascript">
    $(document).ready(function() {
        ajaxTable();
    });
    function ajaxTable(){
           var dataTable = $('.datatable').DataTable({
            //    dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 15,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('crops.getCrops') }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'name_kh', name: 'name_kh'},
                {data: 'base_price', name: 'base_price'},
                {data: 'description', name: 'description'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-right'},
            ],
            "initComplete": function(settings, json) {
                // alert( 'DataTables has finished its initialisation.' );
                // location.reload();
            }
        });
    }

    function refresh(){
        location.reload();
    }
    // Confirm Delete
    $("body").on("click",".getDeleteId",function(){
          event.preventDefault();
          const url = $(this).attr('href');
          swal({
              title: 'Are you sure?',
              text: 'This record and it`s details will be permanantly deleted!',
            //   icon: 'warning',
            //   buttons: ["Cancel", "Yes!"],
            //   showCloseButton: true,
            //   showCancelButton: true,
            //   focusConfirm: false,
            //   onClose:refresh,
            //   title: "Delete?",
                icon: 'question',
                // text: "Please ensure and then confirm!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: !0,
          }).then(function(value) {
            //   alert(value);
              if (value) {
                  window.location.href = url;
                //   alert(url);
                //   location.reload();
                // setTimeout(refresh, 6000);
              }
          });

    });
    
    </script>
@endpush