@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">

        <div class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="card ">
				<div class="card-header ">
					<h4 class="card-title">Stacked Form</h4> </div>
				<div class="card-body ">
					<form method="#" action="#">
						<label>Email address</label>
						<div class="form-group">
							<input type="email" class="form-control" placeholder="Enter email"> </div>
						<label>Password</label>
						<div class="form-group">
							<input type="password" class="form-control" placeholder="Password"> </div>
						<div class="form-check mt-3">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" value=""> Subscribe to newsletter <span class="form-check-sign"></span> </label>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer ">
					<button type="submit" class="btn btn-info btn-round">Submit</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card ">
				<div class="card-header ">
					<h4 class="card-title">Horizontal Form</h4> </div>
				<div class="card-body ">
					<form class="form-horizontal">
						<div class="row">
							<label class="col-md-3 col-form-label">Username</label>
							<div class="col-md-9">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Username"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-md-3 col-form-label">Email</label>
							<div class="col-md-9">
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-md-3 col-form-label">Password</label>
							<div class="col-md-9">
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Password"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-md-3"></label>
							<div class="col-md-9">
								<div class="form-check">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox"> <span class="form-check-sign"></span> Remember me </label>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer ">
					<div class="row">
						<label class="col-md-3"></label>
						<div class="col-md-9">
							<button type="submit" class="btn btn-info btn-round">Sign in</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card ">
				<div class="card-header ">
					<h4 class="card-title">Form Elements</h4> </div>
				<div class="card-body ">
					<form method="get" action="/" class="form-horizontal">
						<div class="row">
							<label class="col-sm-2 col-form-label">With help</label>
							<div class="col-sm-10">
								<div class="form-group">
									<input type="text" class="form-control"> <span class="form-text">A block of help text that breaks onto a new line.</span> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Password</label>
							<div class="col-sm-10">
								<div class="form-group">
									<input type="password" class="form-control"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Placeholder</label>
							<div class="col-sm-10">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="placeholder"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Disabled</label>
							<div class="col-sm-10">
								<div class="form-group">
									<input type="text" class="form-control" value="Disabled input here.." disabled=""> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Static control</label>
							<div class="col-sm-10">
								<div class="form-group">
									<p class="form-control-static">hello@creative-tim.com</p>
								</div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Checkboxes and radios</label>
							<div class="col-sm-10 checkbox-radios">
								<div class="form-check">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox"> <span class="form-check-sign"></span> First Checkbox </label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox"> <span class="form-check-sign"></span> Second Checkbox </label>
								</div>
								<div class="form-check-radio">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="exampleRadioz" id="exampleRadios11" value="option1"> First Radio <span class="form-check-sign"></span> </label>
								</div>
								<div class="form-check-radio">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="exampleRadioz" id="exampleRadios12" value="option2" checked=""> Second Radio <span class="form-check-sign"></span> </label>
								</div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Inline checkboxes</label>
							<div class="col-sm-10">
								<div class="form-check form-check-inline">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox" checked=""> <span class="form-check-sign"></span> a </label>
								</div>
								<div class="form-check form-check-inline">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox"> <span class="form-check-sign"></span> b </label>
								</div>
								<div class="form-check form-check-inline">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox"> <span class="form-check-sign"></span> c </label>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card ">
				<div class="card-header ">
					<h4 class="card-title">Input Variants</h4> </div>
				<div class="card-body ">
					<form method="get" action="/" class="form-horizontal">
						<div class="row">
							<label class="col-sm-2 col-form-label">Custom Checkboxes &amp; radios</label>
							<div class="col-sm-4 col-sm-offset-1 checkbox-radios">
								<div class="form-check">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox"> <span class="form-check-sign"></span> Unchecked </label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox" checked=""> <span class="form-check-sign"></span> Checked </label>
								</div>
								<div class="form-check disabled">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox" disabled=""> <span class="form-check-sign"></span> Disabled Unchecked </label>
								</div>
								<div class="form-check disabled">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox" disabled=""> <span class="form-check-sign"></span> Disabled Checked </label>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3">
								<div class="form-check-radio">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1"> Radio is off <span class="form-check-sign"></span> </label>
								</div>
								<div class="form-check-radio">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" checked=""> Radio is on <span class="form-check-sign"></span> </label>
								</div>
								<div class="form-check-radio disabled">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled=""> Disabled radio is off <span class="form-check-sign"></span> </label>
								</div>
								<div class="form-check-radio disabled">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="exampleRadioz" id="exampleRadios4" value="option4" checked="" disabled=""> Disabled radio is on <span class="form-check-sign"></span> </label>
								</div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Input with success</label>
							<div class="col-sm-10">
								<div class="form-group has-success">
									<input type="text" class="form-control" value="Success"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Input with error</label>
							<div class="col-sm-10">
								<div class="form-group has-danger">
									<input type="text" class="form-control" value="Error"> </div>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-2 col-form-label">Column sizing</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<input type="text" class="form-control" placeholder=".col-md-3"> </div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input type="text" class="form-control" placeholder=".col-md-4"> </div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<input type="text" class="form-control" placeholder=".col-md-5"> </div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<h5>Sweet Alert 2</h5>
        <p>A beautiful plugin, that replace the classic alert, Handcrafted by our friend
            <a target="_blank" href="https://twitter.com/t4t5">Tristan Edwards</a>. Please check out the
            <a href="https://sweetalert2.github.io/" target="_blank">full documentation.</a>
        </p>
        <div class="places-sweet-alerts">
            <div class="row">
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">Basic example</p>
                            <button class="btn btn-outline-default btn-round" onclick="demo.showSwal('basic')">Try
                                me!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">A title with a text under</p>
                            <button class="btn btn-outline-default btn-round" onclick="demo.showSwal('title-and-text')">Try
                                me!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">A success message</p>
                            <button class="btn btn-outline-default btn-round" onclick="demo.showSwal('success-message')">Try
                                me!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">Custom HTML description</p>
                            <button class="btn btn-outline-default btn-round" onclick="demo.showSwal('custom-html')">Try
                                me!</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">A warning message, with a function attached to the "Confirm" Button...</p>
                            <button class="btn btn-outline-default btn-round"
                                onclick="demo.showSwal('warning-message-and-confirmation')">Try me!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">...and by passing a parameter, you can execute something else for "Cancel"
                            </p>
                            <button class="btn btn-outline-default btn-round"
                                onclick="demo.showSwal('warning-message-and-cancel')">Try me!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">A message with auto close timer set to 2 seconds</p>
                            <button class="btn btn-outline-default btn-round" onclick="demo.showSwal('auto-close')">Try
                                me!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card ">
                        <div class="card-body text-center">
                            <p class="card-text">Modal window with input field</p>
                            <button class="btn btn-outline-default btn-round" onclick="demo.showSwal('input-field')">Try
                                me!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card ">

                    
                    <div class="card-header">
                            {{ trans('global.create') }} {{ trans('cruds.khmergeo.title_singular') }}
                        </div>

                        {{-- <div class="card-body">
                            <form method="POST" action="{{ route("khmergeos.store") }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="required" for="vil_name">{{ trans('cruds.khmergeo.fields.vil_name') }}</label>
                                    <input class="form-control {{ $errors->has('vil_name') ? 'is-invalid' : '' }}" type="text" name="vil_name" id="vil_name" value="{{ old('vil_name', '') }}" required>
                                    @if($errors->has('vil_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('vil_name') }}
                                    </div>
                                    @endif
                                    <span class="help-block">{{ trans('cruds.khmergeo.fields.vil_name_helper') }}</span>
                                </div>
                                <div class="form-group">
                                    <label class="required" for="vil_code">{{ trans('cruds.khmergeo.fields.vil_code') }}</label>
                                    <input class="form-control {{ $errors->has('vil_code') ? 'is-invalid' : '' }}" type="text" name="vil_code" id="vil_code" value="{{ old('vil_code', '') }}" required>
                                    @if($errors->has('vil_code'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('vil_code') }}
                                    </div>
                                    @endif
                                    <span class="help-block">{{ trans('cruds.khmergeo.fields.vil_code_helper') }}</span>
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-danger" type="submit">
                                        {{ trans('global.save') }}
                                    </button>
                                </div>
                            </form>
                        </div> --}}
                        <div class="card-body">
                            <div class="container">
                                <div class="form-group">
                                    <label for="province">Province:</label>
                                    <select id="pro_code" name="pro_code" class="form-control">
                                            <option value="" selected disabled>Select Province</option> 
                                            @foreach($provinces as $key => $province)
                                                <option value="{{$province->pro_code}}"> {{$province->pro_name}}</option> 
                                            @endforeach 
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="dis_code">District:</label>
                                    <select name="dis_code" id="dis_code" class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label for="com_code">Commune:</label>
                                    <select name="com_code" id="com_code" class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label for="vil_code">Village:</label>
                                    <select name="vil_code" id="vil_code" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // $(document).ready(function() {

        // });

$('#pro_code').change(function() {
	var pro_code = $(this).val();
    $("#dis_code").empty();
    $("#com_code").empty();
    $("#vil_code").empty();
	if (pro_code) {
		$.ajax({
			type: "GET",
			url: "{{url('getDistrict')}}?pro_code=" + pro_code,
			success: function(res) {
				if (res) {
					$("#dis_code").empty();
					$("#dis_code").append('<option>Select District</option>');
					$.each(res, function(key, value) {
						$("#dis_code").append('<option value="' + key + '">' + value + '</option>');
					});

				} else {
					$("#dis_code").empty();
				}
			}
		});
	} else {
		$("#dis_code").empty();
		$("#com_code").empty();
		$("#vil_code").empty();
	}
});
$('#dis_code').on('change', function() {
	var dis_code = $(this).val();
    $("#com_code").empty();
    $("#vil_code").empty();
	if (dis_code) {
		$.ajax({
			type: "GET",
			url: "{{url('getCommune')}}?dis_code=" + dis_code,
			success: function(res) {
				if (res) {
					$("#com_code").empty();
					$("#com_code").append('<option>Select Commune</option>');
					$.each(res, function(key, value) {
						$("#com_code").append('<option value="' + key + '">' + value + '</option>');
					});

				} else {
					$("#com_code").empty();
				}
			}
		});
	} else {
		$("#com_code").empty();
        $("#vil_code").empty();
	}

});
$('#com_code').on('change', function() {
	var com_code = $(this).val();
    $("#vil_code").empty();
	if (com_code) {
		$.ajax({
			type: "GET",
			url: "{{url('getVillage')}}?com_code=" + com_code,
			success: function(res) {
				if (res) {
					$("#vil_code").empty();
					$("#vil_code").append('<option>Select Village</option>');
					$.each(res, function(key, value) {
						$("#vil_code").append('<option value="' + key + '">' + value + '</option>');
					});

				} else {
					$("#vil_code").empty();
				}
			}
		});
	} else {
		// $("#vil_code").empty();
	}

});           
  </script>
@endpush