@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-menus'
])

@section('content')
 <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    {{-- start --}}


                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2>Khmergeo Management</h2> </div>
                            <div class="pull-right"> 
                                <!-- Button trigger modal -->
                                <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Import</a>
                                <a class="btn btn-success" href="{{ route('khmergeos.file-export') }}">Export data</a>
                                @can('role-create') 
                                    <a class="btn btn-success" href="{{ route('khmergeos.create') }}"> Create New Geographic Area</a> 
                                @endcan 
                            </div>
                        </div>
                    </div> 

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div> 
                    @endif

                        {{-- Adavnce search --}}
                        <table id="datatable" class="table table-striped table-bordered datatable" cellspacing="0" width="100%">    
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Province Code</th>
                                    <th>Province Name</th>
                                    <th>District Code</th>
                                    <th>District Name</th>
                                    <th>Commune Code</th>
                                    <th>Commune Name</th>
                                    <th>Village Code</th>
                                    <th>Village Name</th>
                                    <th>Lattitude</th>
                                    <th>Longtitude</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Import Khmergeo From Excel</h5>
        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> --}}
      </div>
      <div class="modal-body">
        
        <form class="form-inline" role="form" action="{{ route('khmergeos.file-import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="col-md-6 col-sm-6">
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <span class="btn btn-rose btn-round btn-file">
                            <span class="fileinput-new">Select Excel</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="file" class="custom-file-input" id="customFile">
                        </span>
                        <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <button class="btn btn-primary btn-round btn-file">Import data</button>
            </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{--  --}}
<!-- Delete  Modal -->
<div class="modal" id="DeleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"> Delete</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h4>Are you sure want to delete this ?</h4>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="SubmitDeleteForm">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
       $(document).ready(function() {
        // init datatable.
        //https://datatables.net/forums/discussion/33368/jquery-datatable-serverside-search-on-enter
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('get-khmergeos') }}',
            columns: [
                    {data: 'id', name: 'id'},
                    {data: 'pro_code', name: 'pro_code'},
                    {data: 'pro_name', name: 'pro_name'},
                    {data: 'dis_code', name: 'dis_code'},
                    {data: 'dis_name', name: 'dis_name'},
                    {data: 'com_code', name: 'com_code'},
                    {data: 'com_name', name: 'com_name'},
                    {data: 'vil_code', name: 'vil_code'},
                    {data: 'vil_name', name: 'vil_name'},
                    {data: 'lattitude', name: 'lattitude'},
                    {data: 'longtitute', name: 'longtitute'},
                    {data: 'Actions', name: 'Actions',orderable:false,searchable:false,sClass:'text-center'},
            ]

            
        });

        

        // Create article Ajax request.
        $('#SubmitCreateForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('khmergeos.store') }}",
                data: $('form').serialize(),
                method: 'post',
                data: {
                    title: $('#title').val(),
                    description: $('#description').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){
                            $('.alert-success').hide();
                            $('#CreateModal').modal('hide');
                            location.reload();
                        }, 2000);
                    }
                }
            });
        });

        // Get single article in EditModel
        $('.modelClose').on('click', function(){
            $('#EditModal').hide();
        });

        var id;
        $('body').on('click', '#getEditData', function(e) {
            // e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: "khmergeos/"+id+"/edit",
                method: 'GET',
                // data: {
                //     id: id,
                // },
                success: function(result) {
                    console.log(result);
                    $('#EditModalBody').html(result.html);
                    $('#EditModal').show();
                }
            });
        });

        // Update article Ajax request.
        $('#SubmitEditForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "khmergeos/"+id,
                method: 'PUT',
                data: {
                    title: $('#editTitle').val(),
                    description: $('#editDescription').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){
                            $('.alert-success').hide();
                            $('#EditModal').hide();
                            location.reload();
                        }, 2000);

                    }
                }
            });
        });

        // Delete article Ajax request.
        var deleteID;
        $('body').on('click', '#getDeleteId', function(){
            deleteID = $(this).data('id');
        })
        
        $('#SubmitDeleteForm').click(function(e) {
            e.preventDefault();
            var id = deleteID;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "khmergeos/"+id,
                method: 'DELETE',
                success: function(result) {
                    setInterval(function(){
                        // $('.datatable').DataTable().ajax.reload();
                        $('#DeleteModal').hide();
                        location.reload();
                    }, 1000);
                }
            });
        });
    });


    function deleteConfirmation(id) {
    swal({
        title: "Delete?",
        text: "Please ensure and then confirm!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: !0
        }).then(function(e) {
            if (e.value === true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: 'POST',
                    url: "{{url('/delete')}}/" + id,
                    data: {
                        _token: CSRF_TOKEN
                    },
                    dataType: 'JSON',
                    success: function(results) {
                        if (results.success === true) {
                            swal("Done!", results.message, "success");
                        } else {
                            swal("Error!", results.message, "error");
                        }
                    }
                });
            } else {
                e.dismiss;
            }
        }, function(dismiss) {
            return false;
        })
    }
</script>
@endpush
