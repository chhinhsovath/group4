@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.khmergeo.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.khmergeos.update", [$khmergeo->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="vil_name">{{ trans('cruds.khmergeo.fields.vil_name') }}</label>
                <input class="form-control {{ $errors->has('vil_name') ? 'is-invalid' : '' }}" type="text" name="vil_name" id="vil_name" value="{{ old('vil_name', $khmergeo->vil_name) }}" required>
                @if($errors->has('vil_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('vil_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.khmergeo.fields.vil_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="vil_code">{{ trans('cruds.khmergeo.fields.vil_code') }}</label>
                <input class="form-control {{ $errors->has('vil_code') ? 'is-invalid' : '' }}" type="text" name="vil_code" id="vil_code" value="{{ old('vil_code', $khmergeo->vil_code) }}" required>
                @if($errors->has('vil_code'))
                    <div class="invalid-feedback">
                        {{ $errors->first('vil_code') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.khmergeo.fields.vil_code_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection