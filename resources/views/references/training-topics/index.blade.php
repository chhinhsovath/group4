@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-xx'
])
@section('content')
     <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    {{-- start --}}

                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2>Training Topics Management</h2> </div>
                            <div class="pull-right"> 
                                @can('role-create') 
                                    <a class="btn btn-success" href="{{ route('permissions.create') }}"> Create New Permission</a> 
                                @endcan 
                            </div>
                        </div>
                    </div> 

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div> 
                    @endif

                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    {{-- <th>
                                    <select name="name" id="name" class="form-control">
                                        <option value="">Select Name</option>
                                        @foreach($trainingtopics as $row)
                                            <option value="{{ $row->name }}">{{ $row->name }}</option>
                                        @endforeach
                                        </select>
                                    </th> --}}
                                    <th>Name Khmer</th>
                                    <th>Note</th>
                                    <th width="150" class="text-right">Action</th>
                                </tr>
                            </thead>
                        </table>
                        
                    </div>


                        <!-- Create Article Modal -->
                        <div class="modal" id="CreateArticleModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-name">Article Create</h4>
                                        {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                                            <strong>Success!</strong>Article was added successfully.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Title:</label>
                                            <input type="text" class="form-control" name="name" id="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Title Khmer:</label>
                                            <input type="text" class="form-control" name="name_kh" id="name_kh">
                                        </div>
                                        <div class="form-group">
                                            <label for="note">Description:</label>
                                            <textarea class="form-control" name="note" id="note">                        
                                            </textarea>
                                        </div>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <div class="d-flex justify-content-center">
                                            <button type="button" class="btn btn-success" id="SubmitCreateArticleForm">Create</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Article Modal -->
                        <div class="modal" id="EditArticleModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-name">Article Edit</h4>
                                        <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                                            <strong>Success!</strong>Article was added successfully.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div id="EditArticleModalBody">
                                            
                                        </div>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <div class="d-flex justify-content-center">
                                            <button type="button" class="btn btn-success" id="SubmitEditArticleForm">Update</button>
                                            <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Delete Article Modal -->
                        <div class="modal" id="DeleteArticleModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-name">Article Delete</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                                            <strong>Success!</strong>Article was added successfully.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <h4>Are you sure want to delete this Article?</h4>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <div class="d-flex justify-content-center">
                                            <button type="button" class="btn btn-danger" id="SubmitDeleteArticleForm">Yes</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- end --}}
                    </div>
                </div>
          </div>
    </div>
 </div>
@endsection

@push('scripts')
    <script type="text/javascript">

    
    $(document).ready(function() {
        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('training-topics.getTrainingTopics') }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'name_kh', name: 'name_kh'},
                {data: 'note', name: 'note'},
                {data: 'Actions', name: 'Actions',orderable:true,serachable:true,sClass:'text-center'},
            ]
        });

        // Create article Ajax request.
        $('#SubmitCreateArticleForm').click(function(e) {
            e.preventDefault();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('training-topics.store') }}",
                data: $('form').serialize(),
                method: 'post',
                data: {
                    name: $('#name').val(),
                    name_kh: $('#name_kh').val(),
                    note: $('#note').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        // $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){ 
                            $('.alert-success').hide();
                            $('#CreateArticleModal').modal('hide');
                            location.reload();
                        }, 2000);
                    }
                }
            });
        });

        // Get single article in EditModel
        $('.modelClose').on('click', function(){
            $('#EditArticleModal').hide();
        });
        var id;
        $('body').on('click', '#getEditArticleData', function(e) {
            // e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: "training-topics/"+id+"/edit",
                method: 'GET',
                // data: {
                //     id: id,
                // },
                success: function(result) {
                    console.log(result);
                    $('#EditArticleModalBody').html(result.html);
                    $('#EditArticleModal').show();
                }
            });
        });

        // Update article Ajax request.
        $('#SubmitEditArticleForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "training-topics/"+id,
                method: 'PUT',
                data: {
                    name: $('#editname').val(),
                    name_kh: $('#editname_kh').val(),
                    note: $('#editnote').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        // $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){ 
                            $('.alert-success').hide();
                            $('#EditArticleModal').hide();
                            location.reload();
                        }, 2000);
                    }
                }
            });
        });

        // Delete article Ajax request.
        var deleteID;
        $('body').on('click', '#getDeleteId', function(){
            deleteID = $(this).data('id');
        })

        $('#SubmitDeleteArticleForm').click(function(e) {
            e.preventDefault();
            var id = deleteID;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "training-topics/"+id,
                method: 'DELETE',
                success: function(result) {
                    // setInterval(function(){ 
                        // $('.datatable').DataTable().ajax.reload();
                        // $('#DeleteArticleModal').hide();
                        location.reload();
                    // }, 1000);
                }
            });
        });
    });
</script>
@endpush

{{-- @section('script')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
@endsection --}}
