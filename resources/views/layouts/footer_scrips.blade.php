@push('scripts')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var id = $(this).data("id");
            event.preventDefault();
            swal({
                title: `Are you sure you want to delete this record?`,
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                    // $(".preloader").fadeIn();
                });
        });

        $(document).ready(function() {
            $('.select2').select2({
                placeholder: "Select multiple",
                allowClear: true,
                tags: true,
                tokenSeparators: [',',]
            });
        });
        function advancedsearch() {
            var x = document.getElementById("advancedsearch");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
@endpush
