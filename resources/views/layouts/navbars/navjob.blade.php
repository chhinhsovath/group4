{{--Jobs--}}
<li class="{{ $elementActive == 'list-jobs' || $elementActive == 'show-jobs' || $elementActive == 'create-jobs' || $elementActive == 'edit-jobs' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_jobs">
        <i class="nc-icon nc-single-02"></i>
        <p>
            {{ __('JOBS') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'list-jobs' || $elementActive == 'show-jobs' || $elementActive == 'create-jobs' || $elementActive == 'edit-jobs' ? 'show' : '' }}" id="menu_jobs">
        <ul class="nav">
            <li class="{{ $elementActive == 'list-jobs' ? 'active' : '' }}">
                <a href="{{ route('jobs.index') }}">
                    <span class="sidebar-normal">{{ __(' List Jobs ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'create-jobs' ? 'active' : '' }}">
                <a href="{{ route('jobs.create') }}">
                    <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                </a>
            </li>
            @if ($elementActive == 'show-jobs')
                <li class="{{ $elementActive == 'show-jobs' ? 'active' : '' }}">
                    <a href="#show_job">
                        <span class="sidebar-normal">{{ __(' Show Job ') }}</span>
                    </a>
                </li>

            @endif
            @if ($elementActive == 'edit-jobs')
                <li class="{{ $elementActive == 'edit-jobs' ? 'active' : '' }}">
                    <a href="#edit_job">
                        <span class="sidebar-normal">{{ __(' Edit Job ') }}</span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</li>
{{--End Job--}}
