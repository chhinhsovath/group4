{{--Loans--}}
<li class="{{ $elementActive == 'list-loans' || $elementActive == 'show-loans' || $elementActive == 'create-loans' || $elementActive == 'edit-loans' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_loans">
        <i class="nc-icon nc-single-02"></i>
        <p>
            {{ __('LOANS') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'list-loans' || $elementActive == 'show-loans' || $elementActive == 'create-loans' || $elementActive == 'edit-loans' ? 'show' : '' }}" id="menu_loans">
        <ul class="nav">
            <li class="{{ $elementActive == 'list-loans' ? 'active' : '' }}">
                <a href="{{ route('loans.index') }}">
                    <span class="sidebar-normal">{{ __(' List Loans ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'create-loans' ? 'active' : '' }}">
                <a href="{{ route('loans.create') }}">
                    <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                </a>
            </li>
            @if ($elementActive == 'show-loans')
                <li class="{{ $elementActive == 'show-loans' ? 'active' : '' }}">
                    <a href="#show_loan">
                        <span class="sidebar-normal">{{ __(' Show Loan ') }}</span>
                    </a>
                </li>

            @endif
            @if ($elementActive == 'edit-loans')
                <li class="{{ $elementActive == 'edit-loans' ? 'active' : '' }}">
                    <a href="#edit_loan">
                        <span class="sidebar-normal">{{ __(' Edit Loan ') }}</span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</li>
{{--End Loan--}}
