{{--Investments--}}
<li class="{{ $elementActive == 'list-investments' || $elementActive == 'show-investments' || $elementActive == 'create-investments' || $elementActive == 'edit-investments' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_investments">
        <i class="nc-icon nc-single-02"></i>
        <p>
            {{ __('INVESTMENTS') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'list-investments' || $elementActive == 'show-investments' || $elementActive == 'create-investments' || $elementActive == 'edit-investments' ? 'show' : '' }}" id="menu_investments">
        <ul class="nav">
            <li class="{{ $elementActive == 'list-investments' ? 'active' : '' }}">
                <a href="{{ route('investments.index') }}">
                    <span class="sidebar-normal">{{ __(' List Investments ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'create-investments' ? 'active' : '' }}">
                <a href="{{ route('investments.create') }}">
                    <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                </a>
            </li>
            @if ($elementActive == 'show-investments')
                <li class="{{ $elementActive == 'show-investments' ? 'active' : '' }}">
                    <a href="#show_investment">
                        <span class="sidebar-normal">{{ __(' Show Investment ') }}</span>
                    </a>
                </li>

            @endif
            @if ($elementActive == 'edit-investments')
                <li class="{{ $elementActive == 'edit-investments' ? 'active' : '' }}">
                    <a href="#edit_investment">
                        <span class="sidebar-normal">{{ __(' Edit Investment ') }}</span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</li>
