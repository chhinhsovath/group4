{{--Bmps--}}
<li class="{{ $elementActive == 'list-bmps' || $elementActive == 'show-bmps' || $elementActive == 'create-bmps' || $elementActive == 'edit-bmps' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_bmps">
        <i class="nc-icon nc-single-02"></i>
        <p>
            {{ __('BMPs') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'list-bmps' || $elementActive == 'show-bmps' || $elementActive == 'create-bmps' || $elementActive == 'edit-bmps' ? 'show' : '' }}" id="menu_bmps">
        <ul class="nav">
            <li class="{{ $elementActive == 'list-bmps' ? 'active' : '' }}">
                <a href="{{ route('bmps.index') }}">
                    <span class="sidebar-normal">{{ __(' List Bmps ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'create-bmps' ? 'active' : '' }}">
                <a href="{{ route('bmps.create') }}">
                    <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                </a>
            </li>
            @if ($elementActive == 'show-bmps')
                <li class="{{ $elementActive == 'show-bmps' ? 'active' : '' }}">
                    <a href="#show_bmp">
                        <span class="sidebar-normal">{{ __(' Show Bmp ') }}</span>
                    </a>
                </li>

            @endif
            @if ($elementActive == 'edit-bmps')
                <li class="{{ $elementActive == 'edit-bmps' ? 'active' : '' }}">
                    <a href="#edit_bmp">
                        <span class="sidebar-normal">{{ __(' Edit Bmp ') }}</span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</li>
{{--End Bmp--}}
