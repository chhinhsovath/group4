{{--Producers--}}
<li class="{{ $elementActive == 'list-producers' || $elementActive == 'show-producers' || $elementActive == 'create-producers' || $elementActive == 'edit-producers' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_producers">
        <i class="nc-icon nc-single-02"></i>
        <p>
            {{ __('Producers') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'list-producers' || $elementActive == 'show-producers' || $elementActive == 'create-producers' || $elementActive == 'edit-producers' ? 'show' : '' }}" id="menu_producers">
        <ul class="nav">
            <li class="{{ $elementActive == 'list-producers' ? 'active' : '' }}">
                <a href="{{ route('producers.index') }}">
                    <span class="sidebar-normal">{{ __(' List Producers ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'create-producers' ? 'active' : '' }}">
                <a href="{{ route('producers.create') }}">
                    <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                </a>
            </li>
            @if ($elementActive == 'show-producers')
                <li class="{{ $elementActive == 'show-producers' ? 'active' : '' }}">
                    <a href="#show_bmp">
                        <span class="sidebar-normal">{{ __(' Show Producer ') }}</span>
                    </a>
                </li>

            @endif
            @if ($elementActive == 'edit-producers')
                <li class="{{ $elementActive == 'edit-producers' ? 'active' : '' }}">
                    <a href="#edit_bmp">
                        <span class="sidebar-normal">{{ __(' Edit Producer ') }}</span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</li>
{{--End Producer--}}
