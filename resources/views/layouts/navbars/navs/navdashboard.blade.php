{{--Dashboard--}}
<li class="{{ $elementActive == 'db-overview' || $elementActive == 'db-charts' || $elementActive == 'db-project-year' || $elementActive == 'db-quarter' || $elementActive == 'db-subsector' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_db">
        <i class="nc-icon nc-bank"></i>
        <p>
            {{ __('Dashboard') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'db-overview' || $elementActive == 'db-charts' || $elementActive == 'db-project-year' || $elementActive == 'db-quarter' || $elementActive == 'db-subsector' ? 'show' : '' }}" id="menu_db">
        <ul class="nav">
            <li class="{{ $elementActive == 'db-overview' ? 'active' : '' }}">
                <a href="{{ route('bmps.index') }}">
                    <span class="sidebar-normal">{{ __(' Overview ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'db-charts' ? 'active' : '' }}">
                <a href="{{ route('bmps.create') }}">
                    <span class="sidebar-normal">{{ __(' Charts ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'db-project-year' ? 'active' : '' }}">
                <a href="{{ route('bmps.create') }}">
                    <span class="sidebar-normal">{{ __(' Project Year ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'db-quarter' ? 'active' : '' }}">
                <a href="{{ route('bmps.create') }}">
                    <span class="sidebar-normal">{{ __(' Quarter ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'db-subsector' ? 'active' : '' }}">
                <a href="{{ route('bmps.create') }}">
                    <span class="sidebar-normal">{{ __(' Subsector ') }}</span>
                </a>
            </li>


        </ul>
    </div>
</li>
{{--End Dashboard--}}
