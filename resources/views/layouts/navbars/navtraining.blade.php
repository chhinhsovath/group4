{{--MainTrainings--}}
<li class="{{ $elementActive == 'list-trainings' || $elementActive == 'show-trainings' || $elementActive == 'create-trainings' || $elementActive == 'edit-trainings' ? 'active' : '' }}">
    <a data-toggle="collapse" aria-expanded="true" href="#menu_trainings">
        <i class="nc-icon nc-single-02"></i>
        <p>
            {{ __('TRAINING') }}
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse {{ $elementActive == 'list-trainings' || $elementActive == 'show-trainings' || $elementActive == 'create-trainings' || $elementActive == 'edit-trainings' ? 'show' : '' }}" id="menu_trainings">
        <ul class="nav">
            <li class="{{ $elementActive == 'list-trainings' ? 'active' : '' }}">
                <a href="{{ route('trainings.index') }}">
                    <span class="sidebar-normal">{{ __(' List MainTrainings ') }}</span>
                </a>
            </li>
            <li class="{{ $elementActive == 'create-trainings' ? 'active' : '' }}">
                <a href="{{ route('trainings.create') }}">
                    <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                </a>
            </li>
            @if ($elementActive == 'show-trainings')
                <li class="{{ $elementActive == 'show-trainings' ? 'active' : '' }}">
                    <a href="#show_training">
                        <span class="sidebar-normal">{{ __(' Show Training ') }}</span>
                    </a>
                </li>

            @endif
            @if ($elementActive == 'edit-trainings')
                <li class="{{ $elementActive == 'edit-trainings' ? 'active' : '' }}">
                    <a href="#edit_training">
                        <span class="sidebar-normal">{{ __(' Edit Training ') }}</span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</li>
{{--End Training--}}
