<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="http://monitoring" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="http://reports.local" class="simple-text logo-normal">
            {{ __('HARVESTII') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
{{--            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">--}}
{{--                <a href="{{ route('page.index', 'dashboard') }}">--}}
{{--                    <i class="nc-icon nc-bank"></i>--}}
{{--                    <p>{{ __('Dashboard') }}</p>--}}
{{--                </a>--}}

{{--            </li>--}}

            {{--Dashboard--}}
            <li class="{{ $elementActive == 'dashboard' || $elementActive == 'db-charts' || $elementActive == 'db-project-year' || $elementActive == 'db-quarter' || $elementActive == 'db-subsector' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_db">
                    <i class="nc-icon nc-bank"></i>
                    <p>
                        {{ __('Dashboard') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'dashboard' || $elementActive == 'db-charts' || $elementActive == 'db-project-year' || $elementActive == 'db-quarter' || $elementActive == 'db-subsector' ? 'show' : '' }}" id="menu_db">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                            <a href="{{ route('dashboard.home') }}">
                                <span class="sidebar-normal">{{ __(' Overview ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'db-charts' ? 'active' : '' }}">
                            <a href="{{ route('dashboard.charts') }}">
                                <span class="sidebar-normal">{{ __(' Charts ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'db-project-year' ? 'active' : '' }}">
                            <a href="{{ route('dashboard.projectyears') }}">
                                <span class="sidebar-normal">{{ __(' Project Year ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'db-quarter' ? 'active' : '' }}">
                            <a href="{{ route('dashboard.quarters') }}">
                                <span class="sidebar-normal">{{ __(' Quarter ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'db-subsector' ? 'active' : '' }}">
                            <a href="{{ route('dashboard.subsectors') }}">
                                <span class="sidebar-normal">{{ __(' Subsector ') }}</span>
                            </a>
                        </li>


                    </ul>
                </div>
            </li>
            {{--End Dashboard--}}


            <li class="{{ $elementActive == 'list-all-in-one-contact' || $elementActive == 'show-all-in-one-contact' || $elementActive == 'create-all-in-one-contact' || $elementActive == 'edit-all-in-one-contact'  || $elementActive == 'import-contacts'? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_all_in_one_contact">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('ALL CONTACTS') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-all-in-one-contact' || $elementActive == 'show-all-in-one-contact' || $elementActive == 'create-all-in-one-contact' || $elementActive == 'edit-all-in-one-contact' ? 'show' : '' }}" id="menu_all_in_one_contact">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-all-in-one-contact' ? 'active' : '' }}">
                            <a href="{{ route('allinonecontacts.index') }}">
                                <span class="sidebar-normal">{{ __(' List All Contacts ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-all-in-one-contact' ? 'active' : '' }}">
                            <a href="{{ route('allinonecontacts.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-all-in-one-contact')
                            <li class="{{ $elementActive == 'show-all-in-one-contact' ? 'active' : '' }}">
                                <a href="#show-all-in-one-contact">
                                    <span class="sidebar-normal">{{ __(' Show All Contact ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-all-in-one-contact')
                            <li class="{{ $elementActive == 'edit-all-in-one-contact' ? 'active' : '' }}">
                                <a href="#edit-all-in-one-contact">
                                    <span class="sidebar-normal">{{ __(' Edit All Contact ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'import-contacts')
                            <li class="{{ $elementActive == 'import-contacts' ? 'active' : '' }}">
                                <a href="#import-contacts">
                                    <span class="sidebar-normal">{{ __(' Import Contact ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>

            <li class="{{ $elementActive == 'list-buyers' || $elementActive == 'show-buyers' || $elementActive == 'create-buyers' || $elementActive == 'edit-buyers' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_buyers">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('BUYERS') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-buyers' || $elementActive == 'show-buyers' || $elementActive == 'create-buyers' || $elementActive == 'edit-buyers' ? 'show' : '' }}" id="menu_buyers">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-buyers' ? 'active' : '' }}">
                            <a href="{{ route('buyers.index') }}">
                                <span class="sidebar-normal">{{ __(' List Buyers ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-buyers' ? 'active' : '' }}">
                            <a href="{{ route('buyers.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-buyers')
                        <li class="{{ $elementActive == 'show-buyers' ? 'active' : '' }}">
                                <a href="#show_buyer">
                                <span class="sidebar-normal">{{ __(' Show Buyer ') }}</span>
                                </a>
                        </li>

                        @endif
                        @if ($elementActive == 'edit-buyers')
                        <li class="{{ $elementActive == 'edit-buyers' ? 'active' : '' }}">
                            <a href="#edit_buyer">
                            <span class="sidebar-normal">{{ __(' Edit Buyer ') }}</span>
                            </a>
                        </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--Suppliers--}}
            <li class="{{ $elementActive == 'list-suppliers' || $elementActive == 'show-suppliers' || $elementActive == 'create-suppliers' || $elementActive == 'edit-suppliers' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_suppliers">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('SUPPLIERS') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-suppliers' || $elementActive == 'show-suppliers' || $elementActive == 'create-suppliers' || $elementActive == 'edit-suppliers' ? 'show' : '' }}" id="menu_suppliers">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-suppliers' ? 'active' : '' }}">
                            <a href="{{ route('suppliers.index') }}">
                                <span class="sidebar-normal">{{ __(' List Suppliers ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-suppliers' ? 'active' : '' }}">
                            <a href="{{ route('suppliers.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-suppliers')
                            <li class="{{ $elementActive == 'show-suppliers' ? 'active' : '' }}">
                                <a href="#show_supplier">
                                    <span class="sidebar-normal">{{ __(' Show Supplier ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-suppliers')
                            <li class="{{ $elementActive == 'edit-suppliers' ? 'active' : '' }}">
                                <a href="#edit_supplier">
                                    <span class="sidebar-normal">{{ __(' Edit Supplier ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--Purchase and Sale--}}
            <li class="{{ $elementActive == 'list-mainpurchasesales' || $elementActive == 'show-mainpurchasesales' || $elementActive == 'create-mainpurchasesales' || $elementActive == 'edit-mainpurchasesales' || $elementActive == 'list-purchasesales' ? 'active' : '' }}" >
                <a data-toggle="collapse" aria-expanded="true" href="#menu_mainpurchasesales">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('PURCHASE AND SALE') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-mainpurchasesales' || $elementActive == 'list-purchasesales' || $elementActive == 'show-mainpurchasesales' || $elementActive == 'create-mainpurchasesales' || $elementActive == 'edit-mainpurchasesales' ? 'show' : '' }}" id="menu_mainpurchasesales">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-mainpurchasesales' ? 'active' : '' }}">
                            <a href="{{ route('mainpurchasesales.index') }}">
                                <span class="sidebar-normal">{{ __(' List Main Form ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'create-mainpurchasesales')
                            <li class="{{ $elementActive == 'create-mainpurchasesales' ? 'active' : '' }}">
                                <a href="E">
                                    <span class="sidebar-normal">{{ __(' Create New Main Form ') }}</span>
                                </a>
                            </li>
                        @endif
                        <li class="{{ $elementActive == 'list-purchasesales' ? 'active' : '' }}">
                            <a href="{{ route('purchasesales.index') }}">
                                <span class="sidebar-normal">{{ __(' List Items Detail ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-mainpurchasesales')
                            <li class="{{ $elementActive == 'show-mainpurchasesales' ? 'active' : '' }}">
                                <a href="#show-mainpurchasesales">
                                    <span class="sidebar-normal">{{ __(' Show Main Form ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-mainpurchasesales')
                            <li class="{{ $elementActive == 'edit-mainpurchasesales' ? 'active' : '' }}">
                                <a href="#edit-mainpurchasesales">
                                    <span class="sidebar-normal">{{ __(' Edit Main Form ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
{{--            End Purchase and Sale --}}
            {{--Investments--}}
            <li class="{{ $elementActive == 'list-investments' || $elementActive == 'show-investments' || $elementActive == 'create-investments' || $elementActive == 'edit-investments' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_investments">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('INVESTMENTS') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-investments' || $elementActive == 'show-investments' || $elementActive == 'create-investments' || $elementActive == 'edit-investments' ? 'show' : '' }}" id="menu_investments">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-investments' ? 'active' : '' }}">
                            <a href="{{ route('investments.index') }}">
                                <span class="sidebar-normal">{{ __(' List Investments ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-investments' ? 'active' : '' }}">
                            <a href="{{ route('investments.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-investments')
                            <li class="{{ $elementActive == 'show-investments' ? 'active' : '' }}">
                                <a href="#show_investment">
                                    <span class="sidebar-normal">{{ __(' Show Investment ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-investments')
                            <li class="{{ $elementActive == 'edit-investments' ? 'active' : '' }}">
                                <a href="#edit_investment">
                                    <span class="sidebar-normal">{{ __(' Edit Investment ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
{{--            End Investment--}}
            {{--Jobs--}}
            <li class="{{ $elementActive == 'list-jobs' || $elementActive == 'show-jobs' || $elementActive == 'create-jobs' || $elementActive == 'edit-jobs' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_jobs">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('JOBS') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-jobs' || $elementActive == 'show-jobs' || $elementActive == 'create-jobs' || $elementActive == 'edit-jobs' ? 'show' : '' }}" id="menu_jobs">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-jobs' ? 'active' : '' }}">
                            <a href="{{ route('jobs.index') }}">
                                <span class="sidebar-normal">{{ __(' List Jobs ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-jobs' ? 'active' : '' }}">
                            <a href="{{ route('jobs.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-jobs')
                            <li class="{{ $elementActive == 'show-jobs' ? 'active' : '' }}">
                                <a href="#show_job">
                                    <span class="sidebar-normal">{{ __(' Show Job ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-jobs')
                            <li class="{{ $elementActive == 'edit-jobs' ? 'active' : '' }}">
                                <a href="#edit_job">
                                    <span class="sidebar-normal">{{ __(' Edit Job ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--End Job--}}
            {{--Loans--}}
            <li class="{{ $elementActive == 'list-loans' || $elementActive == 'show-loans' || $elementActive == 'create-loans' || $elementActive == 'edit-loans' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_loans">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('LOANS') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-loans' || $elementActive == 'show-loans' || $elementActive == 'create-loans' || $elementActive == 'edit-loans' ? 'show' : '' }}" id="menu_loans">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-loans' ? 'active' : '' }}">
                            <a href="{{ route('loans.index') }}">
                                <span class="sidebar-normal">{{ __(' List Loans ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-loans' ? 'active' : '' }}">
                            <a href="{{ route('loans.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-loans')
                            <li class="{{ $elementActive == 'show-loans' ? 'active' : '' }}">
                                <a href="#show_loan">
                                    <span class="sidebar-normal">{{ __(' Show Loan ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-loans')
                            <li class="{{ $elementActive == 'edit-loans' ? 'active' : '' }}">
                                <a href="#edit_loan">
                                    <span class="sidebar-normal">{{ __(' Edit Loan ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--End Loan--}}
            {{--MainTrainings--}}
            <li class="{{ $elementActive == 'list-maintrainings' || $elementActive == 'show-maintrainings' || $elementActive == 'create-maintrainings' || $elementActive == 'edit-maintrainings' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_trainings">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('TRAINING') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-maintrainings' || $elementActive == 'show-maintrainings' || $elementActive == 'create-maintrainings' || $elementActive == 'add-participants' || $elementActive == 'edit-maintrainings' ? 'show' : '' }}" id="menu_trainings">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-maintrainings' ? 'active' : '' }}">
                            <a href="{{ route('maintrainings.index') }}">
                                <span class="sidebar-normal">{{ __(' List MainTrainings ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'create-maintrainings' ? 'active' : '' }}">
                            <a href="{{ route('maintrainings.create') }}">
                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>
                            </a>
                        </li>
                        @if ($elementActive == 'show-maintrainings')
                            <li class="{{ $elementActive == 'show-maintrainings' ? 'active' : '' }}">
                                <a href="#show_training">
                                    <span class="sidebar-normal">{{ __(' Show Training ') }}</span>
                                </a>
                            </li>

                        @endif

                        @if ($elementActive == 'add-participants')
                            <li class="active" style="color: #ef8157">
                                <a href="#add_participants">
                                    <span class="sidebar-normal">{{ __(' Add Participants ') }}</span>
                                </a>
                            </li>

                        @endif

                        @if ($elementActive == 'edit-maintrainings')
                            <li class="{{ $elementActive == 'edit-maintrainings' ? 'active' : '' }}">
                                <a href="#edit_training">
                                    <span class="sidebar-normal">{{ __(' Edit Training ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--End Training--}}

            {{--Bmps--}}
            <li class="{{ $elementActive == 'list-bmps' || $elementActive == 'show-bmps' || $elementActive == 'create-bmps' || $elementActive == 'edit-bmps' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_bmps">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('BMPs') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-bmps' || $elementActive == 'show-bmps' || $elementActive == 'create-bmps' || $elementActive == 'edit-bmps' ? 'show' : '' }}" id="menu_bmps">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-bmps' ? 'active' : '' }}">
                            <a href="{{ route('bmps.index') }}">
                                <span class="sidebar-normal">{{ __(' List Bmps ') }}</span>
                            </a>
                        </li>
{{--                        <li class="{{ $elementActive == 'create-bmps' ? 'active' : '' }}">--}}
{{--                            <a href="{{ route('bmps.create') }}">--}}
{{--                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        @if ($elementActive == 'show-bmps')
                            <li class="{{ $elementActive == 'show-bmps' ? 'active' : '' }}">
                                <a href="#show_bmp">
                                    <span class="sidebar-normal">{{ __(' Show Bmp ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-bmps')
                            <li class="{{ $elementActive == 'edit-bmps' ? 'active' : '' }}">
                                <a href="#edit_bmp">
                                    <span class="sidebar-normal">{{ __(' Edit Bmp ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--End Bmp--}}

            {{--Producers--}}
            <li class="{{ $elementActive == 'list-producers' || $elementActive == 'show-producers' || $elementActive == 'create-producers' || $elementActive == 'edit-producers' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#menu_producers">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('Producers') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'list-producers' || $elementActive == 'show-producers' || $elementActive == 'create-producers' || $elementActive == 'edit-producers' ? 'show' : '' }}" id="menu_producers">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'list-producers' ? 'active' : '' }}">
                            <a href="{{ route('producers.index') }}">
                                <span class="sidebar-normal">{{ __(' List Producers ') }}</span>
                            </a>
                        </li>
{{--                        <li class="{{ $elementActive == 'create-producers' ? 'active' : '' }}">--}}
{{--                            <a href="{{ route('producers.create') }}">--}}
{{--                                <span class="sidebar-normal">{{ __(' Create New ') }}</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        @if ($elementActive == 'show-producers')
                            <li class="{{ $elementActive == 'show-producers' ? 'active' : '' }}">
                                <a href="#show_bmp">
                                    <span class="sidebar-normal">{{ __(' Show Producer ') }}</span>
                                </a>
                            </li>

                        @endif
                        @if ($elementActive == 'edit-producers')
                            <li class="{{ $elementActive == 'edit-producers' ? 'active' : '' }}">
                                <a href="#edit_bmp">
                                    <span class="sidebar-normal">{{ __(' Edit Producer ') }}</span>
                                </a>
                            </li>

                        @endif

                    </ul>
                </div>
            </li>
            {{--End Producer--}}

        </ul>
    </div>
</div>
