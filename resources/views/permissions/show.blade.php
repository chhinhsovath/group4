@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'show-xx'
])
@section('content')
 <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    {{-- start --}}


                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-left">
                                    <h2> Show </h2> </div>
                                <div class="pull-right"> <a class="btn btn-primary" href="{{ route('permissions.index') }}"> Back</a> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group"> <strong>Name:</strong> {{ $permission->name }} </div>
                            </div>
                        </div>                        

                        
                    {{-- end --}}
                    </div>
                </div>
            </div>
        </div>
 </div>


@endsection
{{-- 
@push('scripts')
    <script>
        $(document).ready(function () {
        
		});
  </script>
@endpush --}}

