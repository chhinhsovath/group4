@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-xx'
])
@section('content')
 <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    {{-- start --}}


                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2>Permission Management</h2> </div>
                            <div class="pull-right"> @can('role-create') <a class="btn btn-success" href="{{ route('permissions.create') }}"> Create New Permission</a> @endcan </div>
                        </div>
                    </div> 
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div> 
                    @endif
                    <table class="table table-bordered datatable">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th width="280px">Action</th>
                        </tr> 
                        @foreach ($permissions as $key => $permission)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $permission->name }}</td>
                                <td>
                                    <form action="{{ route('permissions.destroy',$permission->id) }}" method="POST"> <a class="btn btn-info" href="{{ route('permissions.show',$permission->id) }}">Show</a> @can('permission-edit') <a class="btn btn-primary" href="{{ route('permissions.edit',$permission->id) }}">Edit</a> @endcan @csrf @method('DELETE') @can('permission-delete')
                                        <button type="submit" class="btn btn-danger">Delete</button> @endcan </form>
                                </td>
                        </tr> 
                        @endforeach 
                    </table> 
                        {!! $permissions->render() !!}
                        

                    {{-- end --}}
                    </div>
                </div>
            </div>
        </div>
 </div>


@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            // init datatable.
            var dataTable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                pageLength: 5,
                // scrollX: true,
                "order": [[ 0, "desc" ]],
                ajax: '{{ route('get-permissions') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
                ]
            });
        });
  </script>
@endpush

