@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'edit-permissions'
])

@section('content')

 <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    {{-- start --}}
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-left">
                                    <h2>Edit Permission</h2>
                                </div>
                                {{-- <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ route('admin.roles.index') }}"> Back</a>
                                </div> --}}
                            </div>
                        </div>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There are some issues.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        {{-- ID = {{ $role->id }} --}}

                        <form action="{{ route('permissions.update', $permission->id) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        <input type="text" name="name" value="{{ $permission->name }}" class="form-control" placeholder="Title">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <a class="btn btn-default" href="{{ route('roles.index') }}"> Back</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>

                        </form>


                    {{-- end --}}
                    </div>
                </div>
            </div>
        </div>
 </div>


@endsection