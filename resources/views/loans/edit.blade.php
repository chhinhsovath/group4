@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'edit-loans'
])
@section('content')
    <div class="content">
        <div class="row">
            {{-- containter --}}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--  end container--}}
        </div>
        <form action="{{ route('loans.update',$Loans->id) }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" class="form-control" name="userid" value="{{ auth()->user()->id }}">
            <input type="hidden" class="form-control" name="grantid" value="{{ auth()->user()->grantid }}">
            <input type="hidden" class="form-control" name="id" value="{{ $Loans->id }}">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="pull-left">
                                <h4 class="card-title"> Edit Loan </h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label class="form-control-label" for="name">Name</label>
                                    <select class="form-control" name="buyersid" style=" height:45px;" required>
                                        <option value="{{ $Loans->id}}" selected>{{$Loans->name}}</option>
                                        @foreach($Buyers as $id => $name)
                                            <option value="{{$id}}"> {{$name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label">Sub Sector </label>
                                    <div class="form-group">
                                        <select class="form-control" name="subsector" style=" height:45px;"  required>
                                            <option value="{{$Loans->subsector}}" selected>{{$Loans->subsector}}</option>
                                            <option value="Cashew">Cashew</option>
                                            <option value="Longan">Longan</option>
                                            <option value="Mango">Mango</option>
                                            <option value="Mixed Fruit">Mixed Fruit</option>
                                            <option value="Pepper Corn">Pepper Corn</option>
                                            <option value="Vegetable">Vegetable</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label">Province</label>
                                    <div class="form-group">
                                        <select id="procode" name="procode" class="form-control" style=" height:45px;"  required>
                                            <option value="{{$Loans->procode}}" selected>{{$Loans->provinces->proname}}</option>
                                            @foreach($Provinces as $procode => $proname)
                                                <option value="{{$procode}}"> {{$proname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label">Lead</label>
                                    <div class="form-group">
                                        <select id="staffprofilesid" name="staffprofilesid" class="form-control" style=" height:45px;"  required>
                                            <option value="{{$Loans->staffprofilesid}}" selected>{{$Loans->leads->name}}</option>
                                            @foreach($Staffprofiles as $id => $name)
                                                <option value="{{$id}}"> {{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="form-label">Note </label>
                                    <input type="text"  class="form-control" value="{{$Loans->note}}" name="note" placeholder="Note">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="pull-left">
                                    <h4 class="card-title"> Formal Loans </h4>
                                </div>
                            </div>
                            <div class="card-body" style="min-height: 310px;">

                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Loan Value in USD</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Loans->loanvalue}}" name="loanvalue" placeholder="Loan Value in USD"  required>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Loan Period</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Loans->period}}" name="period" placeholder="In Months or Years"  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Annual Rate</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Loans->annualrate}}" name="annualrate" placeholder="Annual Rate">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Date Loan Disbursed</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" value="{{$Loans->datedisbursed}}" name="datedisbursed" placeholder="Date Loan Disburstment" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Loan from Bank/MFI Name</label>
                                        <div class="form-group">
                                            {{--<input type="text" class="form-control" name="bankormfi" placeholder="Loan from Bank/MFI Name">--}}
                                            <select class="form-control" name="bankormfi" style=" height:45px;" required>
                                                <option value="{{$Loans->bankormfi}}" selected>{{$Loans->bankormfi}}</option>
                                                @foreach($Financialcenters as $id => $name)
                                                    <option value="{{$name}}"> {{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Type of Investment</label>
                                        <div class="form-group">
                                            <select class="form-control" name="typeofinvestment" style=" height:45px;"  required>
                                                <option value="{{$Loans->typeofinvestment}}" selected>{{$Loans->typeofinvestment}}</option>
                                                <option value="Capital investment">Capital investment</option>
                                                <option value="Working Capital">Working Capital</option>
                                                <option value="Operation Expense">Operation Expense</option>
                                                <option value="Business Loan">Business Loan</option>
                                                <option value="In Kind">In Kind</option>
                                                <option value="Others">Others * Please descript in note</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Purpose of Loan</label>
                                        <div class="form-group">
                                            <input type="text"  class="form-control" value="{{$Loans->purposeofloan}}" name="purposeofloan" placeholder="Purpose of Loan">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="pull-left">
                                    <h4 class="card-title"> In-Kind </h4>
                                </div>
                            </div>
                            <div class="card-body" style="min-height: 310px;">
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">In-Kind Note</label>
                                        <div class="form-group">
                                            <input type="text"  class="form-control" value="{{$Loans->inkind}}" name="inkind" placeholder="In-Kind note">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Number of Female</label>
                                        <div class="form-group">
                                            <input type="number" class="form-control" value="{{$Loans->female}}" name="female" placeholder="Number of Female - Optional">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label">Number of Youth</label>
                                        <div class="form-group">
                                            <input type="number" step="any" class="form-control" value="{{$Loans->youth}}" name="youth" placeholder="Number of Youth - Optional" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label class="form-label">Total</label>
                                        <div class="form-group">
                                            <input type="number" class="form-control" value="{{$Loans->total}}" name="total">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <label class="form-label"></label>
                                        <div class="form-group">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <div class="pull-left">
                                        <a href="{{ url()->previous() }}" class="btn btn-defaultk">Back</a>
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary ms-auto">UPDATE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @endsection
        @push('scripts')
            <script>
                // $(document).ready(function() {
                //     $('.select2multipls').select2({
                //         placeholder: "Select multiple",
                //         allowClear: true,
                //         tags: true,
                //         tokenSeparators: [',',]
                //     });
                // });

                $('#procode').change(function() {
                    var procode = $(this).val();
                    $("#discode").empty();
                    $("#comcode").empty();
                    $("#vilcode").empty();
                    if (procode) {
                        $.ajax({
                            type: "GET",
                            url: "{{url('getDistrict')}}/" + procode,
                            success: function(res) {
                                if (res) {
                                    $("#discode").empty();
                                    $("#discode").append('<option>Select District</option>');
                                    $.each(res, function(key, value) {
                                        $("#discode").append('<option value="' + key + '">' + value + '</option>');
                                    });

                                } else {
                                    $("#discode").empty();
                                }
                            }
                        });
                    } else {
                        $("#discode").empty();
                        $("#comcode").empty();
                        $("#vilcode").empty();
                    }
                });
                {{--$('#discode').on('change', function() {--}}
                {{--    var discode = $(this).val();--}}
                {{--    $("#comcode").empty();--}}
                {{--    $("#vilcode").empty();--}}
                {{--    if (discode) {--}}
                {{--        $.ajax({--}}
                {{--            type: "GET",--}}
                {{--            url: "{{url('getCommune')}}?discode=" + discode,--}}
                {{--            success: function(res) {--}}
                {{--                if (res) {--}}
                {{--                    $("#comcode").empty();--}}
                {{--                    $("#comcode").append('<option>Select Commune</option>');--}}
                {{--                    $.each(res, function(key, value) {--}}
                {{--                        $("#comcode").append('<option value="' + key + '">' + value + '</option>');--}}
                {{--                    });--}}

                {{--                } else {--}}
                {{--                    $("#comcode").empty();--}}
                {{--                }--}}
                {{--            }--}}
                {{--        });--}}
                {{--    } else {--}}
                {{--        $("#comcode").empty();--}}
                {{--        $("#vilcode").empty();--}}
                {{--    }--}}

                {{--});--}}
                {{--$('#comcode').on('change', function() {--}}
                {{--    var comcode = $(this).val();--}}
                {{--    $("#vilcode").empty();--}}
                {{--    if (comcode) {--}}
                {{--        $.ajax({--}}
                {{--            type: "GET",--}}
                {{--            url: "{{url('getVillage')}}?comcode=" + comcode,--}}
                {{--            success: function(res) {--}}
                {{--                if (res) {--}}
                {{--                    $("#vilcode").empty();--}}
                {{--                    $("#vilcode").append('<option>Select Village</option>');--}}
                {{--                    $.each(res, function(key, value) {--}}
                {{--                        $("#vilcode").append('<option value="' + key + '">' + value + '</option>');--}}
                {{--                    });--}}

                {{--                } else {--}}
                {{--                    $("#vilcode").empty();--}}
                {{--                }--}}
                {{--            }--}}
                {{--        });--}}
                {{--    } else {--}}
                {{--        // $("#vilcode").empty();--}}
                {{--    }--}}

                {{--});--}}
            </script>
    @endpush
