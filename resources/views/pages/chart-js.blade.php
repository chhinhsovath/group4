@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'tables'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Chart Js</h4>
                    </div>
                    <div class="card-body">

                        <canvas id="pie-chart"></canvas>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Simple Table</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>
                                    Name
                                </th>
                                <th>
                                    Country
                                </th>
                                <th>
                                    City
                                </th>
                                <th class="text-right">
                                    Salary
                                </th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Dakota Rice
                                    </td>
                                    <td>
                                        Niger
                                    </td>
                                    <td>
                                        Oud-Turnhout
                                    </td>
                                    <td class="text-right">
                                        $36,738
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Minerva Hooper
                                    </td>
                                    <td>
                                        Curaçao
                                    </td>
                                    <td>
                                        Sinaai-Waas
                                    </td>
                                    <td class="text-right">
                                        $23,789
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Sage Rodriguez
                                    </td>
                                    <td>
                                        Netherlands
                                    </td>
                                    <td>
                                        Baileux
                                    </td>
                                    <td class="text-right">
                                        $56,142
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Philip Chaney
                                    </td>
                                    <td>
                                        Korea, South
                                    </td>
                                    <td>
                                        Overland Park
                                    </td>
                                    <td class="text-right">
                                        $38,735
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Doris Greene
                                    </td>
                                    <td>
                                        Malawi
                                    </td>
                                    <td>
                                        Feldkirchen in Kärnten
                                    </td>
                                    <td class="text-right">
                                        $63,542
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mason Porter
                                    </td>
                                    <td>
                                        Chile
                                    </td>
                                    <td>
                                        Gloucester
                                    </td>
                                    <td class="text-right">
                                        $78,615
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Jon Porter
                                    </td>
                                    <td>
                                        Portugal
                                    </td>
                                    <td>
                                        Gloucester
                                    </td>
                                    <td class="text-right">
                                        $98,615
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>--}}
{{--    <script>--}}
{{--        $(function(){--}}
{{--            //get the pie chart canvas--}}
{{--            var cData = JSON.parse(`<?php echo $chart_data; ?>`);--}}
{{--            var ctx = $("#pie-chart");--}}
{{--            //pie chart data--}}
{{--            var data = {--}}
{{--                labels: cData.label,--}}
{{--                datasets: [--}}
{{--                    {--}}
{{--                        label: "Users Count",--}}
{{--                        data: cData.data,--}}
{{--                        backgroundColor: [--}}
{{--                            "#DEB887",--}}
{{--                            "#A9A9A9",--}}
{{--                            "#DC143C",--}}
{{--                            "#F4A460",--}}
{{--                            "#2E8B57",--}}
{{--                            "#1D7A46",--}}
{{--                            "#CDA776",--}}
{{--                        ],--}}
{{--                        borderColor: [--}}
{{--                            "#CDA776",--}}
{{--                            "#989898",--}}
{{--                            "#CB252B",--}}
{{--                            "#E39371",--}}
{{--                            "#1D7A46",--}}
{{--                            "#F4A460",--}}
{{--                            "#CDA776",--}}
{{--                        ],--}}
{{--                        borderWidth: [1, 1, 1, 1, 1,1,1]--}}
{{--                    }--}}
{{--                ]--}}
{{--            };--}}
{{--            //options--}}
{{--            var options = {--}}
{{--                responsive: true,--}}
{{--                title: {--}}
{{--                    display: true,--}}
{{--                    position: "top",--}}
{{--                    text: "Last Week Registered Users -  Day Wise Count",--}}
{{--                    fontSize: 18,--}}
{{--                    fontColor: "#111"--}}
{{--                },--}}
{{--                legend: {--}}
{{--                    display: true,--}}
{{--                    position: "bottom",--}}
{{--                    labels: {--}}
{{--                        fontColor: "#333",--}}
{{--                        fontSize: 16--}}
{{--                    }--}}
{{--                }--}}
{{--            };--}}
{{--            //create Pie Chart class object--}}
{{--            var chart1 = new Chart(ctx, {--}}
{{--                type: "pie",--}}
{{--                data: data,--}}
{{--                options: options--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
    <script>
        $(document).ready(function() {

        });
    </script>
@endpush
