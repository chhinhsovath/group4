@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-single-02 text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Buyers</p>
                                    <p class="card-title">{{$dashboard->buyers_all}}</p>
{{--                                    <p class="card-title">{{$counts['buyers']}}</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-refresh"></i> Update Now--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-single-02 text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Suppliers</p>
                                    <p class="card-title">{{$dashboard->suppliers_active}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-calendar-o"></i> Last day--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-single-02 text-danger"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">BMPs</p>
                                    <p class="card-title">{{$dashboard->bmps_all}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-clock-o"></i> In the last hour--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-single-02 text-primary"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Producers</p>
                                    <p class="card-title">{{$dashboard->producers_all}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-refresh"></i> Update now--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
{{--        Second Row--}}
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-money-coins text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Sale at Farm</p>
                                    <p class="card-title">{{number_format($dashboard->sale_at_farm,2)}}</p>
                                    {{--                                    <p class="card-title">{{$counts['buyers']}}</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-refresh"></i> Update Now--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-money-coins text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Sale at Firm</p>
                                    <p class="card-title">{{number_format($dashboard->sale_at_firm,2)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-calendar-o"></i> Last day--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-money-coins text-danger"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Investment</p>
                                    <p class="card-title">{{number_format($dashboard->investments_all,2)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-clock-o"></i> In the last hour--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-single-02 text-primary"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Jobs</p>
                                    <p class="card-title">{{$dashboard->jobs_all}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-footer ">--}}
{{--                        <hr>--}}
{{--                        <div class="stats">--}}
{{--                            <i class="fa fa-refresh"></i> Update now--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> People Engaged with Project </h4>
                    </div>
                    <div class="card-body">
                        {!! $bspb_chart->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Money Generated with Project </h4>
                    </div>
                    <div class="card-body">
                        {!! $psil_chart->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Jobs Generated with Project </h4>
                    </div>
                    <div class="card-body">
                        {!! $jobs_chart->container() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Value by Subsector </h4>
                    </div>
                    <div class="card-body">
                        {!! $pss_chart->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Top 10 Crops High Value</h4>
                    </div>
                    <div class="card-body">
                        {!! $topfivecrop_chart->container() !!}
                    </div>
                    </div>
                </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Investment by Types </h4>
                    </div>
                    <div class="card-body">
                        {!! $investment_chart->container() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Purchase Sale by Project Year </h4>
                    </div>
                    <div class="card-body">
                        {!! $pspy_chart->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Purchase Sale by Provinces </h4>
                    </div>
                    <div class="card-body">
                        {!! $psp_chart->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Purchase Sale Breakdown by Subsector and Project Year </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Sub-Sector
                                </th>
                                <th>
                                    Purchase
                                </th>

                                <th>
                                    Sale
                                </th>

                                <th  class="text-center">
                                    Project Year
                                </th>
                                </thead>
                                <tbody>
                                @foreach ($purchase_sale_subsector_py as $purchase_sale)
                                    <tr>
                                        <td> {{ $loop->index + 1 }} </td>
                                        <td>
                                            {{ $purchase_sale->subsector }}
                                        </td>
                                        <td>
                                            {{ number_format($purchase_sale->purchase,2) }}
                                        </td>
                                        <td>
                                            {{ number_format($purchase_sale->sale,2) }}
                                        </td>
                                        <td  class="text-center">
                                            {{ $purchase_sale->projectyear }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                                <p class="m-0 text-muted">Showing   {{$purchase_sale_subsector_py->count()}} entries</p>
                                <ul class="pagination m-0 ms-auto">
                                    {{$purchase_sale_subsector_py->links()}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Purchase Sale Breakdown by Provinces and Project Year </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Province
                                </th>
                                <th>
                                    Purchase
                                </th>

                                <th>
                                    Sale
                                </th>

                                <th  class="text-center">
                                    Project Year
                                </th>
                                </thead>
                                <tbody>
                                @foreach ($purchase_sale_province_py as $purchase_sale)
                                    <tr>
                                        <td> {{ $loop->index + 1 }} </td>
                                        <td>
                                            {{ $purchase_sale->province }}
                                        </td>
                                        <td>
                                            {{ number_format($purchase_sale->purchase,2) }}
                                        </td>
                                        <td>
                                            {{ number_format($purchase_sale->sale,2) }}
                                        </td>
                                        <td class="text-center">
                                            {{ $purchase_sale->projectyear }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                                <p class="m-0 text-muted">Showing   {{$purchase_sale_province_py->count()}} entries</p>
                                <ul class="pagination m-0 ms-auto">
                                    {{$purchase_sale_province_py->links()}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection

@push('scripts')
    {!! $psp_chart->script() !!}
    {!! $pspy_chart->script() !!}
    {!! $investment_chart->script() !!}
    {!! $pss_chart->script() !!}
    {!! $topfivecrop_chart->script() !!}
    {!! $bspb_chart->script() !!}
    {!! $psil_chart->script() !!}
    {!! $jobs_chart->script() !!}

    <script>
        ctx = document.getElementById('jobs_chart').getContext("2d");

        myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [1, 2],
                datasets: [{
                    label: "Emails",
                    pointRadius: 0,
                    pointHoverRadius: 0,
                    backgroundColor: ['#66615b', '#f4f3ef'],
                    borderWidth: 0,
                    data: [11, 89]
                }]
            },
            options: {
                elements: {
                    center: {
                        text: '11%',
                        color: '#66615c', // Default is #000000
                        fontStyle: 'Arial', // Default is Arial
                        sidePadding: 60 // Defualt is 20 (as a percentage)
                    }
                },
                cutoutPercentage: 90,
                legend: {

                    display: false
                },

                tooltips: {
                    enabled: false
                },

                scales: {
                    yAxes: [{

                        ticks: {
                            display: false
                        },
                        gridLines: {
                            drawBorder: false,
                            zeroLineColor: "transparent",
                            color: 'rgba(255,255,255,0.05)'
                        }

                    }],

                    xAxes: [{
                        barPercentage: 1.6,
                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(255,255,255,0.1)',
                            zeroLineColor: "transparent"
                        },
                        ticks: {
                            display: false,
                        }
                    }]
                },
            }
        });
    </script>
@endpush
{{--<canvas id="my_Chart"></canvas>--}}
{{--<script>--}}
{{--    // data define for bar chart--}}
{{--    var myData = {--}}
{{--        labels: ["Javascript", "Java", "Python", "PHP", "C++", "TypeScript", "Linux Shell","C","Ruby on Rails"],--}}
{{--        datasets: [{--}}
{{--            label: "Hey, baby!",--}}
{{--            fill: false,--}}
{{--            backgroundColor: ['#ff0000', '#ff4000', '#ff8000', '#ffbf00', '#ffbf00', '#ffff00', '#bfff00', '#80ff00', '#40ff00', '#00ff00'],--}}
{{--            borderColor: 'black',--}}
{{--            data: [85, 60,70, 50, 18, 20, 45, 30, 20],--}}
{{--        }]--}}
{{--    };--}}
{{--    // Options define for display value on top of bars--}}
{{--    var myoption = {--}}
{{--        tooltips: {--}}
{{--            enabled: true--}}
{{--        },--}}
{{--        hover: {--}}
{{--            animationDuration: 1--}}
{{--        },--}}
{{--        animation: {--}}
{{--            duration: 1,--}}
{{--            onComplete: function () {--}}
{{--                var chartInstance = this.chart,--}}
{{--                    ctx = chartInstance.ctx;--}}
{{--                ctx.textAlign = 'center';--}}
{{--                ctx.fillStyle = "rgba(0, 0, 0, 1)";--}}
{{--                ctx.textBaseline = 'bottom';--}}
{{--                // Loop through each data in the datasets--}}
{{--                this.data.datasets.forEach(function (dataset, i) {--}}
{{--                    var meta = chartInstance.controller.getDatasetMeta(i);--}}
{{--                    meta.data.forEach(function (bar, index) {--}}
{{--                        var data = dataset.data[index];--}}
{{--                        ctx.fillText(data, bar._model.x, bar._model.y - 5);--}}
{{--                    });--}}
{{--                });--}}
{{--            }--}}
{{--        }--}}
{{--    };--}}
{{--    // Code to draw Chart--}}
{{--    var ctx = document.getElementById('my_Chart').getContext('2d');--}}
{{--    var myChart = new Chart(ctx, {--}}
{{--        type: 'bar',        // Define chart type--}}
{{--        data: myData,    	// Chart data--}}
{{--        options: myoption 	// Chart Options [This is optional paramenter use to add some extra things in the chart].--}}
{{--    });--}}
{{--</script>--}}
