@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'import-contacts'
])
@section('content')
 <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="card-title"> Import Contacts From Excel</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('import-tmp-contacts.import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="file" name="file" class="form-control" required>
                                </div>
                                <div class="col-md-2">
                                    <div class="pull-right">
                                        <button class="btn btn-success" style="margin-top: 3px">Preview Contact</button>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="pull-right">
                                        <a href="../../excel_templates/import_contacts.xlsx" style="margin-top: 3px" class="btn btn-info">Download Template</a>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>



            </div>

        </div>
     @if($preview_data->isNotEmpty())
     <div class="row">
         <div class="col-md-12">
             <div class="card">
                 <div class="card-header">
                     <div class="row">
                         <div class="col-md-8">
                             <h4 class="card-title"> Preview before Import</h4>
                         </div>
                         <div class="col-md-2">
                             <div class="pull-right">
                                 <form method="POST" action="{{ route('import-tmp-contacts.clearall') }}">
                                     @csrf
                                     <button type="submit" class="show_confirm btn btn-danger" data-toggle="tooltip" title='Clear All'>Clear All</button>
                                 </form>
                             </div>
                         </div>
                         <div class="col-md-2">
                             <div class="pull-right">
                                 <form method="POST" action="{{ route('import-tmp-contacts.finalimport') }}">
                                     @csrf
                                     <button type="submit" class="confirm_import btn btn-primary" data-toggle="tooltip" title='Import Contact'>Import Contact</button>
                                 </form>

{{--                                <a href="{{ route('import-tmp-contacts.finalimport') }}" style="margin-top: 3px" class="btn btn-success">Import Contact</a>--}}
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="card-body">
                     <table class="table">
                         <thead class=" text-primary">
                         <th>
                             ID
                         </th>
                         <th>
                             Name.English
                         </th>
                         <th>
                             Name.Khmer
                         </th>

                         <th>
                             Sex
                         </th>

                         <th>
                             Age
                         </th>

                         <th>
                             Phone
                         </th>

                         <th>
                             Province
                         </th>

                         <th>
                             District
                         </th>

                         <th>
                             Commune
                         </th>
                         <th>
                             Commune
                         </th>
                         <th>
                             Commune
                         </th>
                         <th>
                             Note
                         </th>
                         <th>
                             Table
                         </th>
                         <th class="text-right">
                             Action
                         </th>
                         </thead>
                         <tbody>

                             @foreach( $preview_data as $data)
                                 <tr>
                                     <td> {{ $data->id }} </td>
                                     <td> {{ $data->name_en }} </td>
                                     <td> {{ $data->name_kh }} </td>
                                     <td> {{ $data->sex }} </td>
                                     <td> {{ $data->age }} </td>
                                     <td> {{ $data->phone }} </td>
                                     <td> {{ $data->email }} </td>
                                     <td> {{ $data->proname }} </td>
                                     <td> {{ $data->disname }} </td>
                                     <td> {{ $data->comname }} </td>
                                     <td> {{ $data->vilname }} </td>
                                     <td> {{ $data->note }} </td>
                                     <td> {{ $data->tables }} </td>
                                     <td class="text-right">
                                         <form method="POST" action="{{ route('import-tmp-contacts.destroy', $data->id) }}">
                                             @csrf
                                             @method('DELETE')
                                             <input type="hidden" value="{{$data->id}}" name="id">
                                             <button type="submit" class="show_confirm btn btn-sm btn-danger" data-toggle="tooltip" title='Delete'><i class="nc-icon nc-simple-remove"></i></button>
                                         </form>
                                     </td>
                                 </tr>
                             @endforeach

                         </tbody>
                     </table>
                     <div class="card-footer d-flex align-items-center">
                         <p class="m-0 text-muted">Showing   {{$preview_data->count()}} entries</p>
                         <ul class="pagination m-0 ms-auto">
                             {{$preview_data->links()}}
                         </ul>
                     </div>

                 </div>
             </div>
         </div>
     </div>
     @endif
 </div>


@endsection
@push('scripts')
    <script>

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var id = $(this).data("id");
            event.preventDefault();
            swal({
                title: `Are you sure you want clear all contents in preview?`,
                text: "All preview content will be deleted forever.",
                icon: "warning",
                // buttons: true,
                // dangerMode: true,
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                    // $(".preloader").fadeIn();
                });
        });
        $('.confirm_import').click(function(event) {
            var form =  $(this).closest("form");
            var id = $(this).data("id");
            event.preventDefault();
            swal({
                title: `Are you sure you want to import contact?`,
                text: "All information in preview page will be imported and cleared from preview.",
                icon: "success",
                // buttons: true,
                // dangerMode: true,
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            })
                .then((willImport) => {
                    if (willImport) {
                        form.submit();
                    }
                    // $(".preloader").fadeIn();
                });
        });

        $(document).ready(function () {

		});
    </script>
@endpush

