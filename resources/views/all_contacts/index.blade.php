@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'list-all-in-one-contact'
])
@section('content')
<div class="content">

{{--    <div class="row">--}}
{{--        <div class="col-lg-3 col-md-6 col-sm-6">--}}
{{--            <div class="card card-stats">--}}
{{--                <div class="card-body ">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-5 col-md-4">--}}
{{--                            <div class="icon-big text-center icon-warning">--}}
{{--                                <i class="nc-icon nc-globe text-warning"></i>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-7 col-md-8">--}}
{{--                            <div class="numbers">--}}
{{--                                <p class="card-category">Buyers</p>--}}
{{--                                <p class="card-title">{{$counts['buyers']}}--}}
{{--                                <p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-footer ">--}}
{{--                    <hr>--}}
{{--                    <div class="stats">--}}
{{--                        <i class="fa fa-refresh"></i> Update Now--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-md-6 col-sm-6">--}}
{{--            <div class="card card-stats">--}}
{{--                <div class="card-body ">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-5 col-md-4">--}}
{{--                            <div class="icon-big text-center icon-warning">--}}
{{--                                <i class="nc-icon nc-money-coins text-success"></i>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-7 col-md-8">--}}
{{--                            <div class="numbers">--}}
{{--                                <p class="card-category">Revenue</p>--}}
{{--                                <p class="card-title">$ 1,345--}}
{{--                                <p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-footer ">--}}
{{--                    <hr>--}}
{{--                    <div class="stats">--}}
{{--                        <i class="fa fa-calendar-o"></i> Last day--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-md-6 col-sm-6">--}}
{{--            <div class="card card-stats">--}}
{{--                <div class="card-body ">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-5 col-md-4">--}}
{{--                            <div class="icon-big text-center icon-warning">--}}
{{--                                <i class="nc-icon nc-vector text-danger"></i>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-7 col-md-8">--}}
{{--                            <div class="numbers">--}}
{{--                                <p class="card-category">Errors</p>--}}
{{--                                <p class="card-title">23--}}
{{--                                <p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-footer ">--}}
{{--                    <hr>--}}
{{--                    <div class="stats">--}}
{{--                        <i class="fa fa-clock-o"></i> In the last hour--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-md-6 col-sm-6">--}}
{{--            <div class="card card-stats">--}}
{{--                <div class="card-body ">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-5 col-md-4">--}}
{{--                            <div class="icon-big text-center icon-warning">--}}
{{--                                <i class="nc-icon nc-favourite-28 text-primary"></i>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-7 col-md-8">--}}
{{--                            <div class="numbers">--}}
{{--                                <p class="card-category">Followers</p>--}}
{{--                                <p class="card-title">+45K--}}
{{--                                <p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-footer ">--}}
{{--                    <hr>--}}
{{--                    <div class="stats">--}}
{{--                        <i class="fa fa-refresh"></i> Update now--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="row">

        <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> List of All-in-One Contact </h4>
                    </div>
                    <div class="card-body">

{{--                        <div id="advancedsearch" style="display:none; border-radius: 5px; background-color:#F4F6FA; padding-bottom:1px; padding-top: 8px;">--}}
                            <form action="{{ route('allinonecontacts.index') }}" method="get">
                                <div class="row" style=" margin:10px;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Name</label>
                                            <input type="text" class="form-control" name="filter[name_en]" placeholder="Name English">
{{--                                            <select class="form-control" id="name" name="filter[name_en]" style=" height:45px;">--}}
{{--                                                <option value="" selected>Name</option>--}}
{{--                                                @foreach($all_contacts as $all_contact)--}}
{{--                                                    <option value="{{$all_contact->name_en}}">{{$all_contact->name_en}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">Phone</label>
                                            <input type="text" class="form-control" id="phone" name="filter[phone]" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="form-control-label" for="sex">Sex</label>
                                            <select class="form-control" id="tables" name="filter[sex]" style=" height:45px;">
                                                <option value="" selected>Sex</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">Age</label>
                                            <input type="text" class="form-control" id="age" name="filter[age]" placeholder="Age">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">Note</label>
                                            <input type="text" class="form-control" id="note" name="filter[note]" placeholder="Note">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="vilname">Contact Type</label>
                                            <select class="form-control" id="tables" name="filter[tables]" style=" height:45px;">
                                                <option value="" selected>Contact Type</option>
                                                <option value="bmps">BMP</option>
                                                <option value="buyers">Buyers</option>
                                                <option value="grantees">Grantees</option>
                                                <option value="producers">Producers</option>
                                                <option value="suppliers">Suppliers</option>
                                                <option value="general">General</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button type="submit"  id="search" class="form-control btn btn-warning" style="margin-top: 24px; height:45px;"> Search </button>
                                        </div>
                                    </div>

                                </div>
                                <div class="row" style=" margin:10px;">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="proname">Province</label>
                                            <select class="form-control select2multipls" id="proname" name="filter[proname]" style=" height:45px;">
                                                <option value="" selected>Province</option>
                                                @foreach($provinces as $procode => $proname)
                                                    <option value="{{$proname}}"> {{$proname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="disname">District</label>
                                            <select class="form-control" id="disname" name="filter[disname]" style=" height:45px;">
                                                <option value="" selected>District</option>
                                                @foreach($districts as $discode => $disname)
                                                    <option value="{{$disname}}"> {{$disname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comname">Commune</label>
                                            <select class="form-control" id="comname" name="filter[comname]" style=" height:45px;">
                                                <option value="" selected>Commune</option>
                                                @foreach($communes as $comcode => $comname)
                                                    <option value="{{$comname}}"> {{$comname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="vilname">Village</label>
                                            <select class="form-control" id="vilname" name="filter[vilname]" style=" height:45px;">
                                                <option value="" selected>Village</option>
                                                @foreach($villages as $vilcode => $vilname)
                                                    <option value="{{$vilname}}"> {{$vilname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="{{ route('allinonecontacts-export') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Export</a>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="{{ route('import-tmp-contacts.uploadfile') }}" class="form-control btn btn-warning" style="margin-top: 24px; height:43px;">Import</a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <a href="{{ route('allinonecontacts.create') }}" class="form-control btn btn-primary" style="margin-top: 24px; height:43px;">Add New Contact</a>
                                        </div>
                                    </div>

                                </div>
                            </form>

{{--                        </div>--}}


                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name English
                                    </th>
                                    <th>
                                        Name Khmer
                                    </th>

                                    <th>
                                        Sex
                                    </th>

                                    <th>
                                        Age
                                    </th>

                                    <th>
                                        Phone
                                    </th>

                                    <th>
                                        Province
                                    </th>

                                    <th>
                                        District
                                    </th>

                                    <th>
                                        Commune
                                    </th>
                                    <th>
                                        Village
                                    </th>

                                    <th>
                                        Latitude
                                    </th>
                                    <th>
                                        Longitute
                                    </th>
                                    <th>
                                        Tables
                                    </th>
                                    <th>
                                        UUID
                                    </th>


                                    <th class="text-right">

                                        <form method="POST" action="{{ route('allinonecontacts.restore') }}">
                                            @csrf
                                            <button type="submit" class="show_confirm btn btn-sm btn-danger" data-toggle="tooltip" title='Restore'>Restore</button>
                                        </form>

                                    </th>
                                </thead>
                                <tbody>
                                     @foreach ($all_contacts as $contact)
                                    <tr>
                                        <td> {{ $contact->id }} </td>
                                        <td>
                                            {{ $contact->name_en }}
                                        </td>
                                        <td>
                                            {{ $contact->name_kh }}
                                        </td>
                                        <td>
                                            {{ $contact->sex }}
                                        </td>
                                        <td>
                                            {{ $contact->age }}
                                        </td>
                                        <td>
                                            {{ $contact->phone }}
                                        </td>
                                        <td>
                                            {{ $contact->proname }}
                                        </td>
                                        <td>
                                            {{ $contact->disname }}
                                        </td>
                                        <td>
                                            {{ $contact->comname }}
                                        </td>
                                        <td>
                                            {{ $contact->vilname }}
                                        </td>
                                        <td>
                                            {{ $contact->latitude }}
                                        </td>
                                        <td>
                                            {{ $contact->longitude }}
                                        </td>
                                        <td>
                                            {{ $contact->tables }}
                                        </td>
                                        <td>
                                            {{ $contact->uuid }}
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="nc-icon nc-bullet-list-67"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
{{--                                                    <a class="dropdown-item" data-toggle="tooltip" title='Edit' href="{{ route("allinonecontacts.edit",$contact->id) }}">Edit</a>--}}
                                                    @can('admin')
                                                        <form method="POST" action="{{ route('allinonecontacts.destroy', $contact->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            {{--                                                            <input name="_method" type="hidden" value="DELETE">--}}
                                                            <input type="hidden" value="{{$contact->id}}" name="id">
                                                            <button type="submit" class="dropdown-item show_confirm" data-toggle="tooltip" title='Delete'>Delete</button>
                                                        </form>
                                                    @endcan
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer d-flex align-items-center">
                            <p class="m-0 text-muted">Showing   {{$all_contacts->count()}} entries</p>
                            <ul class="pagination m-0 ms-auto">
                                {{$all_contacts->links()}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        // $(document).ready(function() {
        //     $('.select2multipls').select2({
        //         placeholder: "Select multiple",
        //         allowClear: true,
        //         tags: true,
        //         tokenSeparators: [',',]
        //     });
        // });

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var id = $(this).data("id");
            event.preventDefault();
            swal({
                title: `Are you sure you want to delete this record?`,
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                // buttons: true,
                // dangerMode: true,
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                    // $(".preloader").fadeIn();
                });
        });

        function advancedsearch() {
        var x = document.getElementById("advancedsearch");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
@endpush
