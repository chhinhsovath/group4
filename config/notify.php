<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notify Theme
    |--------------------------------------------------------------------------
    |
    | You can change the theme of notifications by specifying the desired theme.
    | By default the theme light is activated, but you can change it by
    | specifying the dark mode. To change theme, update the global variable to `dark`
    |
    */

    'theme' => env('NOTIFY_THEME', 'light'),

    /*
    |--------------------------------------------------------------------------
    | Demo URL
    |--------------------------------------------------------------------------
    |
    | if true you can access to the demo documentation of the notify package
    | here: http://localhost:8000/notify/demo, by default is true
    |
    */

//    'demo' => true,

    /*
    |--------------------------------------------------------------------------
    | Notification timeout
    |--------------------------------------------------------------------------
    |
    | Defines the number of seconds during which the notification will be visible.
    |
    */

    'timeout'   => 5000,
//    'animate' => [
//        'in_class' => 'bounceInRight', // The class to use to animate the notice in.
//        'out_class' => 'bounceOutRight', // The class to use to animate the notice out.
//        'timeout'   => 5000 // Number of seconds before the notice disappears
//    ],

    /*
    |--------------------------------------------------------------------------
    | Preset Messages
    |--------------------------------------------------------------------------
    |
    | Define any preset messages here that can be reused.
    | Available model: connect, drake, emotify, smiley, toast
    |
    */

    'preset-messages' => [
        // An example preset 'user updated' Connectify notification.
        'user-created' => [
            'message' => 'Data has been created successfully.',
            'type'    => 'success',
            'model'   => 'connect',
            'title'   => 'Record Updated',
        ],
        'user-updated' => [
            'message' => 'Data has been updated successfully.',
            'type'    => 'info',
            'model'   => 'connect',
            'title'   => 'Record Updated',
        ],
        'user-deleted' => [
            'message' => 'Data has been deleted successfully.',
            'type'    => 'danger',
            'model'   => 'connect',
            'title'   => 'Record Deleted',
        ],

        'user-approved' => [
            'message' => 'Data has been approved successfully.',
            'type'    => 'success',
            'icon'    => 'flaticon2-check-mark',
            'model'   => 'connect',
            'title'   => 'Record Approved',
//            'position' => 'bottom-right',
        ],

        'user-transfered' => [
            'message' => 'Data has been transfer successfully.',
            'type'    => 'info',
            'model'   => 'connect',
            'title'   => 'Farmers Assigned to training',
        ],
        'user-failed' => [
            'message' => 'Farmers already participanted in this training.',
            'type'    => 'danger',
            'model'   => 'connect',
            'title'   => 'Farmers Assigned to training',
        ],
    ],

];
